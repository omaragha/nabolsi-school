<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResourceController;
use App\Models\GroupMonthlyValues;
use App\Models\Sector;
use App\Traits\ResourceTrait;
use Illuminate\Http\Request;

class SectorController extends ResourceController
{
    // use ResourceTrait {
    //     ResourceTrait::__construct as private __tConstruct;
    // }

    public function __construct(Sector $model)
    {
        parent::__construct($model);
        // $this->__tConstruct($model);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $request->validate([
            'name' => 'required'
        ]);

        $sector = Sector::create([
            'name' => $request->name
        ]);

        return redirect()->route('sectors.index')->with('success', 'تم إضافة المنطقة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $request->validate([
            'name' => 'required'
        ]);

        $sector = Sector::find($id);

        if (!$sector) {
            return abort(404);
        }

        $sector->update([
            'name' => $request->name
        ]);

        return redirect()->route('sectors.index')->with('success', 'تم تعديل المنطقة بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function getSectors() {

        $sectors = Sector::all();

        return sendResponse($sectors, 'Getting sectors successfully.');

    }

}
