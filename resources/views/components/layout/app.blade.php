<!DOCTYPE html>
<html lang="ar">
    <head>
        <meta charset="utf-8" />
        <title>ثانوية عبد الغني النابلسي - {{$title}}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="{{asset('assets/images/Logo.png')}}">

        {{-- Plugins css --}}
        <link href="{{asset('assets/libs/toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
        @stack('plugin-css')

        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/app-rtl.min.css')}}" rel="stylesheet" type="text/css" />

        @stack('css')
    </head>

    <body>
        <div id="preloader">
            <div id="status">
                <div class="spinner">Loading...</div>
            </div>
        </div>

        <div id="wrapper">
            <x-includes.topbar title="{{$title}}"/>
            <x-includes.left-sidebar/>
            <div class="content-page" id="content-page">
                <div class="content">
                    <div class="container-fluid">
                        {{ $slot }}
                    </div>
                </div>
            </div>
            <x-includes.footer/>
        </div>

        <script src="{{asset('assets/js/vendor.min.js')}}"></script>
        <script src="{{asset('assets/libs/toastr/toastr.min.js')}}"></script>
        @stack('plugin-js')
        <script src="{{asset('assets/js/app.min.js')}}"></script>
        @stack('javascript')
        <script>
            $(function(){
                $(".preloader").fadeOut();
            })
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-left",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            @if((session('success')))
            toastr["success"]("{{session('success')}}")
            @endif
            @if(count($errors))
                @foreach($errors->all() as $error)
                toastr["error"]("{{$error}}")
                @endforeach
            @endif

        </script>
    </body>
</html>
