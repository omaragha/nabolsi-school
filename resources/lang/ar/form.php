<?php

return [
    'users' => [
        'name' => 'الاسم',
        'email' => 'البريد الالكتروني',
        'password' => 'كلمة المرور',
        'confirm_password' => 'تأكيد كلمة المرور',
        'description' => 'التوصيف',
        'role' => 'الصلاحية',
        'active' => 'مفعل'
    ],
    'sectors' => [
        'name' => 'الاسم'
    ],
    'students' => [
        'name' => 'اسم الطالب',
        'father_name' => 'اسم الأب',
        'mother_name' => 'اسم الأم',
        'class' => 'الصف',
        'division' => 'الشعبة',
        'gender' => 'الجنس',
        'phone' => 'رقم الهاتف',
        'student_mobile' => 'رقم موبايل الطالب',
        'father_mobile' => 'رقم موبايل الأب',
        'mother_mobile' => 'رقم موبايل الأم',
        'address' => 'العنوان',
        'sector' => 'المنطقة',
        'group' => 'الوجبة',
        'actual_cost' => 'الكلفة الفعلية',
        'notes' => 'ملاحظات'
    ],
    'moderators' => [
        'name' => 'الاسم',
        'mobile' => 'رقم الموبايل',
        'address' => 'العنوان',
        'salary' => 'الراتب'
    ],
    'contractors' => [
        'name' => 'الاسم',
        'mobile1' => 'رقم موبايل أول',
        'mobile2' => 'رقم موبايل ثان',
    ],
    'receipts' => [
        'receipt_num' => 'رقم الإيصال',
        'amount' => 'المبلغ',
        'date' => 'تاريخ الدفع',
        'notes' => 'ملاحظات',
        'student' => 'الطالب'
    ],
    'additional_expenses' => [
        'amount' => 'المبلغ',
        'date' => 'تاريخ الدفع',
        'description' => 'التوصيف'
    ],
    'additional_receipts' => [
        'amount' => 'المبلغ',
        'date' => 'تاريخ القبض',
        'description' => 'التوصيف'
    ],
    'payments' => [
        'contractor' => 'اسم المتعهد',
        'receipt_num' => 'رقم الإيصال',
        'amount' => 'المبلغ',
        'date' => 'تاريخ الدفع',
        'description' => 'التوصيف'
    ],
    'buses' => [
        'driver_name' => 'اسم السائق',
        'bus_num' => 'رقم نمرة الباص',
        'driver_mobile' => 'رقم موبايل السائق',
        'capacity' => 'السعة العظمى',
    ],
    'groups' => [
        'name' => 'اسم الوجبة',
        'type' => 'النوع',
        'driver_am' => 'سائق الفترة الصباحية',
        'driver_pm' => 'سائق الفترة المسائية',
        'moderator_am' => 'مشرفة الفترة الصباحية',
        'moderator_pm' => 'مشرفة الفترة المسائية',
        'sector' => 'المنطقة',
        'student_cost' => 'الكلفة على الطالب',
        'school_cost' => 'الكلفة على المدرسة'
    ],
    'roles' => [
        'name' => 'الاسم',
        'permissions' => 'الإجرائيات'
    ],
    'group_monthly_values' => [
        'group' => 'الوجبة',
        'sector' => 'المنطقة',
        'sep' => 'شهر 9',
        'oct' => 'شهر 10',
        'nov' => 'شهر 11',
        'dec' => 'شهر 12',
        'jan' => 'شهر 1',
        'feb' => 'شهر 2',
        'mar' => 'شهر 3',
        'apr' => 'شهر 4',
        'may' => 'شهر 5',
    ],
    'moderator_payments' => [
        'moderator' => 'اسم المشرفة',
        'amount' => 'المبلغ',
        'month' => 'الشهر',
        'description' => 'التوصيف'
    ]
];
