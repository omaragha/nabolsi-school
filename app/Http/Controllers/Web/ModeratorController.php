<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResourceController;
use App\Models\Moderator;
use App\Traits\ResourceTrait;
use Illuminate\Http\Request;

class ModeratorController extends ResourceController
{
    // use ResourceTrait {
    //     ResourceTrait::__construct as private __tConstruct;
    // }

    public function __construct(Moderator $model) {
        parent::__construct($model);
        // $this->__tConstruct($model);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'mobile' => 'required|unique:moderators,mobile',
            'address' => 'required',
            'salary' => 'required|numeric'
        ]);

        $moderator = Moderator::create($request->all());

        return redirect()->route('moderators.index')->with('success', 'تم إضافة مشرفة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'name' => 'required',
            'mobile' => 'required|unique:moderators,mobile,' . $id,
            'address' => 'required',
            'salary' => 'required|numeric'
        ]);

        $moderator = Moderator::find($id);

        if (!$moderator) {
            return abort(404);
        }

        $moderator->update($request->all());

        return redirect()->route('moderators.index')->with('success', 'تم تعديل المشرفة بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}
