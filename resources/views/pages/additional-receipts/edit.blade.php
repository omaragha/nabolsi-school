
<x-layout.form :title="__('sidebar.additional_receipts.update')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{ route('additional_receipts.update', ['additional_receipt' => $item->id]) }}" data-parsley-validate novalidate autocomplete="off">
                    @method('put')
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="amount">{{__('form.additional_receipts.amount')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="amount" :value="old('amount', $item['amount'] ?? '')" :placeholder="__('form.additional_receipts.amount')" type="number" />
                        </div>
                        <div class="col-md-6">
                            <label for="date">{{__('form.additional_receipts.date')}}*</label>
                            <x-UI.forms.datepicker :readonly="$readonly" name="date" :value="old('date',$item['date']??'')"></x-UI.forms.datepicker>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="description">{{__('form.additional_receipts.description')}}</label>
                            <x-UI.forms.textarea :readonly=$readonly name="description" :value="old('description', $item['description'] ?? '')" :placeholder="__('form.additional_receipts.description')" :readonly="$readonly"/>
                        </div>
                    </div>

                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.edit',['attr'=>' ']) }}
                            </button>
                            @endif
                            <a  href="{{route('additional_receipts.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
