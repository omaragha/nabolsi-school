<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResourceController;
use App\Models\Payment;
use App\Traits\ResourceTrait;
use Illuminate\Http\Request;

class PaymentController extends ResourceController
{
    // use ResourceTrait {
    //     ResourceTrait::__construct as private __tConstruct;
    // }

    public function __construct(Payment $model) {
        parent::__construct($model);
        // $this->__tConstruct($model);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'receipt_num' => 'required|unique:payments,receipt_num',
            'amount' => 'required|numeric',
            'date' => 'required|date',
            'contractor_id' => 'required|exists:contractors,id'
        ]);

        $payment = Payment::create($request->all());

        return redirect()->route('payments.index')->with('success', 'تم إضافة دفعة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'receipt_num' => 'required|unique:payments,receipt_num,' . $id . ',id',
            'amount' => 'required|numeric',
            'date' => 'required|date',
            'contractor_id' => 'required|exists:contractors,id'
        ]);

        $payment = Payment::find($id);

        if (!$payment) {
            return abort(404);
        }

        $payment->update($request->all());

        return redirect()->route('payments.index')->with('success', 'تم تعديل الدفعة بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}
