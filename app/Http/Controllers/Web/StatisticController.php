<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ModeratorPayment;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticController extends Controller
{

    public function index() {

        $contructorDeservedAmount = 0;
        $studentsPayments = 0;
        $contructorsPayments = 0;
        $expectedReceipts = 0;
        $additionalExpenses = 0;
        $additionalReceipts = 0;
        $studentsDiscounts = 0;

        // get contructor deserved amount.
        DB::table('group_monthly_values')
                ->select(
                            DB::raw('
                                    SUM(sep) as sep,
                                    SUM(oct) as oct,
                                    SUM(nov) as nov,
                                    SUM(jan) as jan,
                                    SUM(group_monthly_values.dec) as decc,
                                    SUM(jan) as jan,
                                    SUM(feb) as feb,
                                    SUM(mar) as mar,
                                    SUM(apr) as apr,
                                    SUM(may) as may'
                            )
                        )
                ->get()
                ->map(function ($item) use (&$contructorDeservedAmount) {
                    $contructorDeservedAmount = $item->sep +
                    $item->oct +
                    $item->nov +
                    $item->decc +
                    $item->jan +
                    $item->feb +
                    $item->mar +
                    $item->apr +
                    $item->may;
                });

        // students receipts.
        DB::table('receipts')
            ->select(DB::raw('SUM(amount) as amount'))
            ->get()
            ->map(function ($item) use (&$studentsPayments) {

                $studentsPayments += $item->amount;

            });

        // contructor payments.
        DB::table('payments')
            ->select(DB::raw('SUM(amount) as amount'))
            ->get()
            ->map(function ($item) use (&$contructorsPayments) {

                $contructorsPayments += $item->amount;

            });

        // contructor remaining amoutn.
        $contructorRemainingAmount = $contructorDeservedAmount - $contructorsPayments;

        // expected receipts.
        Student::with('group')->where('group_id', '!=', NULL)->get()->map(function ($item) use (&$expectedReceipts) {

            $expectedReceipts += ($item->group->student_cost - ($item->group->student_cost - $item->actual_cost));

        });

        // additional expenses.
        DB::table('additional_expenses')
            ->select(DB::raw('SUM(amount) as amount'))
            ->get()
            ->map(function ($item) use (&$additionalExpenses) {

                $additionalExpenses += $item->amount;

            });

        // additional receipts.
        DB::table('additional_receipts')
            ->select(DB::raw('SUM(amount) as amount'))
            ->get()
            ->map(function ($item) use (&$additionalReceipts) {

                $additionalReceipts += $item->amount;

            });

        // students discounts.
        Student::with('group')->where('group_id', '!=', NULL)->get()->map(function ($item) use (&$studentsDiscounts) {

            $studentsDiscounts += ($item->group->student_cost - $item->actual_cost);

        });

        // moderators salaries.
        $moderatorsSalaries = ModeratorPayment::sum('amount');

        $data = [
            'today'                     => date('Y-m-d h:i a'),
            'contructorDeservedAmount'  => $contructorDeservedAmount,
            'studentsPayments'          => $studentsPayments,
            'contructorsPayments'       => $contructorsPayments,
            'contructorRemainingAmount' => $contructorRemainingAmount,
            'expectedReceipts'          => $expectedReceipts,
            'additionalExpenses'        => $additionalExpenses,
            'additionalReceipts'        => $additionalReceipts,
            'studentsDiscounts'         => $studentsDiscounts,
            'moderatorsSalaries'        => $moderatorsSalaries,
            'currentAccount'            => ($studentsPayments + $additionalReceipts) - ($contructorsPayments + $additionalExpenses),
        ];

        return view('pages.statistics', compact('data'));
    }

}
