<?php

use App\Http\Controllers\Web\PaymentController;
use Illuminate\Support\Facades\Route;

Route::resource('payments', PaymentController::class);

