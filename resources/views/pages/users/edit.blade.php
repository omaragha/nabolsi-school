
<x-layout.form :title="__('sidebar.users.update')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{ route('users.update', ['user' => $item->id]) }}" data-parsley-validate novalidate autocomplete="off">
                    @method('put')
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="name">{{__('form.users.name')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="name" :value="old('name', $item['name'] ?? '')" :placeholder="__('form.users.name')" />
                        </div>
                        <div class="col-md-6">
                            <label for="email">{{__('form.users.email')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="email" :value="old('email', $item['email'] ?? '')" :placeholder="__('form.users.email')" type="email" />
                        </div>
                    </div>
                    {{-- @if (!$readonly)
                    <div class="row d-flex justify-content-center">
                        <div class="form-group col-md-6">
                            <label for="password">{{__('form.users.password')}}*</label>
                            <x-UI.forms.input name="password" :placeholder="__('form.users.password')" type="password" :value="old('password', $item['password'] ?? '')" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password_confirmation">{{__('form.users.confirm_password')}}*</label>
                            <x-UI.forms.input name="password_confirmation" :placeholder="__('form.users.confirm_password')" type="password" :value="old('password', $item['password'] ?? '')"/>
                        </div>
                    </div>
                    @endif --}}
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="password">{{__('form.users.password')}}</label>
                            <x-UI.forms.input name="password" :placeholder="__('form.users.password')" type="password" :required="false" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password_confirmation">{{__('form.users.confirm_password')}}</label>
                            <x-UI.forms.input name="password_confirmation" :placeholder="__('form.users.confirm_password')" type="password" :required="false" />
                        </div>
                    </div>
                    <div class=" row">
                        <div class="form-group col-md-6">
                            <label for="role_id">{{__('form.users.role')}}*</label>
                            <x-UI.forms.select2 name='role_id' :placeholder="__('form.users.role')" :readonly="$readonly">
                                @foreach ($roles as $role)
                                    <option value="{{$role->id}}" {{old('role_id', $item->role_id ?? '') == $role->id ? 'selected' : ''}}>
                                        {{$role->name}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="description">{{__('form.users.description')}}</label>
                            <x-UI.forms.textarea :readonly=$readonly name="description" :value="old('description', $item['description'] ?? '')" :placeholder="__('form.users.description')" :readonly="$readonly"/>
                        </div>

                        <div class="form-group col-md-6">
                            <div class="custom-control custom-checkbox">
                                <input name="is_active" type="checkbox" class="custom-control-input" id="checkbox-signin" @if(old('is_active') == 'on' || (isset($item) && $item['is_active'])) checked @endif {{ $readonly ? 'disabled' : '' }}>
                                <label class="custom-control-label" for="checkbox-signin">{{__('form.users.active')}}</label>
                            </div>
                        </div>

                    </div>


                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.edit',['attr'=>' ']) }}
                            </button>
                            @endif
                            <a  href="{{route('users.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
