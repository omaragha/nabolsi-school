<?php

namespace App\Traits;

use App\Models\Bus;
use App\Models\Contractor;
use App\Models\Moderator;
use App\Models\Role;
use App\Models\Sector;
use App\Models\Student;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

trait ResourceTrait {

    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function index() {

        $relations = $this->model->getRelations();

        $items = $this->model->with($relations)->get();

        $view = 'pages.' . Str::plural(strtolower(fixingName(class_basename($this->model)))) . '.index';

        return view($view, compact('items'));

    }

    public function create() {

        $readonly = false;

        $roles = Role::all();

        $view = 'pages.' . Str::plural(strtolower(fixingName(class_basename($this->model)))) . '.create';

        $classes = getEnumOptions('students', 'class');

        $genders = getEnumOptions('students', 'gender');

        $sectors = Sector::all();

        $students = Student::all();

        $contractors = Contractor::all();

        $drivers = Bus::all();

        $moderators = Moderator::all();

        return view($view, compact('readonly', 'roles', 'sectors', 'classes', 'genders', 'students', 'contractors', 'drivers', 'moderators'));

    }

    public function store(Request $request) {

    }

    public function show($id) {

        $relations = $this->model->getRelations();

        $item = $this->model->with($relations)->find($id);

        if (!$item) {
            return abort(404);
        }

        $view = 'pages.' . Str::plural(strtolower(fixingName(class_basename($this->model)))) . '.create';

        $readonly = true;

        $roles = Role::all();

        $sectors = Sector::all();

        $classes = getEnumOptions('students', 'class');

        $genders = getEnumOptions('students', 'gender');

        $students = Student::all();

        $contractors = Contractor::all();

        $drivers = Bus::all();

        $moderators = Moderator::all();

        return view($view, compact('item', 'readonly', 'roles', 'sectors', 'classes', 'genders', 'students', 'contractors', 'drivers', 'moderators'));
    }

    public function edit($id) {

        $relations = $this->model->getRelations();

        $item = $this->model->with($relations)->find($id);

        if (!$item) {
            return abort(404);
        }

        $view = 'pages.' . Str::plural(strtolower(fixingName(class_basename($this->model)))) . '.edit';

        $readonly = false;

        $roles = Role::all();

        $sectors = Sector::all();

        $classes = getEnumOptions('students', 'class');

        $genders = getEnumOptions('students', 'gender');

        $students = Student::all();

        $contractors = Contractor::all();

        $drivers = Bus::all();

        $moderators = Moderator::all();

        return view($view, compact('readonly', 'roles', 'sectors', 'item', 'classes', 'genders', 'students', 'contractors', 'drivers', 'moderators'));

    }

    public function update(Request $request, $id) {

    }

    public function destroy($id) {

        $item = $this->model->find($id);

        if (!$item) {
            return abort(404);
        }

        $itemName = strtolower(fixingName(class_basename($this->model), '_'));

        $item->delete();

        return redirect()->route(Str::plural($itemName) . '.index')->with('success', 'تم حذف ' . __('web.the_' . $itemName) . ' بنجاح');
    }

}
