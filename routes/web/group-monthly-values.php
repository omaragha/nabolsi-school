<?php

use App\Http\Controllers\Web\GroupMonthlyValueController;
use Illuminate\Support\Facades\Route;

Route::resource('group_monthly_values', GroupMonthlyValueController::class);

