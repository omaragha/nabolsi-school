<x-layout.data-table title="{{__('sidebar.group_monthly_values.index')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                        {{-- @if (canAccess('store_group_monthly_value'))
                            <a href="{{route('group_monthly_values.create')}}" class="btn btn-success waves-effect  waves-light">
                                <i class="fas fa-plus"></i>
                                {{__('web.add',['attr'=>__('web.group_monthly_value')])}}
                            </a>
                        @endif --}}
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="datatable" class="table table-bordered nowrap mt-5">
                        <thead>
                        <tr>
                            <th data-orderable="false">{{__('web.actions')}}</th>
                            <th>{{__('datatable.group_monthly_values.group')}}</th>
                            <th>{{__('datatable.group_monthly_values.sep')}}</th>
                            <th>{{__('datatable.group_monthly_values.oct')}}</th>
                            <th>{{__('datatable.group_monthly_values.nov')}}</th>
                            <th>{{__('datatable.group_monthly_values.dec')}}</th>
                            <th>{{__('datatable.group_monthly_values.jan')}}</th>
                            <th>{{__('datatable.group_monthly_values.feb')}}</th>
                            <th>{{__('datatable.group_monthly_values.mar')}}</th>
                            <th>{{__('datatable.group_monthly_values.apr')}}</th>
                            <th>{{__('datatable.group_monthly_values.may')}}</th>
                            {{-- <th></th> --}}
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($items as $item)
                            <tr>
                                <td>
                                    @if (canAccess('show_group_monthly_value'))
                                        <a href="{{route('group_monthly_values.show',['group_monthly_value'=>$item->id])}}" class="text-info btn-sm waves-light">
                                            <i class="far fa-eye" data-toggle="tooltip" data-placement="top" title="{{__('web.view')}}"></i>
                                            {{-- {{__('web.view')}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('update_group_monthly_value'))
                                        <a href="{{route('group_monthly_values.edit',['group_monthly_value'=>$item->id])}}" class="text-warning btn-sm waves-light">
                                            <i class=" fas fa-edit" data-toggle="tooltip" data-placement="top" title="{{__('web.edit')}}"></i>
                                            {{-- {{__('web.edit')}} --}}
                                        </a>
                                    @endif

                                    {{-- @if (canAccess('delete_group_monthly_value'))
                                        <a  href="javascript" onclick="$('#modal-form').attr('action','{{route('group_monthly_values.destroy',['group_monthly_value'=>$item->id])}}');
                                            $('#delete-name').text('{{$item->group_monthly_value_num}}')""  data-toggle="modal" data-target="#modal" class="btn btn-danger  waves-effect btn-sm waves-light">
                                            <i class="fas fa-trash"></i>
                                            {{__('web.delete',['attr'=>''])}}
                                        </a>
                                    @endif --}}
                                </td>
                                <td>{{$item->group ? $item->group->name : ''}}</td>
                                <td>{{$item->sep}}</td>
                                <td>{{$item->oct}}</td>
                                <td>{{$item->nov}}</td>
                                <td>{{$item->dec}}</td>
                                <td>{{$item->jan}}</td>
                                <td>{{$item->feb}}</td>
                                <td>{{$item->mar}}</td>
                                <td>{{$item->apr}}</td>
                                <td>{{$item->may}}</td>
                                {{-- <td></td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{-- <x-UI.modal.scroll-modal :title="__('web.delete',['attr'=>__('web.group_monthly_value')])" :body="__('web.the_group_monthly_value')"/> --}}
</x-layout.data-table >
