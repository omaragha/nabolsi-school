<div class="modal fade" id="modal" tabindex="-1" role="dialog"
    aria-labelledby="modalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalTitle">{{$title}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="POST" id="modal-form">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    <p>هل تريد حذف {{$body}} "<span class="font-weight-bold" id="delete-name"></span>" ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('web.close')}}</button>
                    <button type="submit"  class="btn btn-primary">{{__('web.confirm')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
