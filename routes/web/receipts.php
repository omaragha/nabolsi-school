<?php

use App\Http\Controllers\Web\ReceiptController;
use Illuminate\Support\Facades\Route;

Route::resource('receipts', ReceiptController::class);

