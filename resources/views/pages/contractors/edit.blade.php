
<x-layout.form :title="__('sidebar.contractors.update')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{ route('contractors.update', ['contractor' => $item->id]) }}" data-parsley-validate novalidate autocomplete="off">
                    @method('put')
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="name">{{__('form.contractors.name')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="name" :value="old('name', $item['name'] ?? '')" :placeholder="__('form.contractors.name')" />
                        </div>
                        <div class="col-md-6">
                            <label for="mobile1">{{__('form.contractors.mobile1')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="mobile1" :value="old('mobile1', $item['mobile1'] ?? '')" :placeholder="__('form.contractors.mobile1')" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="mobile2">{{__('form.contractors.mobile2')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="mobile2" :value="old('mobile2', $item['mobile2'] ?? '')" :placeholder="__('form.contractors.mobile2')" />
                        </div>
                    </div>

                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.edit',['attr'=>' ']) }}
                            </button>
                            @endif
                            <a  href="{{route('contractors.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
