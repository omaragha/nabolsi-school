<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResourceController;
use App\Models\Contractor;
use App\Traits\ResourceTrait;
use Illuminate\Http\Request;

class ContractorController extends ResourceController
{
    // use ResourceTrait {
    //     ResourceTrait::__construct as private __tConstruct;
    // }

    public function __construct(Contractor $model) {
        parent::__construct($model);
        // $this->__tConstruct($model);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'mobile1' => 'required|unique:contractors,mobile1',
            'mobile2' => 'required'
        ]);

        $contractor = Contractor::create($request->all());

        return redirect()->route('contractors.index')->with('success', 'تم إضافة متعهد بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'name' => 'required',
            'mobile1' => 'required|unique:contractors,mobile1,' . $id,
            'mobile2' => 'required'
        ]);

        $contractor = Contractor::find($id);

        if (!$contractor) {
            return abort(404);
        }

        $contractor->update($request->all());

        return redirect()->route('contractors.index')->with('success', 'تم تعديل المتعهد بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}
