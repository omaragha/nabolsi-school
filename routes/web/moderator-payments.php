<?php

use App\Http\Controllers\Web\ModeratorPaymentController;
use Illuminate\Support\Facades\Route;

Route::resource('moderator_payments', ModeratorPaymentController::class);

