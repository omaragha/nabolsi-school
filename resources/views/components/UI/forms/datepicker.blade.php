@push('javascript')
    <script>
        $("#{{$name}}").datepicker({
            format: "yyyy-mm-dd",
            language:'ar',
            rtl: true,
            autoclose: true
        })
        $("#{{$name}}").datepicker('setDate', new Date());
    </script>
@endpush
<div class="input-group">
    <div class="input-group-append">
        <span class="input-group-text"><i class="ti-calendar"></i></span>
    </div>
    <input type="text" class="form-control" {{$readonly ? 'disabled' : ''}} required placeholder="سنة-شهر-يوم" value="{{$value}}" id="{{$name}}" name="{{$name}}">
</div>
