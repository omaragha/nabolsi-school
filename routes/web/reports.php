<?php

use App\Http\Controllers\Web\ReceiptController;
use App\Http\Controllers\Web\ReportController;
use Illuminate\Support\Facades\Route;

Route::get('reports/students/accountStatement', [ReportController::class, 'getAccountStatementOfStudentPage'])->name('reports.accountStatementPage');
Route::get('reports/students/{studentId}/accountStatement', [ReportController::class, 'getAccountStatementOfStudent'])->name('reports.accountStatement');
Route::get('reports/students/{type}', [ReportController::class, 'getAllGroups'])->name('reports.groups');
Route::get('reports/{groupId}/students', [ReportController::class, 'getStudentsReportByGroup'])->name('resports.studentsByGroup');
Route::get('reports/receipts', [ReportController::class, 'getReceiptsBetweenTwoDates'])->name('reports.receiptsStatement');
Route::get('reports/groups', [ReportController::class, 'getGroupsReport'])->name('reports.groupsReport');
Route::get('reports/groupsFinancial', [ReportController::class, 'getGroupFinancialReport'])->name('reports.groupFinancialReport');
Route::get('reports/discounts', [ReportController::class, 'getDiscountsReport'])->name('reports.discountsReport');
Route::get('reports/studentsFinancial', [ReportController::class, 'getStudentsFinancialReport'])->name('reports.studentsFinancialReport');
