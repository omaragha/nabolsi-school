<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\AdditionalReceipt;
use App\Models\Bus;
use App\Models\Group;
use App\Models\Receipt;
use App\Models\Student;
use Illuminate\Http\Request;

class ReportController extends Controller
{

    public function getAllGroups($type) {

        $groups = Group::all(['id', 'name']);

        return view('pages.reports.students-details', compact('groups', 'type'));
    }

    public function getStudentsReportByGroup(Request $request, $groupId) {

        // type of report.
        $type = $request->type ? $request->type : 'drivers';

        // get all groups.
        $drivers = Group::all(['id', 'name']);

        $group = Group::with('driverAm',
                             'driverPm',
                             'moderatorAm',
                             'moderatorPm',
                             'sector',
                             'students')
                            ->where('id', $groupId)
                            ->get()
                            ->first();

        if (!$group) {
            return abort(404);
        }

        if ($type == 'drivers') {

            $data = [
                'sector'     => $group->sector,
                'studentsAm' => [],
                'studentsPm' => [],
            ];

        } else {

            $data = [
                'ID'                 => $group->id,
                'groupName'          => $group->name,
                'sector'             => $group->sector,
                'studentsAm'         => [],
                'studentsPm'         => [],
                'moderatorAmName'    => $group->moderatorAm ? $group->moderatorAm->name : '',
                'moderatorAmMobile'  => $group->moderatorAm ? $group->moderatorAm->mobile : '',
                'moderatorAmAddress' => $group->moderatorAm ? $group->moderatorAm->address : '',
                'moderatorAmSalary'  => $group->moderatorAm ? $group->moderatorAm->salary : '',
                'moderatorPmName'    => $group->moderatorPm ? $group->moderatorPm->name : '',
                'moderatorPmMobile'  => $group->moderatorPm ? $group->moderatorPm->mobile : '',
                'moderatorPmAddress' => $group->moderatorPm ? $group->moderatorPm->address : '',
                'moderatorPmSalary'  => $group->moderatorPm ? $group->moderatorPm->salary : '',
                'driverAmName'       => $group->driverAm ? $group->driverAm->driver_name : '',
                'driverAmMobile'     => $group->driverAm ? $group->driverAm->driver_mobile : '',
                'driverPmName'       => $group->driverPm ? $group->driverPm->driver_name : '',
                'driverPmMobile'     => $group->driverPm ? $group->driverPm->driver_mobile : ''
            ];

        }

        $students = [];

        foreach ($group->students as $key => $student) {

            if ($type == 'drivers') {

                $students[] = [
                    'ID'            => $student->id,
                    'name'          => $student->name,
                    'class'         => $student->class,
                    'division'      => $student->division,
                    'gender'        => $student->gender,
                    'address'       => $student->address,
                    'sector'        => $student->sector ? $student->sector->name : ''
                ];

            } else {

                $temp = [
                    'ID'            => $student->id,
                    'name'          => $student->name,
                    'fatherName'    => $student->father_name,
                    'motherName'    => $student->mother_name,
                    'class'         => $student->class,
                    'division'      => $student->division,
                    'gender'        => $student->gender,
                    'phone'         => $student->phone,
                    'studentMobile' => $student->student_mobile,
                    'fatherMobile'  => $student->father_mobile,
                    'motherMobile'  => $student->mother_mobile,
                    'address'       => $student->address,
                    'notes'         => $student->notes,
                    'sector'        => $student->sector ? $student->sector->name : ''
                ];

                if ($type == 'accountants') {

                    $deservedAmount = $student->actual_cost;

                    $paidAmount = $student->receipts->sum('amount');

                    $temp['deservedAmount'] = $deservedAmount;

                    $temp['paidAmount'] = $paidAmount;

                    $temp['remainingAmount'] = $deservedAmount - $paidAmount;

                }

                $students[] = $temp;

            }

        }

        $data['studentsAm'] = $students;

        $students = [];

        foreach ($group->students as $key => $student) {

            if ($type == 'drivers') {

                $students[] = [
                    'ID'            => $student->id,
                    'name'          => $student->name,
                    'class'         => $student->class,
                    'division'      => $student->division,
                    'gender'        => $student->gender,
                    'address'       => $student->address,
                    'sector'        => $student->sector ? $student->sector->name : ''
                ];

            } else {

                $students[] = [
                    'ID'            => $student->id,
                    'name'          => $student->name,
                    'fatherName'    => $student->father_name,
                    'motherName'    => $student->mother_name,
                    'class'         => $student->class,
                    'division'      => $student->division,
                    'gender'        => $student->gender,
                    'phone'         => $student->phone,
                    'studentMobile' => $student->student_mobile,
                    'fatherMobile'  => $student->father_mobile,
                    'motherMobile'  => $student->mother_mobile,
                    'address'       => $student->address,
                    'notes'         => $student->notes,
                    'sector'        => $student->sector ? $student->sector->name : ''
                ];

            }

        }

        $data['studentsPm'] = $students;

        $students = null;

        $group = null;

        return sendResponse($data, 'Getting student report successfully.');
    }

    public function getAccountStatementOfStudentPage() {

        $students = Student::where('group_id', '!=', NULL)->get();

        return view('pages.reports.student-account-statement', compact('students'));

    }

    public function getAccountStatementOfStudent($studentId) {

        $student = Student::with('sector', 'group', 'receipts')->where('id', $studentId)->get()->first();

        $data = [
            'receipts'       => [],
            'paidAmount'     => $student->receipts->sum('amount'),
            'deservedAmount' => $student->actual_cost,
            'amountToBePaid' => $student->actual_cost - $student->receipts->sum('amount'),
            'name'           => $student->name,
            'class'          => $student->class,
            'division'       => $student->division,
            'gender'         => $student->gender,
            'address'        => $student->address,
            'group'          => $student->group ? $student->group->name : '',
            'sector'         => $student->sector ? $student->sector->name : ''
        ];

        foreach ($student->receipts as $key => $receipt) {
            $data['receipts'][] = [
                'ID'         => $receipt->id,
                'receiptNum' => $receipt->receipt_num,
                'amount'     => $receipt->amount,
                'date'       => date('Y-m-d', strtotime($receipt->date))
            ];
        }

        return sendResponse($data, 'Getting account statement successfully.');
    }

    public function getReceiptsBetweenTwoDates(Request $request) {

        if ($request->start && $request->end) {
            $start = date('Y-m-d', $request->start ? strtotime($request->start) : time());

            $end = date('Y-m-d', $request->end ? strtotime($request->end) : time());

            $data = [
                'receipts' => [],
                'total'    => 0
            ];

            Receipt::whereBetween('date', [$start, $end])
                    ->orderBy('date', 'ASC')
                    ->groupBy('date')
                    ->selectRaw('date, SUM(amount) as amount')
                    ->get()
                    ->map(function ($item) use (&$data) {

                        if (!isset($data['receipts'][date('Y-m-d', strtotime($item->date))])) {
                            $data['receipts'][date('Y-m-d', strtotime($item->date))] = 0;
                        }

                        $data['receipts'][date('Y-m-d', strtotime($item->date))] += $item->amount;

                        $data['total'] += $item->amount;

                        return [
                            'date'   => date('Y-m-d', strtotime($item->date)),
                            'amount' => $item->amount
                        ];
                    });

            AdditionalReceipt::whereBetween('date', [$start, $end])
                            ->orderBy('date', 'ASC')
                            ->groupBy('date')
                            ->selectRaw('date, SUM(amount) as amount')
                            ->get()
                            ->map(function ($item) use (&$data) {

                                if (!isset($data['receipts'][date('Y-m-d', strtotime($item->date))])) {
                                    $data['receipts'][date('Y-m-d', strtotime($item->date))] = 0;
                                }

                                $data['receipts'][date('Y-m-d', strtotime($item->date))] += $item->amount;

                                $data['total'] += $item->amount;

                                return [
                                    'date'   => date('Y-m-d', strtotime($item->date)),
                                    'amount' => $item->amount
                                ];
                            });

            krsort($data['receipts']);

            return sendResponse($data, 'Getting receipts successfully.');

        } else {

            $data = [
                'receipts' => [],
                'total'    => 0
            ];

            Receipt::orderBy('date', 'ASC')
                    ->groupBy('date')
                    ->selectRaw('date, SUM(amount) as amount')
                    ->get()
                    ->map(function ($item) use (&$data) {

                        if (!isset($data['receipts'][date('Y-m-d', strtotime($item->date))])) {
                            $data['receipts'][date('Y-m-d', strtotime($item->date))] = 0;
                        }

                        $data['receipts'][date('Y-m-d', strtotime($item->date))] += $item->amount;

                        $data['total'] += $item->amount;

                        return [
                            'date'   => date('Y-m-d', strtotime($item->date)),
                            'amount' => $item->amount
                        ];
                    });

            AdditionalReceipt::orderBy('date', 'ASC')
                            ->groupBy('date')
                            ->selectRaw('date, SUM(amount) as amount')
                            ->get()
                            ->map(function ($item) use (&$data) {

                                if (!isset($data['receipts'][date('Y-m-d', strtotime($item->date))])) {
                                    $data['receipts'][date('Y-m-d', strtotime($item->date))] = 0;
                                }

                                $data['receipts'][date('Y-m-d', strtotime($item->date))] += $item->amount;

                                $data['total'] += $item->amount;

                                return [
                                    'date'   => date('Y-m-d', strtotime($item->date)),
                                    'amount' => $item->amount
                                ];
                            });

            krsort($data['receipts']);

            return view('pages.reports.payments-between-two-dates', compact('data'));
        }
    }

    public function getGroupsReport() {

        $studentsNumber = 0;

        $groups = Group::withCount('driverAm', 'driverPm', 'moderatorAm', 'moderatorPm', 'sector', 'students')->get()->map(function($group) use (&$studentsNumber) {

            $studentsNumber += $group->students_count;

            return [
                'id'          => $group->id,
                'name'        => $group->name,
                'type'        => $group->type == 1 ? 'ذكور' : 'إناث',
                'sector'      => $group->sector ? $group->sector->name : '',
                'driverAm'    => $group->driverAm ? $group->driverAm->driver_name : '',
                'driverPm'    => $group->driverPm ? $group->driverPm->driver_name : '',
                'moderatorAm' => $group->moderatorAm ? $group->moderatorAm->name : '',
                'moderatorPm' => $group->moderatorPm ? $group->moderatorPm->name : '',
                'students'    => $group->students,
            ];

        });

        $data = [
            'groups' => $groups,
            'studentsNumber' => $studentsNumber
        ];

        return view ('pages.reports.groups', compact('data'));
    }

    public function getGroupFinancialReport() {

        $groups = Group::with('students', 'groupMonthlyValues', 'students.receipts')->get()->map(function ($group) {

            $totalPayments = 0;

            foreach ($group->students as $key => $student) {

                foreach ($student->receipts as $key1 => $receipt) {
                    $totalPayments += $receipt->amount;
                }

            }

            $annualCost = 0;

            $months = ['sep', 'oct', 'nov', 'dec', 'jan', 'feb', 'mar', 'apr', 'may'];

            foreach ($group->groupMonthlyValues as $key => $value) {

                foreach ($months as $key1 => $month) {
                    $annualCost += $value[$month];
                }

            }

            return  [
                'id'            => $group->id,
                'name'          => $group->name,
                'annualCost'    => $annualCost,
                'totalPayments' => $totalPayments,
                'restAmount'    => $annualCost - $totalPayments
            ];
        });

        return view('pages.reports.groups-financial', compact('groups'));
    }

    public function getDiscountsReport() {

        $students = Student::with('group')->where('group_id', '!=', NULL)->where('notes', '!=', NULL)->get()->map(function ($student) {

            return [
                'id'           => $student->id,
                'name'         => $student->name,
                'class'        => $student->class,
                'division'     => $student->division,
                'amount'       => $student->group ? $student->group->student_cost : 0,
                'actualAmount' => $student->actual_cost,
                'notes'        => $student->notes,
            ];

        });

        return view('pages.reports.discounts', compact('students'));
    }

    public function getStudentsFinancialReport() {

        $students = Student::with('group', 'receipts')->where('group_id', '!=', NULL)->get()->map(function ($student) {

            return [
                'id'              => $student->id,
                'name'            => $student->name,
                'class'           => $student->class,
                'division'        => $student->division,
                'gender'          => $student->gender,
                'group'           => $student->group ? $student->group->name : '',
                'actualAmount'    => $student->actual_cost,
                'paidAmount'      => $student->receipts->sum('amount'),
                'remainingAmount' => $student->actual_cost - $student->receipts->sum('amount')
            ];

        });

        return view('pages.reports.students-financial', compact('students'));
    }

    public function getStatistics() {



    }

}
