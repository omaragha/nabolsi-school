<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Statistics permissions.
        Permission::create([
            'guard_name' => 'index_statistic',
            'name' => 'تصفح الإحصاءات',
            'group_by' => 'statistics'
        ]);

        // User permissions
        Permission::create([
            'guard_name' => 'index_user',
            'name' => 'تصفح المستخدمين',
            'group_by' => 'users'
        ]);
        Permission::create([
            'guard_name' => 'store_user',
            'name' => 'إضافة مستخدم',
            'group_by' => 'users'
        ]);
        Permission::create([
            'guard_name' => 'update_user',
            'name' => 'تعديل مستخدم',
            'group_by' => 'users'
        ]);
        Permission::create([
            'guard_name' => 'show_user',
            'name' => 'مشاهدة مستخدم',
            'group_by' => 'users'
        ]);
        Permission::create([
            'guard_name' => 'delete_user',
            'name' => 'حذف مستخدم',
            'group_by' => 'users'
        ]);


        // Role permissions
        Permission::create([
            'guard_name' => 'index_role',
            'name' => 'تصفح الصلاحيات',
            'group_by' => 'roles'
        ]);
        Permission::create([
            'guard_name' => 'store_role',
            'name' => 'إضافة صلاحية',
            'group_by' => 'roles'
        ]);
        Permission::create([
            'guard_name' => 'update_role',
            'name' => 'تعديل صلاحية',
            'group_by' => 'roles'
        ]);
        Permission::create([
            'guard_name' => 'show_role',
            'name' => 'مشاهدة صلاحية',
            'group_by' => 'roles'
        ]);
        Permission::create([
            'guard_name' => 'delete_role',
            'name' => 'حذف صلاحية',
            'group_by' => 'roles'
        ]);


        // Bus permissions
        Permission::create([
            'guard_name' => 'index_bus',
            'name' => 'تصفح الحافلات',
            'group_by' => 'buses'
        ]);
        Permission::create([
            'guard_name' => 'store_bus',
            'name' => 'إضافة حافلة',
            'group_by' => 'buses'
        ]);
        Permission::create([
            'guard_name' => 'update_bus',
            'name' => 'تعديل حافلة',
            'group_by' => 'buses'
        ]);
        Permission::create([
            'guard_name' => 'show_bus',
            'name' => 'مشاهدة حافلة',
            'group_by' => 'buses'
        ]);
        Permission::create([
            'guard_name' => 'delete_bus',
            'name' => 'حذف حافلة',
            'group_by' => 'buses'
        ]);


        // Student permissions.
        Permission::create([
            'guard_name' => 'index_student',
            'name' => 'تصفح الطلاب',
            'group_by' => 'students'
        ]);
        Permission::create([
            'guard_name' => 'store_student',
            'name' => 'إضافة طالب',
            'group_by' => 'students'
        ]);
        Permission::create([
            'guard_name' => 'update_student',
            'name' => 'تعديل طالب',
            'group_by' => 'students'
        ]);
        Permission::create([
            'guard_name' => 'show_student',
            'name' => 'مشاهدة طالب',
            'group_by' => 'students'
        ]);
        Permission::create([
            'guard_name' => 'delete_student',
            'name' => 'حذف طالب',
            'group_by' => 'students'
        ]);
        Permission::create([
            'guard_name' => 'updateSector_student',
            'name' => 'تفعيل طالب',
            'group_by' => 'students'
        ]);
        Permission::create([
            'guard_name' => 'updateActualCost_student',
            'name' => 'حسميات',
            'group_by' => 'students'
        ]);


        // Contractor permissions.
        Permission::create([
            'guard_name' => 'index_contractor',
            'name' => 'تصفح المتعهدون',
            'group_by' => 'contractors'
        ]);
        Permission::create([
            'guard_name' => 'store_contractor',
            'name' => 'إضافة متعهد',
            'group_by' => 'contractors'
        ]);
        Permission::create([
            'guard_name' => 'update_contractor',
            'name' => 'تعديل متعهد',
            'group_by' => 'contractors'
        ]);
        Permission::create([
            'guard_name' => 'show_contractor',
            'name' => 'مشاهدة متعهد',
            'group_by' => 'contractors'
        ]);
        Permission::create([
            'guard_name' => 'delete_contractor',
            'name' => 'حذف متعهد',
            'group_by' => 'contractors'
        ]);


        // Group permissions.
        Permission::create([
            'guard_name' => 'index_group',
            'name' => 'تصفح الوجبات',
            'group_by' => 'groups'
        ]);
        Permission::create([
            'guard_name' => 'store_group',
            'name' => 'إضافة وجبة',
            'group_by' => 'groups'
        ]);
        Permission::create([
            'guard_name' => 'update_group',
            'name' => 'تعديل وجبة',
            'group_by' => 'groups'
        ]);
        Permission::create([
            'guard_name' => 'show_group',
            'name' => 'مشاهدة وجبة',
            'group_by' => 'groups'
        ]);
        Permission::create([
            'guard_name' => 'delete_group',
            'name' => 'حذف وجبة',
            'group_by' => 'groups'
        ]);
        Permission::create([
            'guard_name' => 'linkStudentWithGroup_group',
            'name' => 'ربط طالب بوجبة',
            'group_by' => 'groups'
        ]);
        Permission::create([
            'guard_name' => 'updateStudentGroup_group',
            'name' => 'تعديل وجبة طالب',
            'group_by' => 'groups'
        ]);


        // Moderator permissions.
        Permission::create([
            'guard_name' => 'index_moderator',
            'name' => 'تصفح المشرفات',
            'group_by' => 'moderators'
        ]);
        Permission::create([
            'guard_name' => 'store_moderator',
            'name' => 'إضافة مشرفة',
            'group_by' => 'moderators'
        ]);
        Permission::create([
            'guard_name' => 'update_moderator',
            'name' => 'تعديل مشرفة',
            'group_by' => 'moderators'
        ]);
        Permission::create([
            'guard_name' => 'show_moderator',
            'name' => 'مشاهدة مشرفة',
            'group_by' => 'moderators'
        ]);
        Permission::create([
            'guard_name' => 'delete_moderator',
            'name' => 'حذف مشرفة',
            'group_by' => 'moderators'
        ]);


        // Payment permissions.
        Permission::create([
            'guard_name' => 'index_payment',
            'name' => 'تصفح الدفعات',
            'group_by' => 'payments'
        ]);
        Permission::create([
            'guard_name' => 'store_payment',
            'name' => 'إضافة دفعة',
            'group_by' => 'payments'
        ]);
        Permission::create([
            'guard_name' => 'update_payment',
            'name' => 'تعديل دفعة',
            'group_by' => 'payments'
        ]);
        Permission::create([
            'guard_name' => 'show_payment',
            'name' => 'مشاهدة دفعة',
            'group_by' => 'payments'
        ]);
        Permission::create([
            'guard_name' => 'delete_payment',
            'name' => 'حذف دفعة',
            'group_by' => 'payments'
        ]);

        // Moderator payment permissions.
        Permission::create([
            'guard_name' => 'index_moderator_payment',
            'name' => 'تصفح دفعات المشرفات',
            'group_by' => 'moderator_payments'
        ]);
        Permission::create([
            'guard_name' => 'store_moderator_payment',
            'name' => 'إضافة دفعة لمشرفة',
            'group_by' => 'moderator_payments'
        ]);
        Permission::create([
            'guard_name' => 'update_moderator_payment',
            'name' => 'تعديل دفعة لمشرفة',
            'group_by' => 'moderator_payments'
        ]);
        Permission::create([
            'guard_name' => 'show_moderator_payment',
            'name' => 'مشاهدة دفعة لمشرفة',
            'group_by' => 'moderator_payments'
        ]);
        Permission::create([
            'guard_name' => 'delete_moderator_payment',
            'name' => 'حذف دفعة لمشرفة',
            'group_by' => 'moderator_payments'
        ]);

        // Receipt permissions.
        Permission::create([
            'guard_name' => 'index_receipt',
            'name' => 'تصفح المقبوضات',
            'group_by' => 'receipts'
        ]);
        Permission::create([
            'guard_name' => 'store_receipt',
            'name' => 'إضافة دفعة',
            'group_by' => 'receipts'
        ]);
        Permission::create([
            'guard_name' => 'update_receipt',
            'name' => 'تعديل دفعة',
            'group_by' => 'receipts'
        ]);
        Permission::create([
            'guard_name' => 'show_receipt',
            'name' => 'مشاهدة دفعة',
            'group_by' => 'receipts'
        ]);
        Permission::create([
            'guard_name' => 'delete_receipt',
            'name' => 'حذف دفعة',
            'group_by' => 'receipts'
        ]);


        // Sector permissions.
        Permission::create([
            'guard_name' => 'index_sector',
            'name' => 'تصفح المناطق',
            'group_by' => 'sectors'
        ]);
        Permission::create([
            'guard_name' => 'store_sector',
            'name' => 'إضافة منطقة',
            'group_by' => 'sectors'
        ]);
        Permission::create([
            'guard_name' => 'update_sector',
            'name' => 'تعديل منطقة',
            'group_by' => 'sectors'
        ]);
        Permission::create([
            'guard_name' => 'show_sector',
            'name' => 'مشاهدة منطقة',
            'group_by' => 'sectors'
        ]);
        Permission::create([
            'guard_name' => 'delete_sector',
            'name' => 'حذف منطقة',
            'group_by' => 'sectors'
        ]);


        // AdditionalExpense
        Permission::create([
            'guard_name' => 'index_additional_expense',
            'name' => 'تصفح النفقات الإضافية',
            'group_by' => 'additional_expenses'
        ]);
        Permission::create([
            'guard_name' => 'store_additional_expense',
            'name' => 'إضافة نفقة إضافية',
            'group_by' => 'additional_expenses'
        ]);
        Permission::create([
            'guard_name' => 'update_additional_expense',
            'name' => 'تعديل نفقة إضافية',
            'group_by' => 'additional_expenses'
        ]);
        Permission::create([
            'guard_name' => 'show_additional_expense',
            'name' => 'مشاهدة نفقة إضافية',
            'group_by' => 'additional_expenses'
        ]);
        Permission::create([
            'guard_name' => 'delete_additional_expense',
            'name' => 'حذف نفقة إضافية',
            'group_by' => 'additional_expenses'
        ]);


        // AdditionalReceipt
        Permission::create([
            'guard_name' => 'index_additional_receipt',
            'name' => 'تصفح المقبوضات الإضافية',
            'group_by' => 'additional_receipts'
        ]);
        Permission::create([
            'guard_name' => 'store_additional_receipt',
            'name' => 'إضافة دفعة إضافية',
            'group_by' => 'additional_receipts'
        ]);
        Permission::create([
            'guard_name' => 'update_additional_receipt',
            'name' => 'تعديل دفعة إضافية',
            'group_by' => 'additional_receipts'
        ]);
        Permission::create([
            'guard_name' => 'show_additional_receipt',
            'name' => 'مشاهدة دفعة إضافية',
            'group_by' => 'additional_receipts'
        ]);
        Permission::create([
            'guard_name' => 'delete_additional_receipt',
            'name' => 'حذف دفعة إضافية',
            'group_by' => 'additional_receipts'
        ]);

        // GroupMonthlyValues permissions.
        Permission::create([
            'guard_name' => 'index_group_monthly_value',
            'name' => 'تصفح القيم الشهرية',
            'group_by' => 'group_monthly_values'
        ]);
        Permission::create([
            'guard_name' => 'update_group_monthly_value',
            'name' => 'تعديل القيم الشهرية',
            'group_by' => 'group_monthly_values'
        ]);
        Permission::create([
            'guard_name' => 'show_group_monthly_value',
            'name' => 'مشاهدة القيم الشهرية',
            'group_by' => 'group_monthly_values'
        ]);

        // Report permissions.
        Permission::create([
            'guard_name' => 'index_students_to_drivers_report',
            'name' => 'تصفح تقرير الطلاب للسائقين',
            'group_by' => 'reports'
        ]);
        Permission::create([
            'guard_name' => 'index_students_to_moderators_report',
            'name' => 'تصفح تقرير الطلاب للمشرفين',
            'group_by' => 'reports'
        ]);
        Permission::create([
            'guard_name' => 'index_students_to_accountants_report',
            'name' => 'تصفح تقرير الطلاب للمحاسبين',
            'group_by' => 'reports'
        ]);
        Permission::create([
            'guard_name' => 'index_account_statement_student_report',
            'name' => 'تصفح تقرير كشف حساب طالب',
            'group_by' => 'reports'
        ]);
        Permission::create([
            'guard_name' => 'getAccountStatementOfStudentPage_report',
            'name' => 'تصفح تقرير كشف بالدفعات',
            'group_by' => 'reports'
        ]);
        Permission::create([
            'guard_name' => 'getGroupsReport_report',
            'name' => 'تصفح تقرير الوجبات',
            'group_by' => 'reports'
        ]);
        Permission::create([
            'guard_name' => 'getGroupFinancialReport_report',
            'name' => 'تصفح تقرير مالي للوجبات',
            'group_by' => 'reports'
        ]);
        Permission::create([
            'guard_name' => 'getDiscountsReport_report',
            'name' => 'تصفح تقرير حسومات',
            'group_by' => 'reports'
        ]);
        Permission::create([
            'guard_name' => 'getStudentsFinancialReport_report',
            'name' => 'تصفح تقرير مالي للطلاب',
            'group_by' => 'reports'
        ]);

    }
}
