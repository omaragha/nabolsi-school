<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\ResourceController;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\ResourceTrait;

class UserController extends ResourceController
{
    // use ResourceTrait {
    //     ResourceTrait::__construct as private __tConstruct;
    // }

    public function __construct(User $model)
    {
        parent::__construct($model);
        // $this->__tConstruct($model);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|confirmed',
            'role_id' => 'required|exists:roles,id'
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'description' => $request->description,
            'is_active' => $request->has('is_active') ? 1 : 0,
            'role_id' => $request->role_id
        ]);

        return redirect()->route('users.index')->with('success','تم اضافة المستخدم بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id . 'id',
            'role_id' => 'required|exists:roles,id'
        ];

        if ($request->has('password')) {

            $rules['password'] = 'min:6|confirmed';

        }

        $request->validate($rules);

        $user = User::find($id);

        if (!$user) {
            return abort(404);
        }

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'role_id' => $request->role_id,
            'description' => $request->description,
            'is_active' => $request->has('is_active') ? 1 : 0
        ];

        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }

        $user->update($data);

        return redirect()->route('users.index')->with('success','تم تعديل المستخدم بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}
