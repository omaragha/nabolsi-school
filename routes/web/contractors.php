<?php

use App\Http\Controllers\Web\ContractorController;
use Illuminate\Support\Facades\Route;

Route::resource('contractors', ContractorController::class);

