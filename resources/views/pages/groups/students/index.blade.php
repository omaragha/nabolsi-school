<x-layout.data-table title="{{__('web.students') . ' ' . __('web.group') . ' ' . $group->name}}">
    @push('plugin-css')
    <link href="{{asset('assets/libs/multiselect/multi-select.css')}}"  rel="stylesheet" type="text/css" />
    <style>
        label.error {
            color: red;
            font-style: italic;
        }
    </style>
    @endpush

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                        @if (canAccess('linkStudentWithGroup_group'))
                            <a href="#" class="btn btn-success waves-effect  waves-light" id="add_student_to_group">
                                <i class="fas fa-plus"></i>
                                {{__('web.add',['attr'=>' طالب لهذه الوجبة'])}}
                            </a>
                        @endif
                    </div>
                </div>
                <input type="hidden" name="group_id" value="{{ $groupId }}">
                <div class="table-responsive">
                    <table id="datatable" class="table table-bordered nowrap mt-5">
                        <thead>
                        <tr>
                            <th data-orderable="false">{{__('web.actions')}}</th>
                            <th>{{__('datatable.students.name')}}</th>
                            <th>{{__('datatable.students.father_name')}}</th>
                            <th>{{__('datatable.students.mother_name')}}</th>
                            <th>{{__('datatable.students.class')}}</th>
                            <th>{{__('datatable.students.division')}}</th>
                            <th>{{__('datatable.students.gender')}}</th>
                            <th>{{__('datatable.students.phone')}}</th>
                            <th>{{__('datatable.students.student_mobile')}}</th>
                            <th>{{__('datatable.students.father_mobile')}}</th>
                            <th>{{__('datatable.students.mother_mobile')}}</th>
                            <th>{{__('datatable.students.address')}}</th>
                            <th>{{__('datatable.students.sector')}}</th>
                            <th>{{__('datatable.students.created_at')}}</th>
                            <th></th>
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($items as $item)
                            <tr>
                                <input type="hidden" value="{{ $item->id }}">
                                <td>
                                    @if (canAccess('updateActualCost_student'))
                                        <a href="#" class="text-pink btn-sm waves-light">
                                            <i class="fas fa-file-invoice-dollar discounts" data-toggle="tooltip" data-placement="top" title="{{__('web.discounts')}}"></i>
                                            {{-- {{__('web.discounts')}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('updateStudentGroup_group'))
                                        <a href="#" class="text-warning btn-sm waves-light">
                                            <i class=" fas fa-edit updateGroup" data-toggle="tooltip" data-placemnet="top" title="{{__('web.edit_student_group')}}"></i>
                                            {{-- {{__('web.edit')}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('updateStudentGroup_group'))
                                        <a  href="javascript" onclick="$('#modal-form').attr('action','{{route('groups.deleteStudentGroup',['studentId'=>$item->id])}}');
                                            $('#delete-name').text('{{$item->name}}')""  data-toggle="modal" data-target="#modal" class="text-danger btn-sm waves-light">
                                            <i class="fas fa-trash" data-toggle="tooltip" data-placemnet="top" title="{{__('web.delete',['attr'=>''])}}"></i>
                                            {{-- {{__('web.delete',['attr'=>''])}} --}}
                                        </a>
                                    @endif


                                </td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->father_name}}</td>
                                <td>{{$item->mother_name}}</td>
                                <td>{{$item->class}}</td>
                                <td>{{$item->division}}</td>
                                <td>{{$item->gender}}</td>
                                <td>{{$item->phone}}</td>
                                <td>{{$item->student_mobile}}</td>
                                <td>{{$item->father_mobile}}</td>
                                <td>{{$item->mother_mobile}}</td>
                                <td>{{$item->address}}</td>
                                <td>{{$item->sector ? $item->sector->name : ''}}</td>
                                <td>{{$item->created_at}}</td>
                                <td></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <x-UI.modal.scroll-modal :title="__('web.delete',['attr'=>__('web.student')])" :body="__('web.the_student')"/>

    <div id="add-students" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myCenterModalLabel">{{__('web.add',['attr'=> 'طالب'])}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form id="students-form" class="form-horizontal" role="form" method="post" action="#" data-parsley-validate novalidate autocomplete="off">
                    <div class="modal-body">
                        @csrf
                        @method('put')
                        <div class="row">
                            <div class="form-group col-lg-12 col-md-12">
                                <select  name="students[]" class="multi-select" multiple="" id="my_multi_select3" >
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary waves-effect waves-light mr-1" id="link_student_with_group">
                            {{ __('web.add',['attr'=>' ']) }}
                        </button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div id="actualCost" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title student-name" id="myLargeModalLabel">{{ __('web.student_details') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div id="update_student_sector">
                    <form id="updateActualCostForm">
                        <div class="modal-body">
                            <div id="loader" class="spinner-border text-pink m-2 " role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                            <div id="main-info">
                                    @csrf
                                    @method('put')
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="actual_cost">{{__('form.students.actual_cost')}}</label>
                                            <x-UI.forms.input name="actual_cost" :placeholder="__('form.students.actual_cost')" :readonly="false"/>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="notes">{{__('form.students.notes')}}</label>
                                            <x-UI.forms.textarea :value="''" name="notes" :placeholder="__('form.students.notes')" :readonly="false"/>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="update_student_actual_cost" type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.add',['attr'=>' ']) }}
                            </button>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div id="updateGroupModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">{{ __('web.student_details') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div id="update_student_sector">
                    <form id="update-group-form" class="form-horizontal" role="form" method="post" action="#" data-parsley-validate novalidate autocomplete="off">
                        <div class="modal-body">
                            <div id="loader1" class="spinner-border text-pink m-2 " role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                            <div id="main-info1">
                                    @csrf
                                    @method('put')
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="group_id">{{__('form.students.group')}}*</label>
                                            <x-UI.forms.select2 name='group_id' :placeholder="__('form.students.group')" :readonly="false">
                                            </x-UI.forms.select2>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="update_group" type="button" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.add',['attr'=>' ']) }}
                            </button>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="add-more-than-capacity" tabindex="-1" role="dialog"
        aria-labelledby="modalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    {{-- <h4 class="modal-title" id="modalTitle">{{$title}}</h4> --}}
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>لا يوجد شواغر في هذه الوجبة.</p>
                </div>
                <div class="modal-footer">
                    <button id="ok" class="btn btn-primary">{{__('web.confirm')}}</button>
                </div>
            </div>
        </div>
    </div>


    @push('javascript')
        <script src="{{asset('assets/libs/jquery-quicksearch/jquery.quicksearch.min.js')}}"></script>
        <script src="{{asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>
        <script>

            let groupId = $('input[name="group_id"]').val();

            let studentId = '';

            let studentName = '';

            let vacancies = 0;

            $('document').ready(function () {

                $('#add_student_to_group').click(function (evt) {

                    $.ajax({
                        url: '/groups/' + groupId + '/students/outside',
                        type: 'GET',
                        success: function (response) {

                            let html = ``;

                            vacancies = response.data.vacancies;

                            response.data.students.forEach(student => {
                                html += `<option value="${student.id}">${student.name}, ${student.class}, ${student.division}</option>`;
                            });

                            $('#my_multi_select3').html(html);
                            $("#my_multi_select3").multiSelect('destroy')
                            refreshMultiSelect();
                            $('#add-students').modal('show');

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $('#add-more-than-capacity').modal('show');
                        }
                    });


                });

                $('#link_student_with_group').click(function (evt) {
                    linkStudentWithGroup();
                });

                $('.discounts').click(function (evt) {

                    studentName = $($(evt.target).parent().parent().parent().children()[2]).text();

                    $('.student-name').text('تفاصيل الطالب ' + studentName);

                    studentId = $($(evt.target).parent().parent().parent().children()[0]).val();

                    getActualCost();


                });

                $('#update_student_actual_cost').click(function (evt) {
                    validateUpdateActualCostForm();
                    // updateActualCost();
                });

                $('#ok').click(function (evt) {
                    $('#add-more-than-capacity').modal('hide');
                });

                $('.updateGroup').click(function (evt) {

                    studentId = $($(evt.target).parent().parent().parent().children()[0]).val();

                    getGroups();

                });

                $('#update_group').click(function (evt) {
                    updateGroup();
                });

            });

            function linkStudentWithGroup() {
                $.ajax({
                        url: `/groups/${groupId}/students`,
                        type: 'POST',
                        data:$('#students-form').serialize(),
                        success: function(response) {
                            toastr["success"](response.message);
                            $('#add-students').modal('hide');
                            window.location.reload();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                        }
                    });
            }

            function refreshMultiSelect(){

                let selectedItems = 0;

                $("#my_multi_select3").multiSelect({
                    selectableHeader:
                        `
                        <label> الطلاب </label>
                        <input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>`,
                    selectionHeader:
                        `<label> المنضمون</label>
                        <input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>`,
                    afterInit: function (e) {
                        selectedItems = 0;
                        var t = this,
                            a = t.$selectableUl.prev(),
                            n = t.$selectionUl.prev(),
                            s =
                                "#" +
                                t.$container.attr("id") +
                                " .ms-elem-selectable:not(.ms-selected)",
                            r =
                                "#" +
                                t.$container.attr("id") +
                                " .ms-elem-selection.ms-selected";
                        (t.qs1 = a.quicksearch(s).on("keydown", function (e) {
                            if (40 === e.which) return t.$selectableUl.focus(), !1;
                        })),
                            (t.qs2 = n.quicksearch(r).on("keydown", function (e) {
                                if (40 == e.which) return t.$selectionUl.focus(), !1;
                            }));
                    },
                    afterSelect: function () {
                        selectedItems++;
                        this.qs1.cache(), this.qs2.cache();
                        if (selectedItems > vacancies) {

                            let elementId = $('#ms-my_multi_select3 .ms-selection .ms-list li:last')[0].id.split('-')[0] + '-selectable';

                            // remove from selected elements.
                            $('#ms-my_multi_select3 .ms-selection .ms-list li:last').css('display', 'none');

                            // add to unselected elements.
                            $('#ms-my_multi_select3 .ms-selectable .ms-list #' + elementId).css('display', '');

                            $('#add-more-than-capacity').modal('show');
                        }
                    },
                    afterDeselect: function () {
                        selectedItems--;
                        this.qs1.cache(), this.qs2.cache();
                    },
                })
            }

            function getActualCost() {

                $.ajax({
                        url: '/api/students/' + studentId + '/actualCost',
                        type: 'GET',
                        success: function (response) {

                            $('input[name="actual_cost"]').val(response.data.actual_cost ? response.data.actual_cost : response.data.group.student_cost);
                            $('textarea[name="notes"]').val(response.data.notes);

                            removeElement('label.error');
                            $('#actualCost').modal('show');

                            $('#loader').css('display', 'none');
                            $('#main-info').css('display', 'block');


                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                        }
                    });

            }

            function updateActualCost() {

                $.ajax({
                    url: '/api/students/' + studentId + '/actualCost',
                    type: 'POST',
                    data: JSON.stringify({
                        _token: '{{ csrf_token() }}',
                        _method: 'PUT',
                        actual_cost: $('input[name="actual_cost"]').val(),
                        notes: $('textarea[name="notes"]').val()
                    }),
                    processData: false,
                    contentType: 'application/json',
                    cache: false,
                    success: function (response) {
                        toastr["success"](response.message);

                        setTimeout(() => {
                            window.location.reload();
                        }, 1000);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });


            }

            function initSelect() {
                $('#group_id').find('option').remove();
                $('#group_id').append('<option></option>');
                $('#loader1').css('display', 'inline-block');
                $('#main-info1').css('display', 'none');
            }

            function getGroups() {

                initSelect();

                $.ajax({
                        url: '/groups/students/' + studentId,
                        type: 'GET',
                        success: function (response) {

                            let html = '';

                            let groups = $('select[name="group_id"]');

                            response.data.groups.forEach(group => {
                                if (group.vacancies) {
                                    if (group.id == groupId) {
                                        html += `<option value="${group.id}" selected> ${group.name}</option>`;
                                    } else {
                                        html += `<option value="${group.id}"> ${group.name}</option>`;
                                    }
                                } else if (!group.vacancies && group.id == groupId) {
                                    html += `<option value="${group.id}" selected> ${group.name}</option>`;
                                }
                            });

                            groups.append(html);

                            $('#updateGroupModal').modal('show');

                            $('#loader1').css('display', 'none');
                            $('#main-info1').css('display', 'block');


                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                        }
                    });

            }

            function updateGroup() {

                let groupId = $('select[name="group_id"]').val();

                $.ajax({
                    url: '/groups/students/' + studentId,
                    type: 'POST',
                    data: $('#update-group-form').serialize(),
                    success: function (response) {
                        toastr["success"](response.message);

                        setTimeout(() => {
                            window.location.reload();
                        }, 1000);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });


            }

            function validateUpdateActualCostForm() {

                let form = $('#updateActualCostForm');

                form.validate({
                    rules: {
                        actual_cost: 'required',
                        notes: 'required'
                    },
                    messages: {
                        actual_cost: {
                            required: "هذا الحقل مطلوب.",
                        },
                        notes: {
                            required: 'هذا الحقل مطلوب.'
                        }
                    }
                });

                if (form.valid()) {
                    updateActualCost();
                }

            }

            function removeElement(id) {
                $(id).remove();
            }

            function deleteGroup() {

            }

        </script>
    @endpush


</x-layout.data-table >
