
<x-layout.form :title="$readonly ? __('sidebar.moderators.show') : __('sidebar.moderators.add')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{ route('moderators.store') }}" data-parsley-validate novalidate autocomplete="off">
                    @if ($readonly)
                        @method('get')
                    @endif
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="name">{{__('form.moderators.name')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="name" :value="$readonly ? $item['name'] : old('name')" :placeholder="__('form.moderators.name')" />
                        </div>
                        <div class="col-md-6">
                            <label for="mobile">{{__('form.moderators.mobile')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="mobile" :value="$readonly ? $item['mobile'] : old('mobile')" :placeholder="__('form.moderators.mobile')" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="address">{{__('form.moderators.address')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="address" :value="$readonly ? $item['address'] : old('address')" :placeholder="__('form.moderators.address')" />
                        </div>
                        <div class="col-md-6">
                            <label for="salary">{{__('form.moderators.salary')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="salary" :value="$readonly ? $item['salary'] : old('salary')" :placeholder="__('form.moderators.salary')" type="number" />
                        </div>
                    </div>

                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.add',['attr'=>' ']) }}
                            </button>
                            @endif
                            <a  href="{{route('moderators.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
