<div class="left-side-menu">

    <div class="slimscroll-menu">


        <div class="user-box text-center">
            <img src="{{asset('assets/images/Logo.png')}}" alt="user-img" title="Mat Helme" class="rounded-circle img-thumbnail avatar-lg">
            <div class="dropdown">
                <a href="#" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block" data-toggle="dropdown">{{auth()->user()->name}}</a>
                <div class="dropdown-menu user-pro-dropdown">


                    {{-- <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-user mr-1"></i>
                        <span>My Account</span>
                    </a>


                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-settings mr-1"></i>
                        <span>Settings</span>
                    </a>


                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-lock mr-1"></i>
                        <span>Lock Screen</span>
                    </a> --}}


                    <a href="{{ route('logout') }}" class="dropdown-item notify-item">
                        <i class="fe-log-out mr-1"></i>
                        <span>{{ __('web.logout') }}</span>
                    </a>

                </div>
            </div>
            <p class="text-muted">{{auth()->user()->role->name}}</p>
            <ul class="list-inline">
                {{-- <li class="list-inline-item">
                    <a href="{{route('settings.index')}}" class="text-muted">
                        <i class="mdi mdi-settings"></i>
                    </a>
                </li> --}}

                <li class="list-inline-item">
                    <a href="{{route('logout')}}" class="text-muted">
                        <i class="mdi mdi-power"></i>
                    </a>
                </li>
            </ul>
        </div>


        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                @if (canAccess('index_statistic'))
                    <x-UI.lift-sidebar.link
                    title="{{ __('sidebar.statistics.index') }}"
                    icon="fas fa-file-invoice-dollar"
                    :url="route('statistics.index')" />
                @endif

                @if (canAccess('index_user'))
                    <x-UI.lift-sidebar.link
                    title="{{__('sidebar.users.index')}}"
                    icon="fas fa-user-shield"
                    :url="route('users.index')"
                    />
                @endif

                @if (canAccess('index_role'))
                    <x-UI.lift-sidebar.link
                    title="{{__('sidebar.roles.index')}}"
                    icon="fas fa-lock"
                    :url="route('roles.index')"
                    />
                @endif

                @if (canAccess('index_sector'))
                    <x-UI.lift-sidebar.link
                    title="{{__('sidebar.sectors.index')}}"
                    icon="fas fa-city"
                    :url="route('sectors.index')"
                    />
                @endif

                @if (canAccess('index_student'))
                    <x-UI.lift-sidebar.link
                    title="{{__('sidebar.students.index')}}"
                    icon="fas fa-users"
                    :url="route('students.index')"
                    />
                @endif

                @if (canAccess('index_bus'))
                    <x-UI.lift-sidebar.link
                    title="{{__('sidebar.buses.index')}}"
                    icon="fas fa-bus"
                    :url="route('buses.index')"
                    />
                @endif

                @if (canAccess('index_group'))
                    <x-UI.lift-sidebar.link
                    title="{{__('sidebar.groups.index')}}"
                    icon="fas fa-users"
                    :url="route('groups.index')"
                    />
                @endif

                @if (canAccess('index_moderator'))
                    <x-UI.lift-sidebar.link
                    title="{{__('sidebar.moderators.index')}}"
                    icon="fas fa-users"
                    :url="route('moderators.index')"
                    />
                @endif

                @if (canAccess('index_contractor'))
                    <x-UI.lift-sidebar.link
                    title="{{__('sidebar.contractors.index')}}"
                    icon="fas fa-users"
                    :url="route('contractors.index')"
                    />
                @endif

                @if (canAccess('index_payment'))
                    <x-UI.lift-sidebar.link
                    title="{{__('sidebar.payments.index')}}"
                    icon="fas fa-file-invoice-dollar"
                    :url="route('payments.index')"
                    />
                @endif

                @if (canAccess('index_moderator_payment'))
                    <x-UI.lift-sidebar.link
                    title="{{__('sidebar.moderator_payments.index')}}"
                    icon="fas fa-file-invoice-dollar"
                    :url="route('moderator_payments.index')"
                    />
                @endif

                @if (canAccess('index_receipt'))
                    <x-UI.lift-sidebar.link
                    title="{{__('sidebar.receipts.index')}}"
                    icon="fas fa-receipt"
                    :url="route('receipts.index')"
                    />
                @endif

                @if (canAccess('index_additional_expense'))
                    <x-UI.lift-sidebar.link
                    title="{{__('sidebar.additional_expenses.index')}}"
                    icon="fas fa-file-invoice-dollar"
                    :url="route('additional_expenses.index')"
                    />
                @endif

                @if (canAccess('index_additional_receipt'))
                    <x-UI.lift-sidebar.link
                    title="{{__('sidebar.additional_receipts.index')}}"
                    icon="fas fa-file-invoice-dollar"
                    :url="route('additional_receipts.index')"
                    />
                @endif

                @if (canAccess('index_group_monthly_value'))
                    <x-UI.lift-sidebar.link
                    title="{{__('sidebar.group_monthly_values.index')}}"
                    icon="fas fa-file-invoice-dollar"
                    :url="route('group_monthly_values.index')"
                    />
                @endif

                <li>
                    <a href="javascript: void(0);" aria-expanded="false">
                        <i class="fas fa-file"></i>
                        <span> {{ __('sidebar.reports.reports') }} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level collapse" aria-expanded="false">

                        <li>

                            @php
                                $studentReports = 0;
                                $studentReports = canAccess('index_students_to_drivers_report') +
                                                  canAccess('index_students_to_moderators_report') +
                                                  canAccess('index_students_to_accountants_report');
                            @endphp

                            @if ($studentReports)
                                <a href="javascript: void(0);" aria-expanded="false">
                                    <i class="fas fa-users"></i>
                                    <span> {{ __('sidebar.reports.students') }} </span>
                                    <span class="menu-arrow"></span>
                                </a>
                            @endif

                            <ul class="nav-second-level collapse" aria-expanded="false">
                                @if (canAccess('index_students_to_drivers_report'))
                                    <x-UI.lift-sidebar.link title="{{ __('sidebar.reports.students_for_drivers') }}"
                                        icon="fas fa-users" :url="route('reports.groups', ['type' => 'drivers'])" />
                                @endif
                                @if (canAccess('index_students_to_moderators_report'))
                                    <x-UI.lift-sidebar.link title="{{ __('sidebar.reports.students_for_moderators') }}"
                                        icon="fas fa-users" :url="route('reports.groups', ['type' => 'moderators'])" />
                                @endif
                                @if (canAccess('index_students_to_accountants_report'))
                                    <x-UI.lift-sidebar.link title="{{ __('sidebar.reports.students_for_accountants') }}"
                                        icon="fas fa-users" :url="route('reports.groups', ['type' => 'accountants'])" />
                                @endif
                            </ul>

                            @if (canAccess('getAccountStatementOfStudent_report'))
                                <x-UI.lift-sidebar.link
                                title="{{ __('sidebar.reports.accountStatementStudent') }}"
                                icon="fas fa-users"
                                :url="route('reports.accountStatementPage')" />
                            @endif

                            @if (canAccess('getReceiptsBetweenTwoDates_report'))
                                <x-UI.lift-sidebar.link
                                title="{{ __('sidebar.reports.paymentsAccount') }}"
                                icon="fas fa-file-invoice-dollar"
                                :url="route('reports.receiptsStatement')" />
                            @endif

                            @if (canAccess('getGroupsReport_report'))
                                <x-UI.lift-sidebar.link
                                title="{{ __('sidebar.reports.groups') }}"
                                icon="fas fa-file-invoice-dollar"
                                :url="route('reports.groupsReport')" />
                            @endif

                            @if (canAccess('getGroupFinancialReport_report'))
                                <x-UI.lift-sidebar.link
                                title="{{ __('sidebar.reports.groupsFinancial') }}"
                                icon="fas fa-file-invoice-dollar"
                                :url="route('reports.groupFinancialReport')" />
                            @endif

                            @if (canAccess('getDiscountsReport_report'))
                                <x-UI.lift-sidebar.link
                                title="{{ __('sidebar.reports.discounts') }}"
                                icon="fas fa-file-invoice-dollar"
                                :url="route('reports.discountsReport')" />
                            @endif

                            @if (canAccess('getStudentsFinancialReport_report'))
                                <x-UI.lift-sidebar.link
                                title="{{ __('sidebar.reports.studentsFinancial') }}"
                                icon="fas fa-file-invoice-dollar"
                                :url="route('reports.studentsFinancialReport')" />
                            @endif

                        </li>

                    </ul>
                </li>

            </ul>

        </div>


        <div class="clearfix"></div>

    </div>


</div>
