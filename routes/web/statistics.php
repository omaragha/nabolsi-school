<?php

use App\Http\Controllers\Web\ReceiptController;
use App\Http\Controllers\Web\ReportController;
use App\Http\Controllers\Web\StatisticController;
use Illuminate\Support\Facades\Route;

Route::get('statistics', [StatisticController::class, 'index'])->name('statistics.index');
