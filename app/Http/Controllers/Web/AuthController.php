<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function login(Request $request) {

        $request->validate([
            'username' => 'required|email',
            'password' => 'required|min:6'
        ]);

        $credentials = [
            'email' => $request->username,
            'password' => $request->password
        ];

        if (Auth::attempt($credentials)) {
            return redirect()->route('index')->with('success',__('web.welcome'));
        } else {
            return redirect()->back()->withInput()->withErrors('اسم المستخدم او كلمة المرور غير صحيحين');
        }

    }

    public function logout() {
        Auth::logout();
        session()->flush();
        return redirect()->route('login');
    }

}
