<x-layout.data-table title="{{__('web.statistics')}}">

    <div class="row">
        <div class="col-12 col-md-6">
            <div class="card-box" style="text-align: center;">
                <h4 class="header-title mt-0 mb-4">اليوم والتاريخ</h4>

                <div class="widget-chart-1">
                    {{-- <div class="widget-chart-box-1 float-left" dir="ltr">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                data-bgColor="#F9B9B9" value="58"
                                data-skin="tron" data-angleOffset="180" data-readOnly=true
                                data-thickness=".15"/>
                    </div> --}}

                    <div class="widget-detail-1 text-center" dir="ltr">
                        <h2 class="font-weight-normal pt-2 mb-1">{{ $data['today'] }}</h2>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-12 col-md-6">
            <div class="card-box" style="text-align: center;">
                <h4 class="header-title mt-0 mb-4">الصندوق</h4>

                <div class="widget-chart-1">
                    {{-- <div class="widget-chart-box-1 float-left" dir="ltr">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                data-bgColor="#F9B9B9" value="58"
                                data-skin="tron" data-angleOffset="180" data-readOnly=true
                                data-thickness=".15"/>
                    </div> --}}

                    <div class="widget-detail-1 text-center">
                        <h2 class="font-weight-normal pt-2 mb-1">{{ $data['currentAccount'] }}</h2>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-6 col-lg-3">
            <div class="card-box" style="text-align: center;">
                <h4 class="header-title mt-0 mb-4">المبلغ المستحق للمتعهد</h4>

                <div class="widget-chart-1">
                    {{-- <div class="widget-chart-box-1 float-left" dir="ltr">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                data-bgColor="#F9B9B9" value="58"
                                data-skin="tron" data-angleOffset="180" data-readOnly=true
                                data-thickness=".15"/>
                    </div> --}}

                    <div class="widget-detail-1 text-center">
                        <h2 class="font-weight-normal pt-2 mb-1">{{ $data['contructorDeservedAmount'] }}</h2>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-6 col-lg-3">
            <div class="card-box" style="text-align: center;">
                <h4 class="header-title mt-0 mb-4">مجموع الإيرادات المتوقعة</h4>

                <div class="widget-chart-1">
                    {{-- <div class="widget-chart-box-1 float-left" dir="ltr">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                data-bgColor="#F9B9B9" value="58"
                                data-skin="tron" data-angleOffset="180" data-readOnly=true
                                data-thickness=".15"/>
                    </div> --}}

                    <div class="widget-detail-1 text-center">
                        <h2 class="font-weight-normal pt-2 mb-1">{{ $data['expectedReceipts'] }}</h2>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-6 col-lg-3">
            <div class="card-box" style="text-align: center;">
                <h4 class="header-title mt-0 mb-4">مجموع دفعات الطلاب</h4>

                <div class="widget-chart-1">
                    {{-- <div class="widget-chart-box-1 float-left" dir="ltr">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                data-bgColor="#F9B9B9" value="58"
                                data-skin="tron" data-angleOffset="180" data-readOnly=true
                                data-thickness=".15"/>
                    </div> --}}

                    <div class="widget-detail-1 text-center">
                        <h2 class="font-weight-normal pt-2 mb-1">{{ $data['studentsPayments'] }}</h2>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-6 col-lg-3">
            <div class="card-box" style="text-align: center;">
                <h4 class="header-title mt-0 mb-4">مجموع النفقات الإضافية</h4>

                <div class="widget-chart-1">
                    {{-- <div class="widget-chart-box-1 float-left" dir="ltr">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                data-bgColor="#F9B9B9" value="58"
                                data-skin="tron" data-angleOffset="180" data-readOnly=true
                                data-thickness=".15"/>
                    </div> --}}

                    <div class="widget-detail-1 text-center">
                        <h2 class="font-weight-normal pt-2 mb-1">{{ $data['additionalExpenses'] }}</h2>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-6 col-lg-3">
            <div class="card-box" style="text-align: center;">
                <h4 class="header-title mt-0 mb-4">مجموع دفعات المتعهد</h4>

                <div class="widget-chart-1">
                    {{-- <div class="widget-chart-box-1 float-left" dir="ltr">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                data-bgColor="#F9B9B9" value="58"
                                data-skin="tron" data-angleOffset="180" data-readOnly=true
                                data-thickness=".15"/>
                    </div> --}}

                    <div class="widget-detail-1 text-center">
                        <h2 class="font-weight-normal pt-2 mb-1">{{ $data['contructorsPayments'] }}</h2>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-6 col-lg-3">
            <div class="card-box" style="text-align: center;">
                <h4 class="header-title mt-0 mb-4">مجموع الإيرادات الإضافية</h4>

                <div class="widget-chart-1">
                    {{-- <div class="widget-chart-box-1 float-left" dir="ltr">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                data-bgColor="#F9B9B9" value="58"
                                data-skin="tron" data-angleOffset="180" data-readOnly=true
                                data-thickness=".15"/>
                    </div> --}}

                    <div class="widget-detail-1 text-center">
                        <h2 class="font-weight-normal pt-2 mb-1">{{ $data['additionalReceipts'] }}</h2>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-6 col-lg-3">
            <div class="card-box" style="text-align: center;">
                <h4 class="header-title mt-0 mb-4">المبلغ المتبقي للمتعهد</h4>

                <div class="widget-chart-1">
                    {{-- <div class="widget-chart-box-1 float-left" dir="ltr">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                data-bgColor="#F9B9B9" value="58"
                                data-skin="tron" data-angleOffset="180" data-readOnly=true
                                data-thickness=".15"/>
                    </div> --}}

                    <div class="widget-detail-1 text-center">
                        <h2 class="font-weight-normal pt-2 mb-1">{{ $data['contructorRemainingAmount'] }}</h2>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-6 col-lg-3">
            <div class="card-box" style="text-align: center;">
                <h4 class="header-title mt-0 mb-4">مجموع حسومات الطلاب</h4>

                <div class="widget-chart-1">
                    {{-- <div class="widget-chart-box-1 float-left" dir="ltr">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                data-bgColor="#F9B9B9" value="58"
                                data-skin="tron" data-angleOffset="180" data-readOnly=true
                                data-thickness=".15"/>
                    </div> --}}

                    <div class="widget-detail-1 text-center">
                        <h2 class="font-weight-normal pt-2 mb-1">{{ $data['studentsDiscounts'] }}</h2>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-6 col-lg-3">
            <div class="card-box" style="text-align: center;">
                <h4 class="header-title mt-0 mb-4">مجموع رواتب المشرفات</h4>

                <div class="widget-chart-1">
                    {{-- <div class="widget-chart-box-1 float-left" dir="ltr">
                        <input data-plugin="knob" data-width="80" data-height="80" data-fgColor="#f05050 "
                                data-bgColor="#F9B9B9" value="58"
                                data-skin="tron" data-angleOffset="180" data-readOnly=true
                                data-thickness=".15"/>
                    </div> --}}

                    <div class="widget-detail-1 text-center">
                        <h2 class="font-weight-normal pt-2 mb-1">{{ $data['moderatorsSalaries'] }}</h2>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @push('javascript')
    @endpush

</x-layout.data-table>
