<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class hasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $controller = class_basename($request->route()->controller);

        $controller = strtolower(fixingName(substr($controller, 0, strpos($controller, 'Controller')), '_'));

        $action = $request->route()->getActionMethod();

        if ($action == 'create' || $action == 'upload' || $action == 'getFormUpload') {
            $action = 'store';
        } else if ($action == 'edit') {
            $action = 'update';
        } else if ($action == 'destroy') {
            $action = 'delete';
        } else if ($controller == 'group') {

            if ($action == 'getStudents' || $action == 'getStudentsOutsideGroup' || $action == 'linkStudentWithGroup') {
                $action = 'linkStudentWithGroup';
            } else if ($action == 'getStudentGroup' || $action == 'deleteStudentGroup') {
                $action = 'updateStudentGroup';
            }

        } else if ($controller == 'student') {
            if ($action == 'getActualCost') {
                $action = 'updateActualCost';
            }
        } else if ($controller == 'bus' && $action == 'getPmVacancyBuses') {
            $action = 'store';
        } else if ($controller == 'report') {

            if ($action == 'getAllGroups') {
                $action = 'index_students_to_' . $request->route()->parameters['type'];
            } else if ($action == 'getStudentsReportByGroup') {
                $action = 'index_students_to_' . $request->type;
            } else if ($action == 'getAccountStatementOfStudentPage') {
                $action = 'getAccountStatementOfStudent';
            }

        }

        $permission = $action . '_' . $controller;

        $permissions = auth()->user()->role->permissions()->pluck('guard_name')->toArray();

        if (in_array($permission, $permissions)) {
            return $next($request);
        } else {
            return redirect()->route('401');
        }

    }
}
