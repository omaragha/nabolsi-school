<?php

return [
    'users' => [
        'name' => 'الاسم',
        'email' => 'البريد الاكتروني',
        'role' => 'الصلاحية',
        'status' => 'حالة الحساب',
        'created_at' => 'تاريخ الإضافة'
    ],
    'sectors' => [
        'name' => 'الاسم',
        'created_at' => 'تاريخ الإضافة'
    ],
    'students' => [
        'name' => 'اسم الطالب',
        'father_name' => 'اسم الأب',
        'mother_name' => 'اسم الأم',
        'class' => 'الصف',
        'division' => 'الشعبة',
        'gender' => 'الجنس',
        'phone' => 'رقم الهاتف',
        'student_mobile' => 'رقم موبايل الطالب',
        'father_mobile' => 'رقم موبايل الأب',
        'mother_mobile' => 'رقم موبايل الأم',
        'address' => 'العنوان',
        'sector' => 'المنطقة',
        'group' => 'الوجبة',
        'notest' => 'ملاحظات',
        'created_at' => 'تاريخ الإضافة'
    ],
    'moderators' => [
        'name' => 'الاسم',
        'mobile' => 'رقم الموبايل',
        'address' => 'العنوان',
        'salary' => 'الراتب',
        'created_at' => 'تاريخ الإضافة'
    ],
    'contractors' => [
        'name' => 'الاسم',
        'mobile1' => 'رقم موبايل أول',
        'mobile2' => 'رقم موبايل ثان',
        'created_at' => 'تاريخ الإضافة'
    ],
    'receipts' => [
        'student' => 'الطالب',
        'receipt_num' => 'رقم الأيصال',
        'amount' => 'المبلغ',
        'date' => 'تاريخ القبض',
        'notes' => 'ملاحظات',
        'day' => 'اليوم'
    ],
    'additional_expenses' => [
        'amount' => 'المبلغ',
        'date' => 'تاريخ الدفع',
        'description' => 'التوصيف'
    ],
    'additional_receipts' => [
        'amount' => 'المبلغ',
        'date' => 'تاريخ القبض',
        'description' => 'التوصيف'
    ],
    'payments' => [
        'contractor' => 'اسم المتعهد',
        'receipt_num' => 'رقم الإيصال',
        'amount' => 'المبلغ',
        'date' => 'تاريخ الدفع',
        'description' => 'التوصيف'
    ],
    'buses' => [
        'driver_name' => 'اسم السائق',
        'bus_num' => 'رقم نمرة الباص',
        'driver_mobile' => 'رقم موبايل السائق',
        'capacity' => 'السعة العظمى',
        'created_at' => 'تاريخ الإضافة'
    ],
    'groups' => [
        'name' => 'اسم الوجبة',
        'type' => 'النوع',
        'driver_am' => 'سائق الفترة الصباحية',
        'driver_pm' => 'سائق الفترة المسائية',
        'moderator_am' => 'مشرفة الفترة الصباحية',
        'moderator_pm' => 'مشرفة الفترة المسائية',
        'sector' => 'المنطقة',
        'student_cost' => 'الكلفة على الطالب',
        'school_cost' => 'الكلفة على المدرسة',
        'students_count' => 'عدد الطلاب في الوجبة',
        'annual_cost' => 'التكلفة السنوية',
        'students_receipts' => 'مجموع دفعات الطلاب ',
        'rest_amount' => 'المبلغ المتبقي',
        'created_at' => 'تاريخ الإضافة'
    ],
    'roles' => [
        'name' => 'الاسم',
        'created_at' => 'تاريخ الإضافة'
    ],
    'group_monthly_values' => [
        'group' => 'الوجبة',
        'sector' => 'المنطقة',
        'sep' => 'شهر 9',
        'oct' => 'شهر 10',
        'nov' => 'شهر 11',
        'dec' => 'شهر 12',
        'jan' => 'شهر 1',
        'feb' => 'شهر 2',
        'mar' => 'شهر 3',
        'apr' => 'شهر 4',
        'may' => 'شهر 5',
        'created_at' => 'تاريخ الإضافة'
    ],
    'moderator_payments' => [
        'moderator' => 'اسم المشرفة',
        'amount' => 'المبلغ',
        'month' => 'الشهر',
        'description' => 'التوصيف'
    ],
    'reports' => [
        'students' => [
            'moderator_am_name'    => 'اسم مشرفة الفترة الصباحية',
            'moderator_pm_name'    => 'اسم مشرفة الفترة المسائية',
            'moderator_am_mobile'  => 'رقم مشرفة الفترة الصباحية',
            'moderator_pm_mobile'  => 'رقم مشرفة الفترة المسائية',
            'moderator_am_address' => 'عنوان مشرفة الفترة الصباحية',
            'moderator_pm_address' => 'عنوان مشرفة الفترة المسائية',
            'moderator_am_salary'  => 'راتب مشرفة الفترة الصباحية',
            'moderator_pm_salary'  => 'راتب مشرفة الفترة المسائية',
            'driver_am_name'       => 'اسم سائق الفترة الصباحية',
            'driver_am_mobile'     => 'رقم موبايل سائق الفترة الصباحية',
            'driver_pm_name'       => 'اسم سائق الفترة المسائية',
            'driver_pm_mobile'     => 'رقم موبايل سائق الفترة المسائية',
            'deserved_amount'      => 'المبلغ المستحق',
            'paid_amount'          => 'المبلغ المدفوع',
            'remaining_amount'     => 'المبلغ المتبقي'
        ],
        'discounts' => [
            'name'          => 'اسم الطالب',
            'class'         => 'الصف',
            'division'      => 'الشعبة',
            'amount'        => 'المبلغ المستحق',
            'actual_amount' => 'المبلغ بعد الحسم',
            'notes'         => 'ملاحظات'
        ]
    ]
];
