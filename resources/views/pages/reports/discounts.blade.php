<x-layout.data-table title="{{__('sidebar.groups.index')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                    </div>
                </div>
                <div class="table-responsive">
                    {{-- <p>omar agha</p> --}}
                    <table id="datatable" class="table table-bordered nowrap mt-5">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('datatable.reports.discounts.name')}}</th>
                            <th>{{__('datatable.reports.discounts.class')}}</th>
                            <th>{{__('datatable.reports.discounts.division')}}</th>
                            <th>{{__('datatable.reports.discounts.amount')}}</th>
                            <th>{{__('datatable.reports.discounts.actual_amount')}}</th>
                            <th>{{__('datatable.reports.discounts.notes')}}</th>
                            {{-- <th></th> --}}
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($students as $key => $student)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $student['name'] }}</td>
                                    <td>{{ $student['class'] }}</td>
                                    <td>{{ $student['division'] }}</td>
                                    <td>{{ $student['amount'] }}</td>
                                    <td>{{ $student['actualAmount'] }}</td>
                                    <td>{{ $student['notes'] }}</td>
                                    {{-- <td></td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <x-UI.modal.scroll-modal :title="__('web.delete',['attr'=>__('web.user')])" :body="__('web.the_user')"/>

    @push('javascript')

        <script>

        </script>

    @endpush


</x-layout.data-table>
