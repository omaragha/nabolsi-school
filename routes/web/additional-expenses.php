<?php

use App\Http\Controllers\Web\AdditionalExpenseController;
use Illuminate\Support\Facades\Route;

Route::resource('additional_expenses', AdditionalExpenseController::class);

