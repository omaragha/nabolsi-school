<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupMonthlyValues extends Model
{
    use HasFactory;

    protected $fillable = ['sep', 'oct', 'nov', 'dec', 'jan', 'feb', 'mar', 'apr', 'may', 'sector_id', 'group_id'];

    protected $relations = ['sector', 'group'];

    public function sector() {
        return $this->belongsTo(Sector::class);
    }

    public function group() {
        return $this->belongsTo(Group::class);
    }

}
