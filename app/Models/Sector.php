<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    public $with = ['students', 'groups', 'groupMonthlyValues'];

    public function students() {
        return $this->hasMany(Student::class);
    }

    public function groups() {
        return $this->hasMany(Group::class);
    }

    public function groupMonthlyValues() {
        return $this->hasMany(GroupMonthlyValues::class);
    }

}
