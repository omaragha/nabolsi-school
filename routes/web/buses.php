<?php

use App\Http\Controllers\Web\BusController;
use Illuminate\Support\Facades\Route;

Route::resource('buses', BusController::class);

Route::get('buses/{driverId}/vacancyBusesSameCapacity', [BusController::class, 'getPmVacancyBuses']);
