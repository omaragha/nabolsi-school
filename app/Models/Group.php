<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'type', 'driver_id_am', 'driver_id_pm', 'moderator_id_am', 'moderator_id_pm', 'sector_id', 'student_cost', 'school_cost'];

    protected $relations = ['driverAm', 'driverPm', 'moderatorAm', 'moderatorPm', 'sector'];

    public function driverAm() {
        return $this->belongsTo(Bus::class, 'driver_id_am');
    }

    public function driverPm() {
        return $this->belongsTo(Bus::class, 'driver_id_pm');
    }

    public function moderatorAm() {
        return $this->belongsTo(Moderator::class, 'moderator_id_am');
    }

    public function moderatorPm() {
        return $this->belongsTo(Moderator::class, 'moderator_id_pm');
    }

    public function sector() {
        return $this->belongsTo(Sector::class);
    }

    public function students() {
        return $this->hasMany(Student::class);
    }

    public function groupMonthlyValues() {
        return $this->hasMany(GroupMonthlyValues::class);
    }

}
