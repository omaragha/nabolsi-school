@push('javascript')
    <script>
        $('select[name="{{$name}}"]').select2({
            placeholder: "{{$placeholder}}",
        });
    </script>
@endpush
<select {{ $required ? 'required' : '' }} name="{{$name}}" id="{{$name}}" {{$readonly ? 'disabled':''}} class="form-control select2">
    <option></option>
    {{$slot}}
</select>
