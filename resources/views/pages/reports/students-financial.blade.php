<x-layout.data-table title="{{__('sidebar.groups.index')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                    </div>
                </div>
                <div class="table-responsive">
                    {{-- <p>omar agha</p> --}}
                    <table id="datatable" class="table table-bordered nowrap mt-5">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('datatable.students.name')}}</th>
                            <th>{{__('datatable.students.class')}}</th>
                            <th>{{__('datatable.students.division')}}</th>
                            <th>{{__('datatable.students.gender')}}</th>
                            <th>{{__('datatable.students.group')}}</th>
                            <th>{{__('datatable.reports.students.deserved_amount')}}</th>
                            <th>{{__('datatable.reports.students.paid_amount')}}</th>
                            <th>{{__('datatable.reports.students.remaining_amount')}}</th>
                            {{-- <th></th> --}}
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($students as $key => $student)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $student['name'] }}</td>
                                    <td>{{ $student['class'] }}</td>
                                    <td>{{ $student['division'] }}</td>
                                    <td>{{ $student['gender'] }}</td>
                                    <td>{{ $student['group'] }}</td>
                                    <td>{{ $student['actualAmount'] }}</td>
                                    <td>{{ $student['paidAmount'] }}</td>
                                    <td>{{ $student['remainingAmount'] }}</td>
                                    {{-- <td></td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <x-UI.modal.scroll-modal :title="__('web.delete',['attr'=>__('web.user')])" :body="__('web.the_user')"/>

    @push('javascript')

        <script>

        </script>

    @endpush


</x-layout.data-table>
