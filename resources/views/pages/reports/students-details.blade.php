<x-layout.data-table title="{{__('sidebar.students.index')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-6">
                        <label for="groupId">{{__('sidebar.groups.index')}}</label>
                        <x-UI.forms.select2 name='groupId' :placeholder="__('sidebar.groups.index')">
                            @foreach ($groups as $key => $group)
                                {{ $group->name }}
                                <option value="{{ $group->id }}">
                                    {{ $group->name }}
                                </option>
                            @endforeach
                        </x-UI.forms.select2>
                    </div>
                </div>
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="datatable" class="table table-bordered nowrap mt-5">
                        <thead>
                        <tr>
                            <th>#</th>
                            @if ($type == 'drivers')
                                <th>{{__('datatable.students.name')}}</th>
                                <th>{{__('datatable.students.class')}}</th>
                                <th>{{__('datatable.students.division')}}</th>
                                {{-- <th>{{__('datatable.students.gender')}}</th> --}}
                                <th>{{__('datatable.students.address')}}</th>
                                {{-- <th>{{__('datatable.students.sector')}}</th> --}}
                            @elseif ($type == 'moderators')
                                <th>{{__('datatable.students.name')}}</th>
                                <th>{{__('datatable.students.class')}}</th>
                                <th>{{__('datatable.students.division')}}</th>
                                {{-- <th>{{__('datatable.students.gender')}}</th> --}}
                                <th>{{__('datatable.students.address')}}</th>
                                {{-- <th>{{__('datatable.students.sector')}}</th> --}}
                                {{-- <th>{{__('datatable.students.phone')}}</th> --}}
                                <th>{{__('datatable.students.student_mobile')}}</th>
                                <th>{{__('datatable.students.father_mobile')}}</th>
                                <th>{{__('datatable.students.mother_mobile')}}</th>
                            @else
                                <th>{{__('datatable.students.name')}}</th>
                                <th>{{__('datatable.students.class')}}</th>
                                <th>{{__('datatable.students.division')}}</th>
                                <th>{{__('datatable.students.gender')}}</th>
                                <th>{{__('datatable.students.sector')}}</th>
                                <th>{{__('datatable.reports.students.deserved_amount')}}</th>
                                <th>{{__('datatable.reports.students.paid_amount')}}</th>
                                <th>{{__('datatable.reports.students.remaining_amount')}}</th>
                            @endif
                            {{-- <th></th> --}}
                        </tr>
                        </thead>


                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <x-UI.modal.scroll-modal :title="__('web.delete',['attr'=>__('web.user')])" :body="__('web.the_user')"/>

    <div id="sectorsModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">{{ __('web.student_details') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div id="update_student_sector">
                    <div class="modal-body">
                        <div id="loader" class="spinner-border text-pink m-2 " role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div id="main-info">
                                @csrf
                                @method('put')
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="father_name">{{__('form.students.father_name')}}</label>
                                        <x-UI.forms.input name="father_name" :placeholder="__('form.students.father_name')" :readonly="false"/>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="mother_name">{{__('form.students.mother_name')}}</label>
                                        <x-UI.forms.input name="mother_name" :placeholder="__('form.students.mother_name')" :readonly="false"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="phone">{{__('form.students.phone')}}</label>
                                        <x-UI.forms.input name="phone" :placeholder="__('form.students.phone')" :readonly="false"/>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="student_mobile">{{__('form.students.student_mobile')}}</label>
                                        <x-UI.forms.input name="student_mobile" :placeholder="__('form.students.student_mobile')" :readonly="false"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="father_mobile">{{__('form.students.father_mobile')}}</label>
                                        <x-UI.forms.input name="father_mobile" :placeholder="__('form.students.father_mobile')" :readonly="false"/>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="mother_mobile">{{__('form.students.mother_mobile')}}</label>
                                        <x-UI.forms.input name="mother_mobile" :placeholder="__('form.students.mother_mobile')" :readonly="false"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="address">{{__('form.students.address')}}</label>
                                        <x-UI.forms.input name="address" :placeholder="__('form.students.address')" :readonly="false"/>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="sector_id">{{__('form.students.sector')}}*</label>
                                        <x-UI.forms.select2 name='sector_id' :placeholder="__('form.students.sector')" :readonly="false">
                                        </x-UI.forms.select2>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="update_sector" type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                            {{ __('web.add',['attr'=>' ']) }}
                        </button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    @push('javascript')

        <script>

            let groupId = '';

            let groupName = '';

            let type = '{{ $type }}';

            $(document).ready(function () {

                $('select[name="groupId"]').change(function (evt) {
                    groupName = $('select[name="groupId"] option:selected').text().trim();
                    groupId = this.value;
                    getReport(type);
                });

            });


            function getReport(type) {

                $.ajax({
                        url: '/reports/' + groupId + '/students?type=' + type,
                        type: 'GET',
                        success: function (response) {

                            if ( $.fn.DataTable.isDataTable('#datatable') ) {
                                $('#datatable').DataTable().destroy();
                            }

                            $('#datatable tbody').empty();

                            var myDatatable = $('#datatable').DataTable({
                                dom: 'Bfrtip',
                                language: {
                                    url: '../../assets/libs/datatables/translate.json'
                                },
                                // responsive: true,
                                iDisplayLength:50,
                                columnDefs: [{
                                    "defaultContent": "",
                                    "targets": "_all"
                                }],
                                buttons: [
                                    {
                                        extend: 'csv',
                                        text: 'تصدير كملف اكسل',
                                        className: 'btn btn-primary',
                                        bom: true,
                                    },
                                    {
                                        extend: 'print',
                                        text: 'طباعة',
                                        className: 'btn btn-secondary',
                                        bom: true,
                                        title: (type == 'drivers' ? 'الطلاب للسائقين' : (type == 'moderators' ? 'الطلاب للمشرفين' : 'الطلاب للمحاسبين')) + ' - وجبة ' + groupName,
                                        exportOptions: ['']
                                    },
                                ]
                            });

                            let studentsAm = response.data.studentsAm;

                            let studentsPm = response.data.studentPm;

                            studentsAm.forEach((student, index) => {

                                let row = [];

                                if (type == 'drivers') {
                                    row = [
                                        index + 1,
                                        student.name,
                                        student.class,
                                        student.division,
                                        // student.gender,
                                        student.address,
                                        // student.sector
                                    ];
                                } else if (type == 'moderators') {

                                    row = [
                                        index + 1,
                                        student.name,
                                        student.class,
                                        student.division,
                                        // student.gender,
                                        student.address,
                                        // student.sector,
                                        // student.phone,
                                        student.studentMobile,
                                        student.fatherMobile,
                                        student.motherMobile,
                                    ];

                                } else {

                                    row = [
                                        index + 1,
                                        student.name,
                                        student.class,
                                        student.division,
                                        student.gender,
                                        student.sector,
                                        student.deservedAmount,
                                        student.paidAmount,
                                        student.remainingAmount
                                    ];

                                }

                                var rowNode = myDatatable.row.add(row).draw().node();

                                // $(rowNode).data('jobId', element.id).data('date', element.date).addClass(['clickable-row']);

                            });




                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                        }
                    });

            }

        </script>

    @endpush


</x-layout.data-table>
