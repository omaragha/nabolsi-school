<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModeratorPayment extends Model
{
    use HasFactory;

    protected $fillable = ['amount', 'month', 'description', 'moderator_id'];

    protected $relations = ['moderator'];

    public function moderator() {
        return $this->belongsTo(Moderator::class);
    }

}
