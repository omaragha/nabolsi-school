<x-layout.data-table title="{{__('sidebar.students.index')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                        @if (canAccess('store_student'))
                            <a href="{{route('students.create')}}" class="btn btn-success waves-effect  waves-light">
                                <i class="fas fa-plus"></i>
                                {{__('web.add',['attr'=>__('web.student')])}}
                            </a>
                            <a href="{{route('students.importForm')}}" class="btn btn-primary waves-effect  waves-light">
                                {{__('web.add',['attr'=>__('web.students')]) . ' من ' . __('web.excel_file')}}
                            </a>
                            <a href="{{route('excel_sample')}}" class="btn btn-secondary waves-effect  waves-light">
                                {{__('web.excel_sample')}}
                            </a>
                        @endif
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="datatable" class="table table-bordered nowrap mt-5">
                        <thead>
                        <tr>
                            <th data-orderable="false">{{__('web.actions')}}</th>
                            <th>{{__('datatable.students.name')}}</th>
                            <th>{{__('datatable.students.father_name')}}</th>
                            <th>{{__('datatable.students.mother_name')}}</th>
                            <th>{{__('datatable.students.class')}}</th>
                            <th>{{__('datatable.students.division')}}</th>
                            <th>{{__('datatable.students.gender')}}</th>
                            <th>{{__('datatable.students.group')}}</th>
                            <th>{{__('datatable.students.phone')}}</th>
                            <th>{{__('datatable.students.student_mobile')}}</th>
                            <th>{{__('datatable.students.father_mobile')}}</th>
                            <th>{{__('datatable.students.mother_mobile')}}</th>
                            <th>{{__('datatable.students.address')}}</th>
                            <th>{{__('datatable.students.sector')}}</th>
                            <th>{{__('datatable.students.created_at')}}</th>
                            <th></th>
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($items as $item)
                            <tr>
                                <input type="hidden" value="{{ $item->id }}">
                                <td>
                                    @if (canAccess('show_student'))
                                        <a href="{{route('students.show',['student'=>$item->id])}}" class="text-info btn-sm waves-light">
                                            <i class="far fa-eye" data-toggle="tooltip" data-placement="top" title="{{__('web.view')}}"></i>
                                            {{-- {{__('web.view')}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('update_student'))
                                        <a href="{{route('students.edit',['student'=>$item->id])}}" class="text-warning btn-sm waves-light">
                                            <i class=" fas fa-edit" data-toggle="tooltip" data-placement="top" title="{{__('web.edit')}}"></i>
                                            {{-- {{__('web.edit')}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('delete_student'))
                                        <a  href="javascript" onclick="$('#modal-form').attr('action','{{route('students.destroy',['student'=>$item->id])}}');
                                            $('#delete-name').text('{{$item->name}}')"  data-toggle="modal" data-target="#modal" class="text-danger btn-sm waves-light">
                                            <i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="{{__('web.delete',['attr'=>''])}}"></i>
                                            {{-- {{__('web.delete',['attr'=>''])}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('updateSector_student'))
                                        @if (!$item->sector)
                                            <a class="text-pink btn-sm waves-light active_student">
                                                <i class="fas fa-user-circle" data-toggle="toolip" data-placement="top" title="{{__('web.active_student')}}"></i>
                                                {{-- {{__('web.active_student')}} --}}
                                            </a>
                                        @else
                                            <a class="text-pink btn-sm waves-light active_student" disabled style="cursor: not-allowed;">
                                                <i class="fas fa-user-circle" data-toggle="toolip" data-placement="top" title="{{__('web.active_student')}}"></i>
                                                {{-- {{__('web.active_student')}} --}}
                                            </a>
                                        @endif
                                    @endif

                                </td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->father_name}}</td>
                                <td>{{$item->mother_name}}</td>
                                <td>{{$item->class}}</td>
                                <td>{{$item->division}}</td>
                                <td>{{$item->gender}}</td>
                                <td>{{$item->group ? $item->group->name : ''}}</td>
                                <td>{{$item->phone}}</td>
                                <td>{{$item->student_mobile}}</td>
                                <td>{{$item->father_mobile}}</td>
                                <td>{{$item->mother_mobile}}</td>
                                <td>{{$item->address}}</td>
                                <td>{{$item->sector ? $item->sector->name : ''}}</td>
                                <td>{{$item->created_at}}</td>
                                <td></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <x-UI.modal.scroll-modal :title="__('web.delete',['attr'=>__('web.user')])" :body="__('web.the_user')"/>

    <div id="sectorsModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">{{ __('web.student_details') }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div id="update_student_sector">
                    <div class="modal-body">
                        <div id="loader" class="spinner-border text-pink m-2 " role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div id="main-info">
                                @csrf
                                @method('put')
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="father_name">{{__('form.students.father_name')}}</label>
                                        <x-UI.forms.input name="father_name" :placeholder="__('form.students.father_name')" :readonly="false"/>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="mother_name">{{__('form.students.mother_name')}}</label>
                                        <x-UI.forms.input name="mother_name" :placeholder="__('form.students.mother_name')" :readonly="false"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="phone">{{__('form.students.phone')}}</label>
                                        <x-UI.forms.input name="phone" :placeholder="__('form.students.phone')" :readonly="false"/>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="student_mobile">{{__('form.students.student_mobile')}}</label>
                                        <x-UI.forms.input name="student_mobile" :placeholder="__('form.students.student_mobile')" :readonly="false"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="father_mobile">{{__('form.students.father_mobile')}}</label>
                                        <x-UI.forms.input name="father_mobile" :placeholder="__('form.students.father_mobile')" :readonly="false"/>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="mother_mobile">{{__('form.students.mother_mobile')}}</label>
                                        <x-UI.forms.input name="mother_mobile" :placeholder="__('form.students.mother_mobile')" :readonly="false"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="address">{{__('form.students.address')}}</label>
                                        <x-UI.forms.input name="address" :placeholder="__('form.students.address')" :readonly="false"/>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="sector_id">{{__('form.students.sector')}}*</label>
                                        <x-UI.forms.select2 name='sector_id' :placeholder="__('form.students.sector')" :readonly="false">
                                        </x-UI.forms.select2>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="update_sector" type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                            {{ __('web.add',['attr'=>' ']) }}
                        </button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    @push('javascript')
        <script>

            let CSRFTOKEN = '{{ csrf_token() }}'

            let studentId = '';

            $('document').ready(function () {

                $('#loader').css('display', 'inline-block');
                $('#main-info').css('display', 'none');

                $('.active_student').click(function (evt) {

                    studentId = $($(evt.target).parent().parent().parent().children()[0]).val();

                    $('input[name="father_name"]').val($($(evt.target).parent().parent().parent().children()[3]).text());
                    $('input[name="mother_name"]').val($($(evt.target).parent().parent().parent().children()[4]).text());
                    $('input[name="phone"]').val($($(evt.target).parent().parent().parent().children()[8]).text());
                    $('input[name="student_mobile"]').val($($(evt.target).parent().parent().parent().children()[9]).text());
                    $('input[name="father_mobile"]').val($($(evt.target).parent().parent().parent().children()[10]).text());
                    $('input[name="mother_mobile"]').val($($(evt.target).parent().parent().parent().children()[11]).text());
                    $('input[name="address"]').val($($(evt.target).parent().parent().parent().children()[12]).text());

                    $('#sectorsModal').modal('show');

                    getSectors();

                });

                $('#update_sector').click(function (evt) {

                    updateSector();

                    // $('#sectorsModal').modal('hide');

                });

            });

            function initSelect() {
                $('#sector_id').find('option').remove();
                $('#sector_id').append('<option></option>');
                $('#loader').css('display', 'inline-block');
                $('#main-info').css('display', 'none');
            }

            function getSectors() {

                initSelect();

                $.ajax({
                    url: '/api/sectors',
                    type: 'GET',
                    success: function (response) {

                        response.data.forEach(sector => {
                            $('#sector_id').append(`<option value=${sector.id}>${sector.name}</option>`);
                        })

                        $('#loader').css('display', 'none');
                        $('#main-info').css('display', 'block');

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });

            }

            function updateSector() {

                let sectorId = $('#sector_id').val();
                let fatherName = $('input[name="father_name"]').val();
                let motherName = $('input[name="mother_name"]').val();
                let phone = $('input[name="phone"]').val();
                let studentMobile = $('input[name="student_mobile"]').val();
                let fatherMobile = $('input[name="father_mobile"]').val();
                let motherMobile = $('input[name="mother_mobile"]').val();
                let address = $('input[name="address"]').val();

                $.ajax({
                    url: '/api/students/' + studentId + '/sector',
                    type: 'POST',
                    data: JSON.stringify({
                        sector_id: sectorId,
                        father_name: fatherName,
                        mother_name: motherName,
                        '_token': 'csrf_token()',
                        '_method': 'PUT',
                        phone,
                        student_mobile: studentMobile,
                        father_mobile: fatherMobile,
                        mother_mobile: motherMobile,
                        address
                    }),
                    processData: false,
                    contentType: 'application/json',
                    cache: false,
                    success: function (response) {
                        toastr["success"](response.message);

                        setTimeout(() => {
                            window.location.reload();
                        }, 1000);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });


            }

        </script>
    @endpush


</x-layout.data-table >
