<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('type')->nullable();
            $table->double('student_cost')->nullable();
            $table->double('school_cost')->nullable();
            $table->unsignedBigInteger('sector_id')->nullable();
            $table->foreign('sector_id')->references('id')->on('sectors')->onUpdate('cascade')->onDelete('set null');
            $table->unsignedBigInteger('driver_id_am')->nullable();
            $table->foreign('driver_id_am')->references('id')->on('buses')->onUpdate('cascade')->onDelete('set null');
            $table->unsignedBigInteger('driver_id_pm')->nullable();
            $table->foreign('driver_id_pm')->references('id')->on('buses')->onUpdate('cascade')->onDelete('set null');
            $table->unsignedBigInteger('moderator_id_am')->nullable();
            $table->foreign('moderator_id_am')->references('id')->on('moderators')->onUpdate('cascade')->onDelete('set null');
            $table->unsignedBigInteger('moderator_id_pm')->nullable();
            $table->foreign('moderator_id_pm')->references('id')->on('moderators')->onUpdate('cascade')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
