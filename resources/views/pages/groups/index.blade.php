<x-layout.data-table title="{{__('sidebar.groups.index')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                        @if (canAccess('store_group'))
                            <a href="{{route('groups.create')}}" class="btn btn-success waves-effect  waves-light">
                                <i class="fas fa-plus"></i>
                                {{__('web.add',['attr'=>__('web.group')])}}
                            </a>
                        @endif
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="datatable" class="table table-bordered nowrap mt-5">
                        <thead>
                        <tr>
                            <th data-orderable="false">{{__('web.actions')}}</th>
                            <th>{{__('datatable.groups.name')}}</th>
                            <th>{{__('datatable.groups.type')}}</th>
                            <th>{{__('datatable.groups.driver_am')}}</th>
                            <th>{{__('datatable.groups.driver_pm')}}</th>
                            <th>{{__('datatable.groups.moderator_am')}}</th>
                            <th>{{__('datatable.groups.moderator_pm')}}</th>
                            <th>{{__('datatable.groups.sector')}}</th>
                            <th>{{__('datatable.groups.student_cost')}}</th>
                            {{-- <th>{{__('datatable.groups.school_cost')}}</th> --}}
                            <th>{{__('datatable.groups.created_at')}}</th>
                            {{-- <th></th> --}}
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($items as $item)
                            <tr>
                                <td>
                                    @if (canAccess('show_group'))
                                        <a href="{{route('groups.show',['group'=>$item->id])}}" class="text-info btn-sm waves-light">
                                            <i class="far fa-eye" data-toggle="tooltip" data-placement="top" title="{{__('web.view')}}"></i>
                                            {{-- {{__('web.view')}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('update_group'))
                                        <a href="{{route('groups.edit',['group'=>$item->id])}}" class="text-warning btn-sm waves-light">
                                            <i class=" fas fa-edit" data-toggle="tooltip" data-placement="top" title="{{__('web.edit')}}"></i>
                                            {{-- {{__('web.edit')}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('delete_group'))
                                        <a  href="javascript" onclick="$('#modal-form').attr('action','{{route('groups.destroy',['group'=>$item->id])}}');
                                            $('#delete-name').text('{{$item->name}}')""  data-toggle="modal" data-target="#modal" class="text-danger btn-sm waves-light">
                                            <i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="{{__('web.delete',['attr'=>''])}}"></i>
                                            {{-- {{__('web.delete',['attr'=>''])}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('linkStudentWithGroup_group'))
                                        <a  href="{{ route('group.students', ['id' => $item->id]) }}" class="text-pink btn-sm waves-light">
                                            <i class="fas fa-user-circle" data-toggle="tooltip" data-placement="top" title="{{__('web.group_students',['attr'=>''])}}"></i>
                                            {{-- {{__('web.group_students',['attr'=>''])}} --}}
                                        </a>
                                    @endif


                                </td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->type}}</td>
                                <td>{{$item->driverAm ? $item->driverAm->driver_name : ''}}</td>
                                <td>{{$item->driverPm ? $item->driverPm->driver_name : ''}}</td>
                                <td>{{$item->moderatorAm ? $item->moderatorAm->name : ''}}</td>
                                <td>{{$item->moderatorPm ? $item->moderatorPm->name : ''}}</td>
                                <td>{{$item->sector ? $item->sector->name : ''}}</td>
                                <td>{{$item->student_cost}}</td>
                                {{-- <td>{{$item->school_cost}}</td> --}}
                                <td>{{$item->created_at}}</td>
                                {{-- <td></td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <x-UI.modal.scroll-modal :title="__('web.delete',['attr'=>__('web.group')])" :body="__('web.the_group')"/>
</x-layout.data-table >
