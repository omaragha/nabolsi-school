<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

include(base_path('/routes/web/auth.php'));

Route::group(['middleware' => 'auth'], function () {

    Route::group(['middleware' => 'hasPermission'], function () {
        include(base_path('/routes/web/users.php'));
        include(base_path('/routes/web/roles.php'));
        include(base_path('/routes/web/sectors.php'));
        include(base_path('/routes/web/students.php'));
        include(base_path('/routes/web/moderators.php'));
        include(base_path('/routes/web/contractors.php'));
        include(base_path('/routes/web/receipts.php'));
        include(base_path('/routes/web/additional-expenses.php'));
        include(base_path('/routes/web/additional-receipts.php'));
        include(base_path('/routes/web/payments.php'));
        include(base_path('/routes/web/moderator-payments.php'));
        include(base_path('/routes/web/buses.php'));
        include(base_path('/routes/web/groups.php'));
        include(base_path('/routes/web/group-monthly-values.php'));
        include(base_path('/routes/web/reports.php'));
        include(base_path('/routes/web/statistics.php'));
    });

    Route::get('/sample-excel', function () {

        $fileName = 'sample.xlsx';

        $path = asset('storage/files/sample.xlsx');

        return redirect($path);

    })->name('excel_sample');

    Route::view('/', 'index')->name('index');

});


Route::view('/401', 'pages.errors.401')->name('401');
