
<x-layout.form :title="$readonly ? __('sidebar.moderator_payments.show') : __('sidebar.moderator_payments.add')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{ route('moderator_payments.store') }}" data-parsley-validate novalidate autocomplete="off">
                    @if ($readonly)
                        @method('get')
                    @endif
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="amount">{{__('form.moderator_payments.amount')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="amount" :value="$readonly ? $item['amount'] : old('amount')" :placeholder="__('form.moderator_payments.amount')" type="number" />
                        </div>
                        <div class="col-md-6">
                            <label for="month">{{__('form.moderator_payments.month')}}*</label>
                            <x-UI.forms.select2 name='month' :placeholder="__('form.moderator_payments.month')" :readonly="$readonly">
                                @foreach ($months as $key => $month)
                                    <option value="{{$month}}" {{old('month',$item['month'] ?? '') == $month ? 'selected' : ''}}>
                                        {{$month}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="moderator_id">{{__('form.moderator_payments.moderator')}}*</label>
                            <x-UI.forms.select2 name='moderator_id' :placeholder="__('form.moderator_payments.moderator')" :readonly="$readonly">
                                @foreach ($moderators as $key => $moderator)
                                    <option value="{{$moderator->id}}" {{old('moderator_id',$item['moderator_id'] ?? '') == $moderator->id ? 'selected' : ''}}>
                                        {{$moderator->name}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>
                        <div class="col-md-6">
                            <label for="description">{{__('form.moderator_payments.description')}}</label>
                            <x-UI.forms.textarea :readonly=$readonly name="description" :value="$readonly ? $item['description'] : old('description')" :placeholder="__('form.moderator_payments.description')" :readonly="$readonly"/>
                        </div>
                    </div>

                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.add',['attr'=>' ']) }}
                            </button>
                            @endif
                            <a  href="{{route('moderator_payments.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
