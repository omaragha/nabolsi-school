<x-layout.data-table title="{{__('sidebar.reports.accountStatementStudent')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-sm-6">
                        <label for="studentId">{{__('sidebar.students.index')}}</label>
                        <x-UI.forms.select2 name='studentId' :placeholder="__('sidebar.students.index')">
                            @foreach ($students as $key => $student)
                                {{ $student->name }}
                                <option value="{{ $student->id }}">
                                    {{ $student->name }}
                                </option>
                            @endforeach
                        </x-UI.forms.select2>
                    </div>
                </div>
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                    </div>
                </div>
                <br>
                <br>
                <br>
                <div class="row card-details" style="display: none;">
                    <div class="col-6">
                        <label for="name">اسم الطالب:</label>
                        <p style="display: inline;" class="name"></p>
                    </div>
                    <div class="col-6">
                        <label for="class">الصف:</label>
                        <p style="display: inline;" class="class"></p>
                    </div>
                    <div class="col-6">
                        <label for="division">الشعبة:</label>
                        <p style="display: inline;" class="division"></p>
                    </div>
                    <div class="col-6">
                        <label for="gender">الجنس:</label>
                        <p style="display: inline;" class="gender"></p>
                    </div>
                    <div class="col-6">
                        <label for="address">العنوان:</label>
                        <p style="display: inline;" class="address"></p>
                    </div>
                    <div class="col-6">
                        <label for="sector">المنطقة:</label>
                        <p style="display: inline;" class="sector"></p>
                    </div>
                    <div class="col-6">
                        <label for="group">الوجبة:</label>
                        <p style="display: inline;" class="group"></p>
                    </div>
                    <div class="col-6">
                        <label for="deservedAmount">المبلغ المستحق:</label>
                        <p style="display: inline;" class="deservedAmount"></p>
                    </div>
                    <div class="col-6">
                        <label for="paidAmount">المبلغ المدفوع:</label>
                        <p style="display: inline;" class="paidAmount"></p>
                    </div>
                    <div class="col-6">
                        <label for="amountToBePaid">المبلغ المستحق للدفع:</label>
                        <p style="display: inline;" class="amountToBePaid"></p>
                    </div>
                </div>
                <br><br><br>
                <div class="table-responsive">
                    <table id="datatable" class="table table-bordered nowrap mt-5">
                        <thead>
                        <tr>
                            <th>{{__('datatable.receipts.receipt_num')}}</th>
                            <th>{{__('datatable.receipts.amount')}}</th>
                            <th>{{__('datatable.receipts.date')}}</th>
                            {{-- <th></th> --}}
                        </tr>
                        </thead>


                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @push('javascript')

        <script>

            let studentId = '';

            $(document).ready(function () {

                $('select[name="studentId"]').change(function (evt) {
                    studentId = this.value;
                    $('.card-details').css('display', 'none');
                    getAccountStatementOfStudent();
                });

            });

            function getAccountStatementOfStudent() {

                $.ajax({
                        url: '/reports/students/' + studentId + '/accountStatement',
                        type: 'GET',
                        success: function (response) {

                            if ( $.fn.DataTable.isDataTable('#datatable') ) {
                                $('#datatable').DataTable().destroy();
                            }

                            $('#datatable tbody').empty();

                            var myDatatable = $('#datatable').DataTable({
                                dom: 'Bfrtip',
                                language: {
                                    url: '../../assets/libs/datatables/translate.json'
                                },
                                // responsive: true,
                                iDisplayLength:50,
                                columnDefs: [{
                                    "defaultContent": "",
                                    "targets": "_all"
                                }],
                                buttons: [
                                    {
                                        extend: 'csv',
                                        text: 'تصدير كملف اكسل',
                                        className: 'btn btn-primary',
                                        bom: true,
                                    },
                                    {
                                        extend: 'print',
                                        text: 'طباعة',
                                        className: 'btn btn-secondary',
                                        bom: true,
                                        customize: function (win) {


                                            let name = $('.name').text();
                                            let cls = $('.class').text();
                                            let division = $('.division').text();
                                            let gender = $('.gender').text();
                                            let address = $('.address').text();
                                            let sector = $('.sector').text();
                                            let group = $('.group').text();
                                            let deservedAmount = $('.deservedAmount').text();
                                            let paidAmount = $('.paidAmount').text();
                                            let amountToBePaid = $('.amountToBePaid').text();

                                            $(win.document.body).prepend(`

                                                <div class="row">
                                                    <div class="col-6">
                                                        <label for="name">اسم الطالب:</label>
                                                        <p style="display: inline;" class="name">${name}</p>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="class">الصف:</label>
                                                        <p style="display: inline;" class="class">${cls}</p>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="division">الشعبة:</label>
                                                        <p style="display: inline;" class="division">${division}</p>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="gender">الجنس:</label>
                                                        <p style="display: inline;" class="gender">${gender}</p>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="address">العنوان:</label>
                                                        <p style="display: inline;" class="address">${address}</p>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="sector">المنطقة:</label>
                                                        <p style="display: inline;" class="sector">${sector}</p>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="group">الوجبة:</label>
                                                        <p style="display: inline;" class="group">${group}</p>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="deservedAmount">المبلغ المستحق:</label>
                                                        <p style="display: inline;" class="deservedAmount">${deservedAmount}</p>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="paidAmount">المبلغ المدفوع:</label>
                                                        <p style="display: inline;" class="paidAmount">${paidAmount}</p>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="amountToBePaid">المبلغ المستحق للدفع:</label>
                                                        <p style="display: inline;" class="amountToBePaid">${amountToBePaid}</p>
                                                    </div>

                                                </div>

                                            `).css('font-size', '20px')
                                        }
                                    }
                                ]
                            });

                            let receipts = response.data.receipts;

                            receipts.forEach(receipt => {

                                var rowNode = myDatatable.row.add([
                                    receipt.receiptNum,
                                    receipt.amount,
                                    receipt.date
                                ]).draw().node();

                            });

                            $('.name').text(response.data.name);
                            $('.class').text(response.data.class);
                            $('.division').text(response.data.division);
                            $('.gender').text(response.data.gender);
                            $('.address').text(response.data.address);
                            $('.sector').text(response.data.sector);
                            $('.group').text(response.data.group);
                            $('.deservedAmount').text(response.data.deservedAmount);
                            $('.paidAmount').text(response.data.paidAmount);
                            $('.amountToBePaid').text(response.data.amountToBePaid);

                            $('.card-details').css('display', 'flex');

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                        }
                    });

            }

        </script>

    @endpush


</x-layout.data-table>
