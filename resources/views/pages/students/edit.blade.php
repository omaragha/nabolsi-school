
<x-layout.form :title="__('sidebar.students.update')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{ route('students.update', ['student' => $item->id]) }}" data-parsley-validate novalidate autocomplete="off">
                    @method('put')
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="name">{{__('form.students.name')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="name" :value="old('name', $item['name'] ?? '')" :placeholder="__('form.students.name')" />
                        </div>
                        <div class="col-md-6">
                            <label for="father_name">{{__('form.students.father_name')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="father_name" :value="old('father_name', $item['father_name'] ?? '')" :placeholder="__('form.students.father_name')" type="text" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="mother_name">{{__('form.students.mother_name')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="mother_name" :value="old('mother_name', $item['mother_name'] ?? '')" :placeholder="__('form.students.mother_name')" />
                        </div>

                        <div class="form-group col-md-6">
                            <label for="class">{{__('form.students.class')}}*</label>
                            <x-UI.forms.select2 name='class' :placeholder="__('form.students.class')" :readonly="$readonly">
                                @foreach ($classes as $key => $class)
                                    <option value="{{$class}}" {{old('class',$item['class'] ?? '') == $class ? 'selected' : ''}}>
                                        {{$class}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="division">{{__('form.students.division')}}*</label>
                            <x-UI.forms.select2 name='division' :placeholder="__('form.students.division')" :readonly="$readonly">
                                @foreach ($divisions as $key => $division)
                                    <option value="{{$division}}" {{old('division',$item['division'] ?? '') == $division ? 'selected' : ''}}>
                                        {{$division}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>
                        {{-- <div class="col-md-6">
                            <label for="division">{{__('form.students.division')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="division" :value="old('division', $item['division'] ?? '')" :placeholder="__('form.students.division')" />
                        </div> --}}
                        <div class="col-md-6">
                            <label for="gender">{{__('form.students.gender')}}*</label>
                            <x-UI.forms.select2 name='gender' :placeholder="__('form.students.gender')" :readonly="$readonly">
                                @foreach ($genders as $key => $gender)
                                    <option value="{{$gender}}" {{old('gender',$item['gender'] ?? '') == $gender ? 'selected' : ''}}>
                                        {{$gender}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="phone">{{__('form.students.phone')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="phone" :value="old('phone', $item['phone'] ?? '')" :placeholder="__('form.students.phone')" />
                        </div>
                        <div class="col-md-6">
                            <label for="student_mobile">{{__('form.students.student_mobile')}}</label>
                            <x-UI.forms.input :readonly=$readonly name="student_mobile" :value="old('student_mobile', $item['student_mobile'] ?? '')" :placeholder="__('form.students.student_mobile')" :required="false"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="father_mobile">{{__('form.students.father_mobile')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="father_mobile" :value="old('father_mobile', $item['father_mobile'] ?? '')" :placeholder="__('form.students.father_mobile')" />
                        </div>
                        <div class="col-md-6">
                            <label for="mother_mobile">{{__('form.students.mother_mobile')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="mother_mobile" :value="old('mother_mobile', $item['mother_mobile'] ?? '')" :placeholder="__('form.students.mother_mobile')" />
                        </div>
                    </div>

                    <div class=" row">
                        <div class="form-group col-md-6">
                            <label for="address">{{__('form.students.address')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="address" :value="old('address', $item['address'] ?? '')" :placeholder="__('form.students.address')" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="sector_id">{{__('form.students.sector')}}*</label>
                            <x-UI.forms.select2 :required="false" name='sector_id' :placeholder="__('form.students.sector')" :readonly="$readonly">
                                @foreach ($sectors as $sector)
                                    <option value="{{$sector->id}}" {{old('sector_id',$item->sector_id ?? '') == $sector->id ? 'selected' : ''}}>
                                        {{$sector->name}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>
                    </div>


                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.edit',['attr'=>' ']) }}
                            </button>
                            @endif
                            <a  href="{{route('students.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
