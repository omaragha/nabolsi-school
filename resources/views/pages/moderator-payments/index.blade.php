<x-layout.data-table title="{{__('sidebar.moderator_payments.index')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                        @if (canAccess('store_moderator_payment'))
                            <a href="{{route('moderator_payments.create')}}" class="btn btn-success waves-effect  waves-light">
                                <i class="fas fa-plus"></i>
                                {{__('web.add',['attr'=>__('web.receipt')])}}
                            </a>
                        @endif
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="datatable" class="table table-bordered nowrap mt-5">
                        <thead>
                        <tr>
                            <th data-orderable="false">{{__('web.actions')}}</th>
                            <th>{{__('datatable.moderator_payments.moderator')}}</th>
                            <th>{{__('datatable.moderator_payments.amount')}}</th>
                            <th>{{__('datatable.moderator_payments.month')}}</th>
                            <th>{{__('datatable.moderator_payments.description')}}</th>
                            {{-- <th></th> --}}
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($items as $item)
                            <tr>
                                <td>
                                    @if (canAccess('show_moderator_payment'))
                                        <a href="{{route('moderator_payments.show',['moderator_payment'=>$item->id])}}" class="text-info btn-sm waves-light">
                                            <i class="far fa-eye" data-toggle="tooltip" data-placement="top" title="{{__('web.view')}}"></i>
                                            {{-- {{__('web.view')}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('update_moderator_payment'))
                                        <a href="{{route('moderator_payments.edit',['moderator_payment'=>$item->id])}}" class="text-warning btn-sm waves-light">
                                            <i class=" fas fa-edit" data-toggle="tooltip" data-placement="top" title="{{__('web.edit')}}"></i>
                                            {{-- {{__('web.edit')}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('delete_moderator_payment'))
                                        <a  href="javascript" onclick="$('#modal-form').attr('action','{{route('moderator_payments.destroy',['moderator_payment'=>$item->id])}}');
                                            $('#delete-name').text('{{$item->receipt_num}}')""  data-toggle="modal" data-target="#modal" class="text-danger btn-sm waves-light">
                                            <i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="{{__('web.delete',['attr'=>''])}}"></i>
                                            {{-- {{__('web.delete',['attr'=>''])}} --}}
                                        </a>
                                    @endif
                                </td>
                                <td>{{$item->moderator ? $item->moderator->name : ''}}</td>
                                <td>{{$item->amount}}</td>
                                <td>{{$item->month}}</td>
                                <td>{{$item->description}}</td>
                                {{-- <td></td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <x-UI.modal.scroll-modal :title="__('web.delete',['attr'=>__('web.receipt')])" :body="__('web.the_payment')"/>
</x-layout.data-table >
