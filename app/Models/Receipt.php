<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    use HasFactory;

    protected $fillable = ['receipt_num', 'amount', 'date', 'notes', 'student_id'];

    protected $relations = ['student'];

    public function student() {
        return $this->belongsTo(Student::class);
    }

}
