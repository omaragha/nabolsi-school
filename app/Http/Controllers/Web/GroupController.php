<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResourceController;
use App\Models\Bus;
use App\Models\Group;
use App\Models\GroupMonthlyValues;
use App\Models\Student;
use App\Traits\ResourceTrait;
use Illuminate\Http\Request;

class GroupController extends ResourceController
{
    // use ResourceTrait {
    //     ResourceTrait::__construct as private __tConstruct;
    // }

    public function __construct(Group $model) {
        parent::__construct($model);
        // $this->__tConstruct($model);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $type = [
            '1' => 'ذكور',
            '2' => 'إناث'
        ];

        $request->merge([
            'gender' => $type[strval($request->type)]
        ]);

        $rules = [
            'name' => 'required',
            'type' => 'required|in:' . implode(',', [1, 2]),
            'gender' => 'required|in:' . implode(',', ['ذكور', 'إناث']),
            'driver_id_am' => 'required|exists:buses,id',
            'driver_id_pm' => 'required|exists:buses,id',
            'moderator_id_am' => 'required_if:gender,إناث' . ($request->moderator_id_am ? '|exists:moderators,id' : ''),
            'moderator_id_pm' => 'required_if:gender,إناث' . ($request->moderator_id_pm ? '|exists:moderators,id' : ''),
            'sector_id' => 'required|exists:sectors,id',
            'student_cost' => 'required|numeric',
            'school_cost' => 'required|numeric'
        ];

        $request->validate($rules);

        $group = Group::create($request->all());

        GroupMonthlyValues::create(['group_id' => $group->id]);

        return redirect()->route('groups.index')->with('success', 'تم إضافة وجبة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'name' => 'required',
            'type' => 'required|in:' . implode(',', [1, 2]),
            'driver_id_am' => 'required|exists:buses,id',
            'driver_id_pm' => 'required|exists:buses,id',
            'moderator_id_am' => 'required|exists:moderators,id',
            'moderator_id_pm' => 'required|exists:moderators,id',
            'sector_id' => 'required|exists:sectors,id',
            'student_cost' => 'required|numeric',
            'school_cost' => 'required|numeric'
        ]);

        $group = Group::find($id);

        if (!$group) {
            return abort(404);
        }

        $group->update($request->all());

        return redirect()->route('groups.index')->with('success', 'تم تعديل الوجبة بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function getStudents($groupId) {

        $request = new Request();

        $request->merge([
            'group_id' => $groupId
        ]);

        $request->validate([
            'group_id' => 'required|exists:groups,id'
        ]);

        $student = new Student();

        $students = Student::with($student->getRelations())->where('group_id', $groupId)->get();

        $group = Group::find($groupId);

        return view('pages.groups.students.index', ['items' => $students, 'groupId' => $groupId, 'group' => $group]);
    }

    public function getStudentsOutsideGroup($groupId) {

        $request = new Request();

        $request->merge([
            'group_id' => $groupId
        ]);

        $request->validate([
            'group_id' => 'required|exists:groups,id'
        ]);

        $group = Group::find($groupId);

        $groupStudents = count(Student::where('group_id', $groupId)->get()->toArray());

        $busCapacity = Bus::where('id', $group->driver_id_am)->get()->first()->capacity;

        if ($groupStudents >= $busCapacity) {
            $message = 'لا يوجد شواغر في هذه الوجبة.';
            return sendError($message, 422);
        }

        $students = Student::with((new Student())->getRelations())
                    ->whereNull('group_id')
                    ->where('sector_id', $group->sector_id)
                    ->where('gender', $group->type == 1 ? 'ذكر' : 'أنثى')
                    ->get();

        $data = [
            'students'  => $students,
            'vacancies' => $busCapacity - $groupStudents
        ];

        return sendResponse($data, 'Getting students successfully.');
    }


    public function linkStudentWithGroup(Request $request, $groupId) {

        $request->validate([
            'students' => 'array',
            'students.*.' => 'exists:students,id'
        ]);

        $group = Group::find($groupId);

        $groupStudents = count(Student::where('group_id', $groupId)->get()->toArray());

        $busCapacity = Bus::where('id', $group->driver_id_am)->get()->first()->capacity;

        if ($groupStudents >= $busCapacity) {
            $message = 'لا يوجد شواغر في هذه الوجبة.';
            return sendError($message, 422);
        }

        $students = Student::whereIn('id', $request->students)->update([
            'group_id'    => $groupId,
            'actual_cost' => $group->student_cost
        ]);

        return sendResponse([], 'تم ربط الطالب بالوجبة بنجاح');
    }

    public function getStudentGroup($studentId) {

        $student = Student::find($studentId);

        if (!$student) {
            return sendError('Student NOT found!');
        }

        $groups = Group::with('students', 'driverAm')->where('sector_id', $student->sector_id)->where('type', $student->gender == 'ذكر' ? 1 : 2)->get()->map(function ($item) {
            $item->vacancies = $item->driverAm->capacity - count($item->students);
            return $item;
        });

        $data = [
            'student' => $student,
            'groups'  => $groups
        ];

        return sendResponse($data, 'Getting student successfully.');
    }

    public function updateStudentGroup(Request $request, $studentId) {

        $request->merge([
            'student_id' => $studentId
        ]);

        $request->validate([
            'group_id'   => 'required|exists:groups,id',
            'student_id' => 'required|exists:students,id'
        ]);

        $student = Student::find($studentId);

        $student->group_id = $request->group_id;

        $student->save();

        return sendResponse([], 'تم تعديل وجبة الطالب بنجاح.');
    }

    public function deleteStudentGroup($studentId) {

        $student = Student::find($studentId);

        if (!$student) {
            return sendError('Student NOT found!');
        }

        $student->group_id = null;

        $student->save();

        return redirect()->back()->with('success', 'تم حذف الطالب من هذه الوجبة.');
    }

}
