<?php

use App\Http\Controllers\Web\StudentController;
use Illuminate\Support\Facades\Route;

Route::resource('students', StudentController::class);

Route::get('upload', [StudentController::class, 'getFormUpload'])->name('students.importForm');
Route::post('upload', [StudentController::class, 'upload'])->name('students.import');
