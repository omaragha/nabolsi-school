<x-layout.data-table title="{{__('sidebar.buses.index')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                        @if (canAccess('store_bus'))
                            <a href="{{route('buses.create')}}" class="btn btn-success waves-effect  waves-light">
                                <i class="fas fa-plus"></i>
                                {{__('web.add',['attr'=>__('web.bus')])}}
                            </a>
                        @endif
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="datatable" class="table table-bordered nowrap mt-5">
                        <thead>
                        <tr>
                            <th data-orderable="false">{{__('web.actions')}}</th>
                            <th>{{__('datatable.buses.driver_name')}}</th>
                            <th>{{__('datatable.buses.bus_num')}}</th>
                            <th>{{__('datatable.buses.driver_mobile')}}</th>
                            <th>{{__('datatable.buses.capacity')}}</th>
                            <th>{{__('datatable.buses.created_at')}}</th>
                            {{-- <th></th> --}}
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($items as $item)
                            <tr>
                                <td>
                                    @if (canAccess('show_bus'))
                                        <a href="{{route('buses.show',['bus'=>$item->id])}}" class="text-info btn-sm waves-light">
                                            <i class="far fa-eye" data-toggle="tooltip" data-placemnet="top" title="{{__('web.view')}}"></i>
                                            {{-- {{__('web.view')}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('update_bus'))
                                        <a href="{{route('buses.edit',['bus'=>$item->id])}}" class="text-warning btn-sm waves-light">
                                            <i class=" fas fa-edit" data-toggle="tooltip" data-placemnet="top" title="{{__('web.edit')}}"></i>
                                            {{-- {{__('web.edit')}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('delete_bus'))
                                        <a  href="javascript" onclick="$('#modal-form').attr('action','{{route('buses.destroy',['bus'=>$item->id])}}');
                                            $('#delete-name').text('{{$item->driver_name}}')""  data-toggle="modal" data-target="#modal" class="text-danger btn-sm waves-light">
                                            <i class="fas fa-trash" data-toggle="tooltip" data-placemnet="top" title="{{__('web.delete',['attr'=>''])}}"></i>
                                            {{-- {{__('web.delete',['attr'=>''])}} --}}
                                        </a>
                                    @endif
                                </td>
                                <td>{{$item->driver_name}}</td>
                                <td>{{$item->bus_num}}</td>
                                <td>{{$item->driver_mobile}}</td>
                                <td>{{$item->capacity}}</td>
                                <td>{{$item->created_at}}</td>
                                {{-- <td></td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <x-UI.modal.scroll-modal :title="__('web.delete',['attr'=>__('web.bus')])" :body="__('web.the_bus')"/>
</x-layout.data-table >
