<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = ['receipt_num', 'date', 'amount', 'description', 'contractor_id'];

    protected $relations = ['contractor'];

    public function contractor() {
        return $this->belongsTo(Contractor::class);
    }

}
