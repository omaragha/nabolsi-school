<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResourceController;
use App\Models\Bus;
use App\Models\Group;
use App\Models\Student;
use App\Traits\ResourceTrait;
use Illuminate\Http\Request;

class BusController extends ResourceController
{
    // use ResourceTrait {
    //     ResourceTrait::__construct as private __tConstruct;
    // }

    public function __construct(Bus $model) {
        parent::__construct($model);
        // $this->__tConstruct($model);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'driver_name' => 'required',
            'bus_num' => 'required|unique:buses,bus_num',
            'driver_mobile' => 'required|unique:buses,driver_mobile',
            'capacity' => 'required|numeric'
        ]);

        $bus = Bus::create($request->all());

        return redirect()->route('buses.index')->with('success', 'تم إضافة حافلة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'driver_name' => 'required',
            'bus_num' => 'required|unique:buses,bus_num,' . $id . ',id',
            'driver_mobile' => 'required|unique:buses,driver_mobile,' . $id,
            'capacity' => 'required|numeric'
        ]);

        $groupAm = Group::where('driver_id_am', $id)->get()->first();
        $groupPm = Group::where('driver_id_pm', $id)->get()->first();

        $studentsNumberGroupAm = PHP_INT_MIN;
        $studentsNumberGroupPm = PHP_INT_MIN;

        if ($groupAm) {
            $studentsNumberGroupAm = max(count(Student::where('group_id', $groupAm->id)->get()->toArray()), PHP_INT_MIN);
        }

        if ($groupPm) {
            $studentsNumberGroupPm = max(count(Student::where('group_id', $groupPm->id)->get()->toArray()), PHP_INT_MIN);
        }

        if ($request->capacity < max($studentsNumberGroupAm, $studentsNumberGroupPm)) {
            return redirect()->back()->withErrors('لا يمكن تعديل سعة الحافلة لرقم أصغر من عدد الطلاب الحاليين للوجبة.');
        }

        $bus = Bus::find($id);

        if (!$bus) {
            return abort(404);
        }

        $bus->update($request->all());

        return redirect()->route('buses.index')->with('success', 'تم تعديل الحافلة بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function getPmVacancyBuses($driverId) {

        $bus = Bus::find($driverId);

        if (!$bus) {
            return sendError('Bus NOT found!');
        }

        $busyDriversPm = Group::where('driver_id_pm', '!=', NULL)->get()->pluck('driver_id_pm')->toArray();

        $buses = Bus::whereNotIn('id', $busyDriversPm)->where('capacity', $bus->capacity)->get();

        return sendResponse($buses, 'Getting buses successfully.');
    }

}
