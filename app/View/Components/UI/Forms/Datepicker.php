<?php

namespace App\View\Components\UI\Forms;

use Illuminate\View\Component;

class Datepicker extends Component
{
    public $name;
    public $value;
    public $readonly;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $value, $readonly = false)
    {
        $this->name = $name;
        $this->value = $value;
        $this->readonly = $readonly;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.UI.forms.datepicker');
    }
}
