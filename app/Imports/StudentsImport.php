<?php

namespace App\Imports;

use App\Models\Student;
use Maatwebsite\Excel\Concerns\ToModel;

class StudentsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Student([
            'name' => $row['اسم الطالب'] ?? '',
            'father_name' => $row['اسم الأب'] ?? '',
            'mother_name' => $row['اسم الأم'] ?? '',
            'class' => $row['الصف'] ?? '',
            'division' => $row['الشعبة'] ?? '',
            'gender' => $row['الجنس'] ?? '',
            'phone' => $row['رقم الهاتف'] ?? '',
            'student_mobile' => $row['رقم موبايل الطالب'] ?? '',
            'father_mobile' => $row['رقم موبايل الأب'] ?? '',
            'mother_mobile' => $row['رقم موبايل الأم'] ?? '',
            'address' => $row['العنوان'] ?? '',
        ]);
    }
}
