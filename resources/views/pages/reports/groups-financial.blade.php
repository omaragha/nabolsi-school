<x-layout.data-table title="{{__('sidebar.groups.index')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                    </div>
                </div>
                <div class="table-responsive">
                    {{-- <p>omar agha</p> --}}
                    <table id="datatable" class="table table-bordered nowrap mt-5">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('datatable.groups.name')}}</th>
                            <th>{{__('datatable.groups.annual_cost')}}</th>
                            <th>{{__('datatable.groups.students_receipts')}}</th>
                            <th>{{__('datatable.groups.rest_amount')}}</th>
                            {{-- <th></th> --}}
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($groups as $key => $group)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $group['name'] }}</td>
                                    <td>{{ $group['annualCost'] }}</td>
                                    <td>{{ $group['totalPayments'] }}</td>
                                    <td>{{ $group['restAmount'] }}</td>
                                    {{-- <td></td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <x-UI.modal.scroll-modal :title="__('web.delete',['attr'=>__('web.user')])" :body="__('web.the_user')"/>

    @push('javascript')

        <script>

        </script>

    @endpush


</x-layout.data-table>
