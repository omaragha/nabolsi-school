<?php

namespace App\View\Components\UI\LiftSidebar;

use Illuminate\View\Component;

class Link extends Component
{
    public $url;
    public $title;
    public $icon;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($url, $title, $icon)
    {
        $this->url = $url;
        $this->title = $title;
        $this->icon = $icon;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.UI.lift-sidebar.link');
    }
}
