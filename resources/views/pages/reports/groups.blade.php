<x-layout.data-table title="{{__('sidebar.groups.index')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="form-group row">
                    <div class="col-sm-6">
                        <label for="studentId">{{__('sidebar.reports.studentsNumber')}}</label>
                        <input class="form-control" type="text" readonly value="{{ $data['studentsNumber'] }}">
                    </div>
                </div>
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                    </div>
                </div>
                <div class="table-responsive">
                    {{-- <p>omar agha</p> --}}
                    <table id="datatable" class="table table-bordered nowrap mt-5">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('datatable.groups.name')}}</th>
                            <th>{{__('datatable.groups.sector')}}</th>
                            <th>{{__('datatable.groups.driver_am')}}</th>
                            <th>{{__('datatable.groups.driver_pm')}}</th>
                            <th>{{__('datatable.groups.moderator_am')}}</th>
                            <th>{{__('datatable.groups.moderator_pm')}}</th>
                            <th>{{__('datatable.groups.type')}}</th>
                            <th>{{__('datatable.groups.students_count')}}</th>
                            {{-- <th></th> --}}
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($data['groups'] as $key => $group)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $group['name'] }}</td>
                                    <td>{{ $group['sector'] }}</td>
                                    <td>{{ $group['driverAm'] }}</td>
                                    <td>{{ $group['driverPm'] }}</td>
                                    <td>{{ $group['moderatorAm'] }}</td>
                                    <td>{{ $group['moderatorPm'] }}</td>
                                    <td>{{ $group['type'] }}</td>
                                    <td>{{ count($group['students']) }}</td>
                                    {{-- <td></td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <x-UI.modal.scroll-modal :title="__('web.delete',['attr'=>__('web.user')])" :body="__('web.the_user')"/>

    @push('javascript')

        <script>

        </script>

    @endpush


</x-layout.data-table>
