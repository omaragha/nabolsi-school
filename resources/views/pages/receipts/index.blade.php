<x-layout.data-table title="{{__('sidebar.receipts.index')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                        @if (canAccess('store_receipt'))
                            <a href="{{route('receipts.create')}}" class="btn btn-success waves-effect  waves-light">
                                <i class="fas fa-plus"></i>
                                {{__('web.add',['attr'=>__('web.receipt')])}}
                            </a>
                        @endif
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="datatable" class="table table-bordered nowrap mt-5">
                        <thead>
                        <tr>
                            <th data-orderable="false">{{__('web.actions')}}</th>
                            <th>{{__('datatable.receipts.student')}}</th>
                            <th>{{__('datatable.receipts.receipt_num')}}</th>
                            <th>{{__('datatable.receipts.amount')}}</th>
                            <th>{{__('datatable.receipts.date')}}</th>
                            <th>{{__('datatable.receipts.notes')}}</th>
                            {{-- <th></th> --}}
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($items as $item)
                            <tr>
                                <td>
                                    @if (canAccess('show_receipt'))
                                        <a href="{{route('receipts.show',['receipt'=>$item->id])}}" class="text-info btn-sm waves-light">
                                            <i class="far fa-eye" data-toggle="tooltip" data-placement="top" title="{{__('web.view')}}"></i>
                                            {{-- {{__('web.view')}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('update_receipt'))
                                        <a href="{{route('receipts.edit',['receipt'=>$item->id])}}" class="text-warning btn-sm waves-light">
                                            <i class=" fas fa-edit" data-toggle="tooltip" data-placement="top" title="{{__('web.edit')}}"></i>
                                            {{-- {{__('web.edit')}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('delete_receipt'))
                                        <a  href="javascript" onclick="$('#modal-form').attr('action','{{route('receipts.destroy',['receipt'=>$item->id])}}');
                                            $('#delete-name').text('{{$item->receipt_num}}')""  data-toggle="modal" data-target="#modal" class="text-danger btn-sm waves-light">
                                            <i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="{{__('web.delete',['attr'=>''])}}"></i>
                                            {{-- {{__('web.delete',['attr'=>''])}} --}}
                                        </a>
                                    @endif
                                </td>
                                <td>{{$item->student ? $item->student->name : ''}}</td>
                                <td>{{$item->receipt_num}}</td>
                                <td>{{$item->amount}}</td>
                                <td>{{$item->date}}</td>
                                <td>{{$item->notes}}</td>
                                {{-- <td></td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <x-UI.modal.scroll-modal :title="__('web.delete',['attr'=>__('web.receipt')])" :body="__('web.the_receipt')"/>
</x-layout.data-table >
