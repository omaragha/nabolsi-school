<?php

use App\Models\Bus;
use App\Models\Contractor;
use App\Models\Group;
use App\Models\Moderator;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Sector;
use App\Models\Student;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


if (!function_exists('getEnumOptions')) {
    function getEnumOptions($table, $field)
    {
        $type = DB::select(DB::raw("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'"))[0]->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }
}

if (!function_exists('fixingName')) {
    function fixingName($name, $delimiter = '-')
    {

        $fixedName = '';

        $name = str_split($name);

        foreach ($name as $key => $value) {
            if (ctype_upper($value) && $key != 0) {
                $fixedName .= ($delimiter . strtolower($value));
            } else {
                $fixedName .= strtolower($value);
            }
        }

        return $fixedName;
    }
}

if (!function_exists('canAccess')) {

    function canAccess($permission)
    {

        $permissions = auth()->user()->role->permissions()->pluck('guard_name')->toArray();

        return in_array($permission, $permissions);
    }
}

if (!function_exists('getModels')) {

    function getModels()
    {
        $models = [];

        if ($handle = opendir('../app/Models')) {

            /* This is the correct way to loop over the directory. */
            while (false !== ($entry = readdir($handle))) {
                if ($entry == '.' || $entry == '..') continue;
                $models[] = strtolower(fixingName(explode('.', $entry)[0], '_'));
            }

            closedir($handle);
        }

        return $models;
    }
}

if (!function_exists('getFunctionsDependModel')) {

    function getFunctionsDependModel()
    {

        $globalCallbackFunctions = [
            'getEnumOptions' => function ($table, $field) {
                $type = DB::select(DB::raw("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'"))[0]->Type;
                preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
                $enum = explode("','", $matches[1]);
                return $enum;
            },
            'roles' => function () {
                return Role::all();
            },
            'permissions' => function () {
                return Permission::all()->groupBy('group_by');
            },
            'driversAm' => function ($id = null) {

                $driverId = null;

                $group = Group::find($id);

                if ($group) {
                    $driverId = $group->driver_id_am;
                }

                $busyDriversAm = Group::where('driver_id_am', '!=', NULL)->where('driver_id_am', '!=', $driverId)->get()->pluck('driver_id_am')->toArray();

                $driversAm = Bus::whereNotIn('id', $busyDriversAm)->get();

                return $driversAm;
            },
            'driversPm' => function ($id = null) {

                $driverId = null;

                $group = Group::find($id);

                if ($group) {
                    $driverId = $group->driver_id_pm;
                }

                $busyDriversPm = Group::where('driver_id_pm', '!=', NULL)->where('driver_id_pm', '!=', $driverId)->get()->pluck('driver_id_pm')->toArray();

                $driversPm = Bus::whereNotIn('id', $busyDriversPm)->get();

                return $driversPm;
            },
            'moderators' => function () {
                return Moderator::all();
            },
            'moderatorsAm' => function ($id = null) {

                $moderatorId = null;

                $group = Group::find($id);

                if ($group) {
                    $moderatorId = $group->moderator_id_am;
                }

                $busyModeratorsAm = Group::where('moderator_id_am', '!=', NULL)->where('moderator_id_am', '!=', $moderatorId)->get()->pluck('moderator_id_am')->toArray();

                $moderatorsAm = Moderator::whereNotIn('id', $busyModeratorsAm)->get();

                return $moderatorsAm;
            },
            'moderatorsPm' => function ($id = null) {

                $moderatorId = null;

                $group = Group::find($id);

                if ($group) {
                    $moderatorId = $group->moderator_id_pm;
                }

                $busyModeratorsPm = Group::where('moderator_id_pm', '!=', NULL)->where('moderator_id_pm', '!=', $moderatorId)->get()->pluck('moderator_id_pm')->toArray();

                $moderatorsPm = Moderator::whereNotIn('id', $busyModeratorsPm)->get();

                return $moderatorsPm;
            },
            'sectors' => function () {
                return Sector::all();
            },
            'contractors' => function () {
                return Contractor::all();
            },
            'students' => function ($conditions = []) {
                return Student::where($conditions)->get();
            },
            'groups' => function () {
                return Group::all();
            }
        ];

        $callFunctionsDependModel = [
            'User' => [
                'roles' => [
                    'method' => $globalCallbackFunctions['roles'],
                    'params' => []
                ]
            ],
            'Group' => [
                'driversAm' => [
                    'method' => $globalCallbackFunctions['driversAm'],
                    'params' => []
                ],
                'driversPm' => [
                    'method' => $globalCallbackFunctions['driversPm'],
                    'params' => []
                ],
                'moderatorsAm' => [
                    'method' => $globalCallbackFunctions['moderatorsAm'],
                    'params' => []
                ],
                'moderatorsPm' => [
                    'method' => $globalCallbackFunctions['moderatorsPm'],
                    'params' => []
                ],
                'sectors' => [
                    'method' => $globalCallbackFunctions['sectors'],
                    'params' => []
                ],
            ],
            'Payment' => [
                'contractors' => [
                    'method' => $globalCallbackFunctions['contractors'],
                    'params' => []
                ]
            ],
            'Receipt' => [
                'students' => [
                    'method' => $globalCallbackFunctions['students'],
                    'params' => [[['group_id', '!=', NULL]]]
                ]
            ],
            'Student' => [
                'classes' => [
                    'method' => $globalCallbackFunctions['getEnumOptions'],
                    'params' => ['students', 'class']
                ],
                'genders' => [
                    'method' => $globalCallbackFunctions['getEnumOptions'],
                    'params' => ['students', 'gender']
                ],
                'sectors' => [
                    'method' => $globalCallbackFunctions['sectors'],
                    'params' => []
                ],
                'divisions' => [
                    'method' => $globalCallbackFunctions['getEnumOptions'],
                    'params' => ['students', 'division']
                ]
            ],
            'Role' => [
                'permissions' => [
                    'method' => $globalCallbackFunctions['permissions'],
                    'params' => []
                ]
            ],
            'GroupMonthlyValues' => [
                'sectors' => [
                    'method' => $globalCallbackFunctions['sectors'],
                    'params' => []
                ],
                'groups' => [
                    'method' => $globalCallbackFunctions['groups'],
                    'params' => []
                ]
            ],
            'ModeratorPayment' => [
                'moderators' => [
                    'method' => $globalCallbackFunctions['moderators'],
                    'params' => []
                ],
                'months' => [
                    'method' => $globalCallbackFunctions['getEnumOptions'],
                    'params' => ['moderator_payments', 'month']
                ]
            ]
        ];

        return $callFunctionsDependModel;
    }
}


if (!function_exists('sendResponse')) {

    function sendResponse($data = null, $message = '', $code = 200)
    {

        $response = [
            'data' => $data,
            'message' => $message,
            'code' => $code
        ];

        return response()->json($response, $code);
    }
}

if (!function_exists('sendError')) {

    function sendError($message = '', $code = 500)
    {

        $response = [
            'message' => $message,
            'code' => $code
        ];

        return response()->json($response, $code);
    }
}
