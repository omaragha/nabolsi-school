
<x-layout.form :title="__('sidebar.buses.update')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{ route('buses.update', ['bus' => $item->id]) }}" data-parsley-validate novalidate autocomplete="off">
                    @method('put')
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="driver_name">{{__('form.buses.driver_name')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="driver_name" :value="old('driver_name', $item['driver_name'] ?? '')" :placeholder="__('form.buses.driver_name')" />
                        </div>
                        <div class="col-md-6">
                            <label for="bus_num">{{__('form.buses.bus_num')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="bus_num" :value="old('bus_num', $item['bus_num'] ?? '')" :placeholder="__('form.buses.bus_num')" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="driver_mobile">{{__('form.buses.driver_mobile')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="driver_mobile" :value="old('driver_mobile', $item['driver_mobile'] ?? '')" :placeholder="__('form.buses.driver_mobile')" />
                        </div>
                        <div class="col-md-6">
                            <label for="capacity">{{__('form.buses.capacity')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="capacity" :value="old('capacity', $item['capacity'] ?? '')" :placeholder="__('form.buses.capacity')" type="number" />
                        </div>
                    </div>

                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.edit',['attr'=>' ']) }}
                            </button>
                            @endif
                            <a  href="{{route('buses.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
