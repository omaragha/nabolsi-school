
<x-layout.form :title="__('sidebar.group_monthly_values.update')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{ route('group_monthly_values.update', ['group_monthly_value' => $item->id]) }}" data-parsley-validate novalidate autocomplete="off">
                    @method('put')
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="sep">{{__('form.group_monthly_values.sep')}}</label>
                            <x-UI.forms.input :required="false" :readonly=$readonly name="sep" :value="old('sep', $item['sep'] ?? '')" :placeholder="__('form.group_monthly_values.sep')" type="number" />
                        </div>
                        <div class="col-md-6">
                            <label for="oct">{{__('form.group_monthly_values.oct')}}</label>
                            <x-UI.forms.input :required="false" :readonly=$readonly name="oct" :value="old('oct', $item['oct'] ?? '')" :placeholder="__('form.group_monthly_values.oct')" type="number" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="nov">{{__('form.group_monthly_values.nov')}}</label>
                            <x-UI.forms.input :required="false" :readonly=$readonly name="nov" :value="old('nov', $item['nov'] ?? '')" :placeholder="__('form.group_monthly_values.nov')" type="number" />
                        </div>
                        <div class="col-md-6">
                            <label for="dec">{{__('form.group_monthly_values.dec')}}</label>
                            <x-UI.forms.input :required="false" :readonly=$readonly name="dec" :value="old('dec', $item['dec'] ?? '')" :placeholder="__('form.group_monthly_values.dec')" type="number" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="jan">{{__('form.group_monthly_values.jan')}}</label>
                            <x-UI.forms.input :required="false" :readonly=$readonly name="jan" :value="old('jan', $item['jan'] ?? '')" :placeholder="__('form.group_monthly_values.jan')" type="number" />
                        </div>
                        <div class="col-md-6">
                            <label for="feb">{{__('form.group_monthly_values.feb')}}</label>
                            <x-UI.forms.input :required="false" :readonly=$readonly name="feb" :value="old('feb', $item['feb'] ?? '')" :placeholder="__('form.group_monthly_values.feb')" type="number" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="mar">{{__('form.group_monthly_values.mar')}}</label>
                            <x-UI.forms.input :required="false" :readonly=$readonly name="mar" :value="old('mar', $item['mar'] ?? '')" :placeholder="__('form.group_monthly_values.mar')" type="number" />
                        </div>
                        <div class="col-md-6">
                            <label for="apr">{{__('form.group_monthly_values.apr')}}</label>
                            <x-UI.forms.input :required="false" :readonly=$readonly name="apr" :value="old('apr', $item['apr'] ?? '')" :placeholder="__('form.group_monthly_values.apr')" type="number" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="may">{{__('form.group_monthly_values.may')}}</label>
                            <x-UI.forms.input :required="false" :readonly=$readonly name="may" :value="old('may', $item['may'] ?? '')" :placeholder="__('form.group_monthly_values.may')" type="number" />
                        </div>
                        <div class="col-md-6">
                            <label for="group_id">{{__('form.group_monthly_values.group')}}</label>
                            <x-UI.forms.select2 name='group_id' :placeholder="__('form.group_monthly_values.group')" :readonly="$readonly">
                                @foreach ($groups as $group)
                                    <option value="{{$group->id}}" {{old('group_id',$item->group_id ?? '') == $group->id ? 'selected' : ''}}>
                                        {{$group->name}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>
                    </div>

                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.edit',['attr'=>' ']) }}
                            </button>
                            @endif
                            <a  href="{{route('group_monthly_values.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
