<?php

use App\Http\Controllers\Web\GroupController;
use Illuminate\Support\Facades\Route;

Route::resource('groups', GroupController::class);

Route::get('groups/{id}/students', [GroupController::class, 'getStudents'])->name('group.students');
Route::get('groups/{id}/students/outside', [GroupController::class, 'getStudentsOutsideGroup']);
Route::put('groups/{id}/students', [GroupController::class, 'linkStudentWithGroup']);
Route::get('groups/students/{studentId}', [GroupController::class, 'getStudentGroup']);
Route::put('groups/students/{studentId}', [GroupController::class, 'updateStudentGroup']);
Route::delete('groups/students/{studentId}', [GroupController::class, 'deleteStudentGroup'])->name('groups.deleteStudentGroup');

// 
