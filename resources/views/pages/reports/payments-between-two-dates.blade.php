<x-layout.data-table title="{{__('sidebar.reports.paymentsAccount')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="form-group row">
                    <div class="col-sm-6">
                        <label>اختر التاريخ</label>
                        <input class="form-control input-daterange-datepicker" type="text" name="daterange" value="" readonly/>
                    </div>
                    <div class="col-sm-6">
                        <label>المجموع الإجمالي</label>
                        <input class="form-control" type="text" name="totalAmount" value="{{ $data['total'] }}" readonly/>
                    </div>
                    {{-- <div class="col-xl-3 col-sm-6">
                        <div class="card-box widget-user">
                            <div class="text-center">
                                <h2 class="font-weight-normal text-primary" data-plugin="counterup">6599</h2>
                                <h5>Statistics</h5>
                            </div>
                        </div>
                    </div> --}}

                </div>
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="datatable" class="table table-bordered nowrap mt-5">
                        <thead>
                        <tr>
                            <th>{{__('datatable.receipts.day')}}</th>
                            <th>{{__('datatable.receipts.amount')}}</th>
                            {{-- <th></th> --}}
                        </tr>
                        </thead>


                        <tbody>

                            @foreach ($data['receipts'] as $date => $amount)
                                <tr>
                                    <td>{{ $date }}</td>
                                    <td>{{ $amount }}</td>
                                    {{-- <td></td> --}}
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @push('javascript')

        <script>

            $(document).ready(function () {

                customOrder($('#datatable').DataTable(), 0, 'desc');

                let dateRangeInput = $('.input-daterange-datepicker');

                // init date range picker.
                dateRangeInput.daterangepicker(
                    {
                        locale: {
                            format: 'YYYY/MM/DD'
                        },
                        autoApply: true,
                    }
                );

                // initialize input.
                dateRangeInput.val('');

                $('.input-daterange-datepicker').change(function (evt) {

                    let start = this.value.split('-')[0];

                    let end = this.value.split('-')[1];

                    getPayments(start, end);

                });

                function customOrder(datatable, col = 0, type = 'asc') {

                    datatable.order()[0][0] = col;
                    datatable.order()[0][1] = type;

                }

                function getPayments(start, end) {

                    $.ajax({
                        url: '/reports/receipts?start=' + start + '&end=' + end,
                        type: 'GET',
                        success: function (response) {

                            if ( $.fn.DataTable.isDataTable('#datatable') ) {
                                $('#datatable').DataTable().destroy();
                            }

                            $('#datatable tbody').empty();

                            var myDatatable = $('#datatable').DataTable({
                                dom: 'Bfrtip',
                                order: [
                                    [0, 'desc']
                                ],
                                language: {
                                    url: '../../assets/libs/datatables/translate.json'
                                },
                                // responsive: true,
                                iDisplayLength:50,
                                columnDefs: [{
                                    "defaultContent": "",
                                    "targets": "_all"
                                }],
                                buttons: [
                                    {
                                        extend: 'csv',
                                        text: 'تصدير كملف اكسل',
                                        className: 'btn btn-primary',
                                        bom: true,
                                    },
                                    {
                                        extend: 'print',
                                        text: 'طباعة',
                                        className: 'btn btn-secondary',
                                        bom: true,
                                    }
                                ]
                            });

                            let receipts = response.data.receipts;

                            $('input[name="totalAmount"]').val(response.data.total);

                            for(let x in receipts) {

                                var rowNode = myDatatable.row.add([
                                    x,
                                    receipts[x]
                                ]).draw().node();

                            }

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                        }
                    });


                }

            });

        </script>

    @endpush


</x-layout.data-table>
