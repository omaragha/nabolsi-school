<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResourceController;
use App\Models\GroupMonthlyValues;
use App\Traits\ResourceTrait;
use Illuminate\Http\Request;

class GroupMonthlyValueController extends ResourceController
{
    public function __construct(GroupMonthlyValues $model) {
        parent::__construct($model);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'group_id' => 'required|exists:groups,id'
        ]);

        $groupMonthlyValue = GroupMonthlyValues::find($id);

        if (!$groupMonthlyValue) {
            return abort(404);
        }

        $groupMonthlyValue->update($request->all());

        return redirect()->route('group_monthly_values.index')->with('success', 'تم تعديل القيم الشهرية للوجبة بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}
