<?php

use App\Http\Controllers\Web\ModeratorController;
use Illuminate\Support\Facades\Route;

Route::resource('moderators', ModeratorController::class);

