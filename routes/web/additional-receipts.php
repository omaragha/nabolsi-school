<?php

use App\Http\Controllers\Web\AdditionalReceiptController;
use Illuminate\Support\Facades\Route;

Route::resource('additional_receipts', AdditionalReceiptController::class);

