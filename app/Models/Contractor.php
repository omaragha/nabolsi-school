<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contractor extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'mobile1', 'mobile2'];

    protected $relations = ['payments'];

    public function payments() {
        return $this->hasMany(Payment::class);
    }

}
