<?php

namespace App\View\Components\UI\Forms;

use Illuminate\View\Component;

class Input extends Component
{
    public $type;
    public $name;
    public $placeholder;
    public $value;
    public $readonly;
    public $required;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($readonly = false , $type = 'text',$name,$placeholder ='',$value = '',$required = true)
    {
        $this->readonly = $readonly;
        $this->type =$type;
        $this->value = $value;
        $this->placeholder = $placeholder;
        $this->name = $name;
        $this->required = $required;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.UI.forms.input');
    }
}
