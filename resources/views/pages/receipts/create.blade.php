
<x-layout.form :title="$readonly ? __('sidebar.receipts.show') : __('sidebar.receipts.add')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{ route('receipts.store') }}" data-parsley-validate novalidate autocomplete="off">
                    @if ($readonly)
                        @method('get')
                    @endif
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="receipt_num">{{__('form.receipts.receipt_num')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="receipt_num" :value="$readonly ? $item['receipt_num'] : old('receipt_num')" :placeholder="__('form.receipts.receipt_num')" />
                        </div>
                        <div class="col-md-6">
                            <label for="amount">{{__('form.receipts.amount')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="amount" :value="$readonly ? $item['amount'] : old('amount')" :placeholder="__('form.receipts.amount')" type="number" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="date">{{__('form.receipts.date')}}*</label>
                            <x-UI.forms.datepicker :readonly="$readonly" name="date" :value="old('date',$item['date']??'')"></x-UI.forms.datepicker>
                        </div>

                        <div class="col-md-6">
                            <label for="student_id">{{__('form.receipts.student')}}*</label>
                            <x-UI.forms.select2 name='student_id' :placeholder="__('form.receipts.student')" :readonly="$readonly">
                                @foreach ($students as $student)
                                    <option value="{{$student->id}}" {{old('student_id',$item->student_id ?? '') == $student->id ? 'selected' : ''}}>
                                        {{$student->name}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="notes">{{__('form.receipts.notes')}}</label>
                            <x-UI.forms.textarea :readonly=$readonly name="notes" :value="$readonly ? $item['notes'] : old('notes')" :placeholder="__('form.receipts.notes')" :readonly="$readonly"/>
                        </div>
                    </div>

                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.add',['attr'=>' ']) }}
                            </button>
                            @endif
                            <a  href="{{route('receipts.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
