
<x-layout.form :title="$readonly ? __('sidebar.payments.show') : __('sidebar.payments.add')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{ route('payments.store') }}" data-parsley-validate novalidate autocomplete="off">
                    @if ($readonly)
                        @method('get')
                    @endif
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="receipt_num">{{__('form.payments.receipt_num')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="receipt_num" :value="$readonly ? $item['receipt_num'] : old('receipt_num')" :placeholder="__('form.payments.receipt_num')" />
                        </div>
                        <div class="col-md-6">
                            <label for="amount">{{__('form.payments.amount')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="amount" :value="$readonly ? $item['amount'] : old('amount')" :placeholder="__('form.payments.amount')" type="number" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="date">{{__('form.payments.date')}}*</label>
                            <x-UI.forms.datepicker :readonly="$readonly" name="date" :value="old('date',$item['date']??'')"></x-UI.forms.datepicker>
                        </div>
                        <div class="col-md-6">
                            <label for="contractor_id">{{__('form.payments.contractor')}}*</label>
                            <x-UI.forms.select2 name='contractor_id' :placeholder="__('form.payments.contractor')" :readonly="$readonly">
                                @foreach ($contractors as $key => $contractor)
                                    <option value="{{$contractor->id}}" {{old('contractor_id',$item['contractor_id'] ?? '') == $contractor->id ? 'selected' : ''}}>
                                        {{$contractor->name}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="description">{{__('form.payments.description')}}</label>
                            <x-UI.forms.textarea :readonly=$readonly name="description" :value="$readonly ? $item['description'] : old('description')" :placeholder="__('form.payments.description')" :readonly="$readonly"/>
                        </div>
                    </div>

                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.add',['attr'=>' ']) }}
                            </button>
                            @endif
                            <a  href="{{route('payments.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
