
<x-layout.form :title="__('sidebar.groups.update')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{ route('groups.update', ['group' => $item->id]) }}" data-parsley-validate novalidate autocomplete="off">
                    @method('put')
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="name">{{__('form.groups.name')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="name" :value="old('name', $item['name'] ?? '')" :placeholder="__('form.groups.name')" />
                        </div>
                        <div class="col-md-6">
                            <label for="type">{{__('form.groups.type')}}*</label>
                            <x-UI.forms.select2 name='type' :placeholder="__('form.groups.type')" :readonly="$readonly">
                                <option value="1" {{ old('type', $item->type ?? '') == 1 ? 'selected' : '' }}>ذكور</option>
                                <option value="2" {{ old('type', $item->type ?? '') == 2 ? 'selected' : '' }}>إناث</option>
                            </x-UI.forms.select2>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="driver_id_am">{{__('form.groups.driver_am')}}*</label>
                            <x-UI.forms.select2 name='driver_id_am' :placeholder="__('form.groups.driver_am')" :readonly="$readonly">
                                @foreach ($driversAm as $driver)
                                    <option value="{{$driver->id}}" {{old('driver_id_am',$item->driver_id_am ?? '') == $driver->id ? 'selected' : ''}}>
                                        {{$driver->driver_name}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>
                        <div class="col-md-6">
                            <label for="driver_id_pm">{{__('form.groups.driver_pm')}}*</label>
                            <x-UI.forms.select2 name='driver_id_pm' :placeholder="__('form.groups.driver_pm')" :readonly="$readonly">
                                @foreach ($driversPm as $driver)
                                    <option value="{{$driver->id}}" {{old('driver_id_pm',$item->driver_id_pm ?? '') == $driver->id ? 'selected' : ''}}>
                                        {{$driver->driver_name}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="moderator_id_am">{{__('form.groups.moderator_am')}}*</label>
                            <x-UI.forms.select2 name='moderator_id_am' :placeholder="__('form.groups.moderator_am')" :readonly="$readonly">
                                @foreach ($moderatorsAm as $moderator)
                                    <option value="{{$moderator->id}}" {{old('moderator_id_am',$item->moderator_id_am ?? '') == $moderator->id ? 'selected' : ''}}>
                                        {{$moderator->name}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>
                        <div class="col-md-6">
                            <label for="moderator_id_pm">{{__('form.groups.moderator_pm')}}*</label>
                            <x-UI.forms.select2 name='moderator_id_pm' :placeholder="__('form.groups.moderator_pm')" :readonly="$readonly">
                                @foreach ($moderatorsPm as $moderator)
                                    <option value="{{$moderator->id}}" {{old('moderator_id_pm',$item->moderator_id_pm ?? '') == $moderator->id ? 'selected' : ''}}>
                                        {{$moderator->name}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="sector_id">{{__('form.groups.sector')}}*</label>
                            <x-UI.forms.select2 name='sector_id' :placeholder="__('form.groups.sector')" :readonly="$readonly">
                                @foreach ($sectors as $sector)
                                    <option value="{{$sector->id}}" {{old('sector_id',$item->sector_id ?? '') == $sector->id ? 'selected' : ''}}>
                                        {{$sector->name}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>
                        <div class="col-md-6">
                            <label for="student_cost">{{__('form.groups.student_cost')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="student_cost" :value="old('student_cost', $item['student_cost'] ?? '')" :placeholder="__('form.groups.student_cost')" type="number" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="school_cost">{{__('form.groups.school_cost')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="school_cost" :value="old('school_cost', $item['school_cost'] ?? '')" :placeholder="__('form.groups.school_cost')" type="number" />
                        </div>
                    </div>

                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.edit',['attr'=>' ']) }}
                            </button>
                            @endif
                            <a  href="{{route('groups.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
