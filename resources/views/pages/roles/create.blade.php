@push('blugin-css')
    <link href="{{asset('assets/libs/bootstrap-toggle/bootstrap-toggle.min.css')}}" rel="stylesheet">
@endpush
@push('javascript')
    <script src="{{asset('assets/libs/bootstrap-toggle/bootstrap-toggle.min.js')}}"></script>
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            $('.permission-group').on('change', function(){
                $(this).siblings('ul').find("input[type='checkbox']").prop('checked', this.checked);
            });

            $('.permission-select-all').on('click', function(){
                $('ul.permissions').find("input[type='checkbox']").prop('checked', true);
                return false;
            });

            $('.permission-deselect-all').on('click', function(){
                $('ul.permissions').find("input[type='checkbox']").prop('checked', false);
                return false;
            });

            function parentChecked(){
                $('.permission-group').each(function(){
                    var allChecked = true;
                    $(this).siblings('ul').find("input[type='checkbox']").each(function(){
                        if(!this.checked) allChecked = false;
                    });
                    $(this).prop('checked', allChecked);
                });
            }

            parentChecked();

            $('.the-permission').on('change', function(){
                parentChecked();
            });
        });
    </script>
@endpush

<x-layout.form :title="$readonly ? __('sidebar.roles.show') : __('sidebar.roles.add')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" role="form" method="POST" action="{{($edit?? false) ?route('roles.update',['role'=>$role->id]):  route('roles.store') }}" data-parsley-validate novalidate autocomplete="off">
                    @csrf
                    <div class="form-group row">
                        <label for="name">{{__('form.roles.name')}}*</label>
                        <x-UI.forms.input :readonly=$readonly name="name" :value="$readonly ? $item['name'] : old('name')" :placeholder="__('form.roles.name')" />
                    </div>
                    <div class="mb-2">
                        <label for="permission">{{__('web.actions')}}*</label><br>
                        @if (!$readonly)
                        <a href="#" class="permission-select-all">{{__('web.select-all')}}</a> / <a href="#"  class="permission-deselect-all">{{__('web.deselect-all')}}</a>
                        @endif
                    </div>

                    <ul class="permissions checkbox ml-2">
                        @foreach($permissions as $table => $permission)
                            <li class="">
                                {{-- <x-UI.forms.input type="checkbox" :readonly=$readonly name="name" :value="$readonly ? $item['name'] : old('name')" :placeholder="''" /> --}}
                                <input type="checkbox" {{$readonly ? 'disabled' : ''}} id="{{$table}}"  class="permission-group">
                                <label for="{{$table}}"><strong>{{ __('web.' . $table) }}</strong></label>
                                <ul>
                                    @foreach($permission as $perm)
                                        <li class="ml-3">
                                            <input type="checkbox" id="permission-{{$perm->id}}" name="permissions[]" class="the-permission " value="{{$perm->id}}" {{in_array($perm->id,isset($item) ? $item['permissions']->pluck('id')->toArray() : []) ? 'checked' :''}} {{$readonly ? 'disabled' : ''}}>
                                            <label for="permission-{{$perm->id}}">{{ $perm->name }}</label>
                                        </li>
                                    @endforeach
                                </ul>
                                <br>
                            </li>
                        @endforeach
                    </ul>

                    <div class="form-group row">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{__('web.add',['attr'=>'']) }}

                            </button>
                            <button type="reset"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.deselect')}}
                            </button>
                            @endif
                            <a  href="{{route('roles.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.back')}}
                            </a>

                        </div>
                    </div>
                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
