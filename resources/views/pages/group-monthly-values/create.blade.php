
<x-layout.form :title="$readonly ? __('sidebar.group_monthly_values.show') : __('sidebar.group_monthly_values.add')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{ route('group_monthly_values.store') }}" data-parsley-validate novalidate autocomplete="off">
                    @if ($readonly)
                        @method('get')
                    @endif
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="sep">{{__('form.group_monthly_values.sep')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="sep" :value="$readonly ? $item['sep'] : old('sep')" :placeholder="__('form.group_monthly_values.sep')" type="number" />
                        </div>
                        <div class="col-md-6">
                            <label for="oct">{{__('form.group_monthly_values.oct')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="oct" :value="$readonly ? $item['oct'] : old('oct')" :placeholder="__('form.group_monthly_values.oct')" type="number" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="nov">{{__('form.group_monthly_values.nov')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="nov" :value="$readonly ? $item['nov'] : old('nov')" :placeholder="__('form.group_monthly_values.nov')" type="number" />
                        </div>
                        <div class="col-md-6">
                            <label for="dec">{{__('form.group_monthly_values.dec')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="dec" :value="$readonly ? $item['dec'] : old('dec')" :placeholder="__('form.group_monthly_values.dec')" type="number" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="jan">{{__('form.group_monthly_values.jan')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="jan" :value="$readonly ? $item['jan'] : old('jan')" :placeholder="__('form.group_monthly_values.jan')" type="number" />
                        </div>
                        <div class="col-md-6">
                            <label for="feb">{{__('form.group_monthly_values.feb')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="feb" :value="$readonly ? $item['feb'] : old('feb')" :placeholder="__('form.group_monthly_values.feb')" type="number" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="mar">{{__('form.group_monthly_values.mar')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="mar" :value="$readonly ? $item['mar'] : old('mar')" :placeholder="__('form.group_monthly_values.mar')" type="number" />
                        </div>
                        <div class="col-md-6">
                            <label for="apr">{{__('form.group_monthly_values.apr')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="apr" :value="$readonly ? $item['apr'] : old('apr')" :placeholder="__('form.group_monthly_values.apr')" type="number" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="may">{{__('form.group_monthly_values.may')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="may" :value="$readonly ? $item['may'] : old('may')" :placeholder="__('form.group_monthly_values.may')" type="number" />
                        </div>
                        <div class="col-md-6">
                            <label for="group_id">{{__('form.group_monthly_values.group')}}*</label>
                            <x-UI.forms.select2 name='group_id' :placeholder="__('form.group_monthly_values.group')" :readonly="$readonly">
                                @foreach ($groups as $group)
                                    <option value="{{$group->id}}" {{old('group_id',$item->group_id ?? '') == $group->id ? 'selected' : ''}}>
                                        {{$group->name}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>
                    </div>

                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.add',['attr'=>' ']) }}
                            </button>
                            @endif
                            <a  href="{{route('group_monthly_values.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
