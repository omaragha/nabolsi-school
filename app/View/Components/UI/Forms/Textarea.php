<?php

namespace App\View\Components\UI\Forms;

use Illuminate\View\Component;

class Textarea extends Component
{
    public $name;
    public $placeholder;
    public $value;
    public $readonly;
    public $required;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $placeholder, $value, $required = false, $readonly = false)
    {
        $this->name = $name;
        $this->placeholder = $placeholder;
        $this->value = $value;
        $this->required = $required;
        $this->readonly = $readonly;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.UI.forms.textarea');
    }
}
