<?php

use App\Http\Controllers\Web\SectorController;
use Illuminate\Support\Facades\Route;

Route::resource('sectors', SectorController::class);

