<x-layout.form :title="'إضافة طلاب من ملف اكسل'">

    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                <form action="{{ route('students.import') }}" method="POST" enctype="multipart/form-data" >
                    {{ csrf_field() }}

                    <div class="form-group">
                        <input name="file" type="file" id="file" class="dropify" data-height="300" />
                    </div>

                    <div class="text-center">
                        <button class="btn btn-success">إضافة</button>
                        <a href="{{ route('students.index') }}" class="btn btn-secondary">عودة</a>
                    </div>


                </form>
            </div>
        </div>
    </div>

</x-layout.form>

