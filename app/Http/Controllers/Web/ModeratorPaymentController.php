<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResourceController;
use App\Models\ModeratorPayment;
use App\Traits\ResourceTrait;
use Illuminate\Http\Request;

class ModeratorPaymentController extends ResourceController
{
    public function __construct(ModeratorPayment $model) {
        parent::__construct($model);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'amount' => 'required|numeric',
            'month' => 'required|numeric|in:' . implode(',', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]),
            'moderator_id' => 'required|exists:moderators,id'
        ]);

        $moderatorPayment = ModeratorPayment::create($request->all());

        return redirect()->route('moderator_payments.index')->with('success', 'تم إضافة دفعة للمشرفة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'amount' => 'required|numeric',
            'month' => 'required|numeric|in:' . implode(',', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]),
            'moderator_id' => 'required|exists:moderators,id'
        ]);

        $moderatorPayment = ModeratorPayment::find($id);

        if (!$moderatorPayment) {
            return abort(404);
        }

        $moderatorPayment->update($request->all());

        return redirect()->route('moderator_payments.index')->with('success', 'تم تعديل الدفعة للمشرفة بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}
