<x-layout.data-table title="{{__('sidebar.roles.index')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                        @if (canAccess('store_role'))
                            <a href="{{route('roles.create')}}" class="btn btn-success waves-effect  waves-light">
                                <i class="fas fa-plus"></i>
                                إضافة صلاحية جديد
                            </a>
                        @endif
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="datatable" class="table table-bordered nowrap mt-5">
                        <thead>
                        <tr>
                            <th data-orderable="false">{{__('web.actions')}}</th>
                            <th>{{__('datatable.roles.name')}}</th>
                            <th>{{__('datatable.roles.created_at')}}</th>
                            {{-- <th></th> --}}
                        </tr>
                        </thead>
                            @foreach ($items as $item)
                            <tr>
                                <td>
                                    @if (canAccess('show_role'))
                                        <a href="{{route('roles.show',['role'=>$item->id])}}" class="text-info btn-sm waves-light">
                                            <i class="far fa-eye" data-toggle="tooltip" data-placement="top" title="{{__('web.view')}}"></i>
                                            {{-- {{__('web.view')}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('update_role'))
                                        <a href="{{route('roles.edit',['role'=>$item->id])}}" class="text-warning btn-sm waves-light">
                                            <i class=" fas fa-edit" data-toggle="tooltip" data-placement="top" title="{{__('web.edit')}}"></i>
                                            {{-- {{__('web.edit')}} --}}
                                        </a>
                                    @endif

                                    @if (canAccess('delete_role'))
                                        <a href="javascript" onclick="$('#modal-form').attr('action','{{route('roles.destroy',['role'=>$item->id])}}');
                                            $('#delete-name').text('{{$item->name}}')""  data-toggle="modal" data-target="#modal" class="text-danger btn-sm waves-light" data-placement="top" title="حذف">
                                            <i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="{{__('web.delete',['attr'=>''])}}"></i>
                                            {{-- {{__('web.delete',['attr'=>''])}} --}}
                                        </a>
                                    @endif
                                </td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->created_at}}</td>
                                {{-- <td></td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <x-UI.modal.scroll-modal :title="__('web.delete',['attr'=>__('web.role')])" :body="__('web.the_role')"/>
</x-layout.data-table >
