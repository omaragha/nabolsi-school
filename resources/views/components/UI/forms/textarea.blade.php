<textarea name="{{ $name }}" placeholder="{{ $placeholder }}" class="form-control" cols="30" rows="5" {{ $required ? 'required' : '' }} {{ $readonly ? 'disabled' : '' }}>{{ $value }}</textarea>

