
<x-layout.form :title="$readonly ? __('sidebar.contractors.show') : __('sidebar.contractors.add')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{ route('contractors.store') }}" data-parsley-validate novalidate autocomplete="off">
                    @if ($readonly)
                        @method('get')
                    @endif
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="name">{{__('form.contractors.name')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="name" :value="$readonly ? $item['name'] : old('name')" :placeholder="__('form.contractors.name')" />
                        </div>
                        <div class="col-md-6">
                            <label for="mobile1">{{__('form.contractors.mobile1')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="mobile1" :value="$readonly ? $item['mobile1'] : old('mobile1')" :placeholder="__('form.contractors.mobile1')" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="mobile2">{{__('form.contractors.mobile2')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="mobile2" :value="$readonly ? $item['mobile2'] : old('mobile2')" :placeholder="__('form.contractors.mobile2')" />
                        </div>
                    </div>

                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.add',['attr'=>' ']) }}
                            </button>
                            @endif
                            <a  href="{{route('contractors.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
