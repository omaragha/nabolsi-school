<?php

namespace App\Http\Controllers;

use App\Models\Bus;
use App\Models\Contractor;
use App\Models\Moderator;
use App\Models\Role;
use App\Models\Sector;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ResourceController extends Controller
{
    protected $model;

    public function __construct($model) {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $relations = $this->model->getRelations();

        $items = $this->model->with($relations)->get()->map(function ($item) {
            $item->type = $item->type == 1 ? 'ذكور' : 'إناث';
            return $item;
        });

        $view = 'pages.' . Str::plural(strtolower(fixingName(class_basename($this->model)))) . '.index';

        return view($view, compact('items'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $readonly = false;

        $view = 'pages.' . Str::plural(strtolower(fixingName(class_basename($this->model)))) . '.create';

        $model = class_basename($this->model);

        $data = [
            'readonly' => $readonly
        ];

        $callFunctionsDependModel = getFunctionsDependModel();

        if (isset($callFunctionsDependModel[$model])) {
            foreach ($callFunctionsDependModel[$model] as $name => $fn) {
                $data[$name] = $fn['method'](...$fn['params']);
            }
        }

        return view($view, $data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        $relations = $this->model->getRelations();

        $item = $this->model->with($relations)->find($id);

        if (!$item) {
            return abort(404);
        }

        $view = 'pages.' . Str::plural(strtolower(fixingName(class_basename($this->model)))) . '.create';

        $readonly = true;

        $model = class_basename($this->model);

        $data = [
            'readonly' => $readonly,
            'item' => $item
        ];

        $callFunctionsDependModel = getFunctionsDependModel();

        if (isset($callFunctionsDependModel[$model])) {
            foreach ($callFunctionsDependModel[$model] as $name => $fn) {
                if ($model == 'Group' && $name != 'sectors') {
                    $fn['params'][] = $id;
                }
                $data[$name] = $fn['method'](...$fn['params']);
            }
        }

        return view($view, $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $relations = $this->model->getRelations();

        $item = $this->model->with($relations)->find($id);

        if (!$item) {
            return abort(404);
        }

        $view = 'pages.' . Str::plural(strtolower(fixingName(class_basename($this->model)))) . '.edit';

        $readonly = false;

        $model = class_basename($this->model);

        $data = [
            'readonly' => $readonly,
            'item' => $item
        ];

        $callFunctionsDependModel = getFunctionsDependModel();

        if (isset($callFunctionsDependModel[$model])) {
            foreach ($callFunctionsDependModel[$model] as $name => $fn) {
                if ($model == 'Group' && $name != 'sectors') {
                    $fn['params'][] = $id;
                }
                $data[$name] = $fn['method'](...$fn['params']);
            }
        }

        return view($view, $data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $item = $this->model->find($id);

        if (!$item) {
            return abort(404);
        }

        $itemName = strtolower(fixingName(class_basename($this->model), '_'));

        $item->delete();

        return redirect()->route(Str::plural($itemName) . '.index')->with('success', 'تم حذف ' . __('web.the_' . $itemName) . ' بنجاح');
    }
}
