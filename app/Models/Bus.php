<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    use HasFactory;

    protected $fillable = ['driver_name', 'bus_num', 'driver_mobile', 'capacity'];

    protected $relations = ['groupsAm', 'groupsPm'];

    public function groupsAm() {
        return $this->hasMany(Group::class, 'driver_id_am');
    }

    public function groupsPm() {
        return $this->hasMany(Group::class, 'driver_id_pm');
    }

}
