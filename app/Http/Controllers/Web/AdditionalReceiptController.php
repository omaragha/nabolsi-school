<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\ResourceController;
use App\Models\AdditionalReceipt;
use Illuminate\Http\Request;

class AdditionalReceiptController extends ResourceController
{
    public function __construct(AdditionalReceipt $model) {
        parent::__construct($model);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'amount' => 'required|numeric',
            'date' => 'required|date'
        ]);

        $additionalReceipt = AdditionalReceipt::create($request->all());

        return redirect()->route('additional_receipts.index')->with('success', 'تم إضافة الدفعة بنجاح');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'amount' => 'required|numeric',
            'date' => 'required|date'
        ]);

        $additionalReceipt = AdditionalReceipt::find($id);

        if (!$additionalReceipt) {
            return abort(404);
        }

        $additionalReceipt->update($request->all());

        return redirect()->route('additional_receipts.index')->with('success', 'تم تعديل الدفعة بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}
