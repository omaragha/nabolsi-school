<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResourceController;
use App\Imports\StudentsImport;
use App\Models\Student;
use App\Traits\ResourceTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class StudentController extends ResourceController
{
    // use ResourceTrait {
    //     ResourceTrait::__construct as private __tConstruct;
    // }

    public function __construct(Student $model)
    {
        parent::__construct($model);
        // $this->__tConstruct($model);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $request->validate([
            'name' => 'required',
            'father_name' => 'required',
            'mother_name' => 'required',
            'class' => 'required|in:' . implode(',', getEnumOptions('students', 'class')),
            'division' => 'required',
            'gender' => 'required|in:'. implode(',', getEnumOptions('students', 'gender')),
            'phone' => 'required',
            'father_mobile' => 'required',
            'mother_mobile' => 'required',
            'address' => 'required',
            'sector_id' => 'nullable|exists:sectors,id'
        ]);

        $student = Student::create($request->all());

        return redirect()->route('students.index')->with('success', 'تم إضافة طالب بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $request->validate([
            'name' => 'required',
            'father_name' => 'required',
            'mother_name' => 'required',
            'class' => 'required|in:' . implode(',', getEnumOptions('students', 'class')),
            'division' => 'required',
            'gender' => 'required|in:'. implode(',', getEnumOptions('students', 'gender')),
            'phone' => 'required',
            'father_mobile' => 'required',
            'mother_mobile' => 'required',
            'address' => 'required',
            'sector_id' => 'nullable|exists:sectors,id'
        ]);

        $student = Student::find($id);

        if (!$student) {
            return abort(404);
        }

        $student->update($request->all());

        return redirect()->route('students.index')->with('success', 'تم تعديل الطالب بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updateSector(Request $request, $studentId) {

        $request->validate([
            'sector_id' => 'required|exists:sectors,id'
        ]);

        $student = Student::find($studentId);

        if (!$student) {
            return sendError('Student NOT found!', 404);
        }

        $student->father_name = $request->father_name;
        $student->mother_name = $request->mother_name;
        $student->phone = $request->phone;
        $student->student_mobile = $request->student_mobile;
        $student->father_mobile = $request->father_mobile;
        $student->mother_mobile = $request->mother_mobile;
        $student->address = $request->address;
        $student->sector_id = $request->sector_id;

        $student->save();

        return sendResponse([], 'تم تعديل منطقة الطالب بنجاح');
    }

    public function getActualCost($studentId) {

        $student = Student::with('group')->where('id', $studentId)->get()->first();

        if (!$student) {
            return sendError('Student NOT found!', 404);
        }

        return sendResponse($student, 'Getting student actual cost successfully.');
    }

    public function updateActualCost(Request $request, $studentId) {

        $request->validate([
            'actual_cost' => 'required|numeric',
            'notes'       => 'required'
        ]);

        $student = Student::find($studentId);

        if (!$student) {
            return sendError('Student NOT found!', 404);
        }

        $student->actual_cost = $request->actual_cost;
        $student->notes = $request->notes;

        $student->save();

        return sendResponse([], 'تم تعديل حسميات الطالب بنجاح');
    }

    public function getFormUpload() {
        return view('pages.import-file');
    }

    public function upload(Request $request) {

        $request->validate([
            'file' => 'required|mimes:' . implode(',', ['doc', 'csv', 'xlsx', 'xls', 'docx', 'ppt', 'odt', 'ods', 'odp'])
        ]);

        $array = Excel::toArray(new StudentsImport, $request->file('file'));

        DB::beginTransaction();

        try {

            foreach ($array as $key => $row) {

                foreach ($row as $key1 => $col) {

                    if ($key1 == 0)continue;

                    Student::create([
                        'name' => $col[0],
                        'father_name' => $col[1],
                        'mother_name' => $col[2],
                        'class' => strval($col[3]),
                        'division' => $col[4],
                        'gender' => $col[5],
                        'phone' => $col[6],
                        'student_mobile' => $col[7],
                        'father_mobile' => $col[8],
                        'mother_mobile' => $col[9],
                        'address' => $col[10]
                    ]);

                }

            }

            DB::commit();

            return redirect()->route('students.index')->with('success', 'تم إضافة الطلاب بنجاح');
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->withErrors('فشل عملية إضافة الطلاب, يرجى التأكد من بيانات الملف.');
        }

    }

}
