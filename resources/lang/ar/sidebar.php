<?php

return [

    'index' => 'الصفحة الرئيسية',
    'Dashboard' => 'الاحصائيات',
    'settings' => [
        'index'=>'الاعدادات',
        'edit' => 'تعديل الاعدادات'
    ],
    'users' =>[
        'index'=>'المستخدمين',
        'add' => 'إضافة مستخدم جديد',
        'update'=>'تعديل مستخدم',
        'show' => 'مشاهدة مستخدم'
    ],
    'roles' =>[
        'index'=>'الصلاحيات',
        'add' => 'إضافة صلاحية جديد',
        'update'=>'تعديل صلاحية',
        'show' => 'مشاهدة صلاحية'
    ],
    'sectors' => [
        'index' => 'المناطق',
        'add' => 'إضافة منطقة',
        'update' => 'تعديل منطقة',
        'show' => 'مشاهدة منطقة'
    ],
    'students' => [
        'index' => 'الطلاب',
        'add' => 'إضافة طالب',
        'update' => 'تعديل طالب',
        'show' => 'مشاهدة طالب'
    ],
    'moderators' => [
        'index' => 'المشرفات',
        'add' => 'إضافة مشرفة',
        'update' => 'تعديل مشرفة',
        'show' => 'مشاهدة مشرفة'
    ],
    'contractors' => [
        'index' => 'المتعهدون',
        'add' => 'إضافة متعهد',
        'update' => 'تعديل متعهد',
        'show' => 'مشاهدة متعهد'
    ],
    'receipts' => [
        'index' => 'الإيرادات',
        'add' => 'إضافة دفعة',
        'update' => 'تعديل دفعة',
        'show' => 'مشاهدة دفعة'
    ],
    'additional_expenses' => [
        'index' => 'النفقات الإضافية',
        'add' => 'إضافة دفعة',
        'update' => 'تعديل دفعة',
        'show' => 'مشاهدة دفعة'
    ],
    'additional_receipts' => [
        'index' => 'إيرادات إضافية',
        'add' => 'إضافة دفعة',
        'update' => 'تعديل دفعة',
        'show' => 'مشاهدة دفعة'
    ],
    'payments' => [
        'index' => 'دفعات المتعهد',
        'add' => 'إضافة دفعة',
        'update' => 'تعديل دفعة',
        'show' => 'مشاهدة دفعة'
    ],
    'buses' => [
        'index' => 'الحافلات',
        'add' => 'إضافة حافلة',
        'update' => 'تعديل حافلة',
        'show' => 'مشاهدة حافلة'
    ],
    'groups' => [
        'index' => 'الوجبات',
        'add' => 'إضافة وجبة',
        'update' => 'تعديل وجبة',
        'show' => 'مشاهدة وجبة'
    ],
    'group_monthly_values' => [
        'index' => 'القيم الشهرية للوجبات',
        'add' => '',
        'update' => 'تعديل القيم الشهرية لوجبة',
        'show' => 'مشاهدة القيم الشهرية لوجبة'
    ],
    'moderator_payments' => [
        'index' => 'رواتب المشرفات',
        'add' => 'إضافة دفعة للمشرفات',
        'update' => 'تعديل دفعة لمشرفة',
        'show' => 'مشاهدة دفعة لمشرفة'
    ],
    'Accounts'=>[
        'index'=>'الحسابات',
        'add'=>'اضافة حساب',
        'update'=>'تعديل حساب',
        'show'=>'مشاهدة حساب'
    ],
    'Payment'=>[
        'index'=>'الدفعات'
    ],
    'classroom'=>[
        'index'=>'القاعات',
        'add'=>'اضافة قاعة',
        'edit'=>'تعديل قاعة',
        'view'=> 'مشاهدة قاعة'
    ],
    'Activities' => [
        'index' => 'النشاطات',
        'add' => 'اضافة نشاط',
        'edit' => 'تعديل نشاط',
        'view' => 'مشاهدة نشاط'
    ],
    'news' => [
        'index' => 'الاخبار',
        'add' => 'اضافة خبر',
        'edit' => 'تعديل خبر',
        'view' => 'مشاهدة خبر'
    ],
    'courses'=>[
        'index'=>'الدورات',
        'add'=>'اضافة دورة',
        'edit'=>'تعديل دورة',
        'view'=>'مشاهدة دورة'
    ],
    'reservations'=>[
        'index'=>'الحجوزات',
        'add' =>'اضافة حجز',
        'edit'=>'تعديل حجز',
        'show' =>'مشاهدة حجز'
    ],
    'birthday-reservations'=>[
        'index'=>'اعياد الميلاد',
        'add' =>'اضافة حجز',
        'edit'=>'تعديل حجز',
        'show' =>'مشاهدة حجز'
    ],
    'contacts' => [
        'index' => 'المراسلات'
    ],
    'statistics' => [
        'index' => 'الإحصاءات'
    ],
    'reports' => [
        'reports'  => 'التقارير',
        'students' => 'الطلاب',
        'students_for_drivers' => 'الطلاب للسائقين',
        'students_for_moderators' => 'الطلاب للمشرفين',
        'students_for_accountants' => 'الطلاب للمحاسبين',
        'accountStatementStudent' => 'كشف حساب طالب',
        'paymentsAccount' => 'كشف بالدفعات',
        'groups' => 'الوجبات',
        'studentsNumber' => 'عدد الطلاب الكلي',
        'groupsFinancial' => 'تقرير مالي للوجبات',
        'discounts' => 'حسومات',
        'studentsFinancial' => 'تقرير مالي للطلاب'
    ]
];
