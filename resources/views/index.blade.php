<x-layout.app title="{{__('pages.index.title')}}">
    <div class="text-center p-5">
        {{__('pages.index.welcome')}} <span class="font-weight-bold">{{auth()->user()->name}}</span>
    </div>
</x-layout.app>
