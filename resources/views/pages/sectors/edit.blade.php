
<x-layout.form :title="__('sidebar.sectors.update')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{ route('sectors.update', ['sector' => $item->id]) }}" data-parsley-validate novalidate autocomplete="off">
                    @method('put')
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="name">{{__('form.sectors.name')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="name" :value="old('name', $item['name'] ?? '')" :placeholder="__('form.sectors.name')" />
                        </div>
                    </div>

                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ __('web.edit',['attr'=>' ']) }}
                            </button>
                            @endif
                            <a  href="{{route('sectors.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('web.back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
