<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupMonthlyValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_monthly_values', function (Blueprint $table) {
            $table->id();
            $table->double('sep')->nullable();
            $table->double('oct')->nullable();
            $table->double('nov')->nullable();
            $table->double('dec')->nullable();
            $table->double('jan')->nullable();
            $table->double('feb')->nullable();
            $table->double('mar')->nullable();
            $table->double('apr')->nullable();
            $table->double('may')->nullable();
            $table->unsignedBigInteger('sector_id')->nullable();
            $table->foreign('sector_id')->references('id')->on('sectors')->onUpdate('cascade')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_monthly_values');
    }
}
