<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'father_name',
        'mother_name',
        'class',
        'division',
        'gender',
        'phone',
        'student_mobile',
        'father_mobile',
        'mother_mobile',
        'address',
        'actual_cost',
        'notes',
        'lat',
        'lon',
        'sector_id',
        'group_id'
    ];

    protected $relations = ['sector', 'receipts', 'group'];

    public function sector() {
        return $this->belongsTo(Sector::class);
    }

    public function receipts() {
        return $this->hasMany(Receipt::class);
    }

    public function group() {
        return $this->belongsTo(Group::class);
    }

}
